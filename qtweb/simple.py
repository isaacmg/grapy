#!/usr/bin/python

import sys
import os
from pathlib import Path
from PySide6.QtWidgets import QWidget, QApplication, QVBoxLayout
from PySide6.QtWidgets import QApplication
from PySide6.QtWebEngineWidgets import QWebEngineView
from PySide6.QtWebChannel import QWebChannel
from PySide6.QtCore import QObject, Signal, Slot, QIODevice, QFile, QUrl
from jinja2 import Template

class Element(QObject):
    loaded = Signal()

    def __init__(self, name, parent=None):
        super(Element, self).__init__(parent)
        self._name = name
        self._is_loaded = False

    @property
    def name(self):
        return self._name

    @property
    def is_loaded(self):
        return self._is_loaded

    @Slot()
    def set_loaded(self):
        self._is_loaded = True
        self.loaded.emit()

    def render_script(self, script, **kwargs):
        kwargs["name"] = self.name
        return Template(script).render(**kwargs)


class TestObject(Element):
    @Slot(str)
    def test(self, res):
        print(res)

class Example(QWidget):

    def __init__(self):
        super().__init__()

        self.initUI()

    def initUI(self):

        vbox = QVBoxLayout(self)

        self.webEngineView = QWebEngineView()
        self.loadPage()

        vbox.addWidget(self.webEngineView)

        self.setLayout(vbox)

        self.setGeometry(300, 300, 350, 250)
        self.setWindowTitle('QWebEngineView')
        self.show()

    @Slot(bool)
    def onLoadFinished(self, ok):
        if ok:
            #self.load_qwebchannel()
            #self.load_objects()
            self.webEngineView.page().runJavaScript("let graph3 = {'name':'Hola'};window.getInfo(graph3);");

    def load_qwebchannel(self):
        file = QFile(":/qtwebchannel/qwebchannel.js")
        if file.open(QIODevice.ReadOnly):
            content = file.readAll()
            file.close()
            self.webEngineView.page().runJavaScript(content.data().decode())
        if self.webEngineView.page().webChannel() is None:
            channel = QWebChannel(self.webEngineView.page())
            self.webEngineView.page().setWebChannel(channel)

    def load_objects(self):
        if self.webEngineView.pagbdbde().webChannel() is not None:
            objects = {obj.name: obj for obj in self._objects}
            self.webEngineView.page().webChannel().registerObjects(objects)
            _script = r"""
            {% for obj in objects %}
            window.{{obj}} = null;
            {% endfor %}

            new QWebChannel(qt.webChannelTransport, function (channel) {
                {% for obj in objects %}
                    window.{{obj}} = channel.objects.{{obj}};
                    window.getInfo(window.{{obj}});
                    window.{{obj}}.set_loaded()

                {% endfor %}
            });
            """
            self.webEngineView.page().runJavaScript(Template(_script).render(objects=objects.keys()))

    def add_object(self, obj):
        self._objects.append(obj)

    @Slot()
    def test_object_loaded(self):
        script = self.test_object.render_script(
            r"""
        //{{name}}.test(alert({{name}}.__id__));
        """
        )
        self.webEngineView.page().runJavaScript(script)

    def loadPage(self):

        #with open('qtweb/test.html', 'r') as f:
        with open('d3graph/examples/graphs1.html', 'r') as f:
            html = f.read()
            self.webEngineView.page().loadFinished.connect(self.onLoadFinished)
            self._objects = []

            #self.test_object = TestObject("test_object", self)
            #self.test_object.loaded.connect(self.test_object_loaded)
            #self.add_object(self.test_object)

            #self.webEngineView.setHtml(html, baseUrl=QUrl.fromLocalFile(str(Path(__file__).resolve().parent)))
            print(os.path.dirname(os.path.realpath(__file__)))
            print(os.getcwd()+os.path.sep)
            self.webEngineView.setHtml(html, baseUrl=QUrl.fromLocalFile(os.getcwd()+os.path.sep))



            #self.channel = QWebChannel()
            #p = Mensaje()
            #self.channel.registerObject("mensaje", p)
            #self.webEngineView.page().setWebChannel(self.channel)




def main():
    sys.argv.append("--disable-web-security")
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec())


if __name__ == '__main__':
    main()
