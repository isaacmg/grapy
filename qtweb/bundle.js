(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
  typeof define === 'function' && define.amd ? define(factory) :
  (global = typeof globalThis !== 'undefined' ? globalThis : global || self, global.socio = factory());
})(this, (function () { 'use strict';

  //import * as d3 from "d3";

  function Graph( ){
    // w: weight of the svg element
    this.w = 600;
    // h: height of the svg element
    this.h = 500;
    // border, default value: true, defined in options
    this.border = true;
    // sociotype: 0: Preference, 1: Perception Preference, 2: Perception Rejection, 3: Rejection. 0 by default.
    this.socioType = 0;
    // Store the graph
    this.graph;
    // Store the svg element
    this.svg;
    // Store the g element
    this.g;
    // Hash of the links to verify that exists in a fast way
    this.linkedHash = {};
    // Array ordenat per la key "Preference", ....
    this.sortedLinksByKey;
    // Array amb els nodes que no tenen connexions
    this.noLinkNodes = new Array(4);

    // 3 Maps to translate socioType to idSocioType, idSocioType to socioType, idColor to colorFunction
    this.toSocioType = new Map();
    this.toSocioType.set("preference",0).set("perception_preference",1).set("perception_rejection",2).set("rejection",3);
    this.fromSocioType = new Map();
    this.fromSocioType.set(0,"preference").set(1,"perception_preference").set(2,"perception_rejection").set(3,"rejection");
    this.fromColor = new Map();
    this.fromColor.set(0,"colorDegree").set(1,"colorBetweenNess").set(2,"colorCloseNess").set(3,"colorSex");

    // Array to store the max in the min centralities (Betweeness, Degree, Closeness) of the 4 socioTypes.
    this.centr = {
          betNess: [{min: Math.min(),max: Math.max()},{min: Math.min(),max: Math.max()},{min: Math.min(),max: Math.max()},{min: Math.min(),max: Math.max()}],
          deg: [{min: Math.min(),max: Math.max()},{min: Math.min(),max: Math.max()},{min: Math.min(),max: Math.max()},{min: Math.min(),max: Math.max()}],
          cloNess:[{min: Math.min(),max: Math.max()},{min: Math.min(),max: Math.max()},{min: Math.min(),max: Math.max()},{min: Math.min(),max: Math.max()}]
        };

    // Variable that stores the links of the graph as a Array
    this.links = [];

    // Store the color id and the color Function to change the node colors.
    this.color;
    this.colorFunction = "colorDegree";

    // Store the svg elements needed for create and changing the svg.
    this.link;
    this.node;
    this.markersRadius;
    this.markerSVG;
    this.circle;
    this.diana;
    this.simulation;
    // Boolean variable to store the autoCenter property.
    this.center = true;
    this.simulation.center = true;


    // Force variables
    this.forces = {
      forceStrength: -85.6,
      chargeDistanceMin: 30.4,
      chargeDistanceMax: 207.8,
      forceCollide: 1,
      forceLinks: 50,
      forceLinkStrength: 0.01
    };

    // Forces availables.
    this.availables = {
      "forceManyBody": 1,
      "forceLink": 1,
      "forceCenter": 1,
      "forceCollide": 1
    };

    // Is force active?
    this.isForceActive = true;
  }



  // Define the functions in prototype to share them with the subclasses.
  Graph.prototype = {

    //Function that change between enable and disable the autoCenter in the graph.
    autoCenter: function(b){
      this.simulation.center = b;
      this.center = b;
    },

    // Function that switch the activity of the forces
    forceSwitch: function(){
      this.isForceActive = ! this.isForceActive;
      console.log(this.isForceActive);
    },
    // Function to change the availability of the forces
    setAvailabilityForces: function(availables){
      Object.keys(availables).forEach(key => {
        this.availables[key] = availables[key] ? 1 : 0;
      });
    },
    // Function to change the forces of the graph
    setForces: function(forces){
      forces = forces || {};
      this.forces.forceStrength = forces.forceStrength || -85.6;
      this.forces.distanceMin = forces.distanceMin || 30.4;
      this.forces.distanceMax = forces.distanceMax || 207.8;
      this.forces.forceCollide = forces.forceCollide || 1;
      this.forces.forceLinks = forces.forceLinks || 50;
      this.forces.forceLinkStrength = forces.forceLinkStrength || 0.01;
    },
    restartForces: function (){
      this.forces.forceStrength = -85.6;
      this.forces.distanceMin = 30.4;
      this.forces.distanceMax = 207.8;
      this.forces.forceCollide = 1;
      this.forces.forceLinks = 50;
      this.forces.forceLinkStrength = 0.01;
    },

    updateForces: function(){
      /*this.simulation.force("charge")
          .strength(this.forces.forceStrength)
          .distanceMin(this.forces.chargeDistanceMin)
          .distanceMax(this.forces.chargeDistanceMax);
      this.simulation.force("collide")
              .strength(this.forceCollide);
      this.simulation.force("link")
                     .distance(this.forces.forceLinks);
      this.simulation.alpha(1).restart();*/
      this.restart();
    },

    // Function to manage the optios parameter that receives on creation.
    // set options to the options supplied or an empty object if none are provided
    manageOptions: function( options ){
      options = options || {};
      this.border = options.border || true;
      this.w = options.w || 500;
      this.h = options.h || 500;
    },

    // Function that creates the svg elements to stores the graph in the id element of the html.
    createSVG: function( id ){
      //this.svg = d3.select("#"+id).append("svg").attr("width", this.w).attr("height", this.h);
      this.svg = d3.select("#"+id).append("svg").attr("viewBox", "0 0 "+ this.w + " "+ this.h).attr("preserveAspectRatio", "xMidYMid meet").classed("svg-content", true);
      this.svg.attr("style", "outline: thin solid grey;");
      this.g = this.svg.append("g").attr("transform", `translate(${this.w / 2},${this.h / 2})`).attr("id","centerGraph");
      this.diana = this.g.append("g").attr("id","dianaGraph");
      this.link = this.g.append("g").attr("stroke", "#888").attr("id","edgesGraph").selectAll("line");
      this.node = this.g.append("g").attr("id","nodesGraph").selectAll(".node");
    },

    // Function that every subclass redefines to execute particular things in every subclass.
    addProperties: function(){},

    // Function that stores the graph.
    setGraph: function( json ){
      this.graph = json;
    },

    // Function that creates two structures to manage the links.
    // First: linkedHash creates the hash to detect if one link exists fast.
    // Second: sortedLinksByKey sorts the link in a array by socioType.
    calcEdges: function(){
      let edges = [];

      this.graph.links.forEach((e) => {
        let sourceNode = this.graph.nodes.filter(function(n) {return n.id === e.source;})[0];
        let targetNode = this.graph.nodes.filter(function(n) { return n.id === e.target; })[0];
        edges.push({source: sourceNode, target: targetNode, weight: e.weight, key: e.key, keyValue: this.toSocioType.get(e.key)});
        this.linkedHash[`${e.source},${e.target},${this.toSocioType.get(e.key)}`] = true;
      });

      this.sortedLinksByKey = d3.nest()
          .key(function(d) { return d.keyValue; }).sortKeys(d3.ascending)
          .entries(edges);
    },

    // Function that iterates the nodes for:
    // 1. calculate the max and min centralities to choose the correct weight of colors by every socioType.
    // 2. calculate the number of connections of every node.
    calcNodes: function(){
      let radis = [];
      for (let i = 0; i < 4; i++)
        this.noLinkNodes[i] = {};



      this.graph.nodes.forEach((e) => {
        let con = [];
        for (let i =0;i < 4;i++) {
          let aux = `${this.fromSocioType.get(i)}_betweenness`;
          if (this.centr.betNess[i].max < e[aux]) this.centr.betNess[i].max = e[aux];
          if (this.centr.betNess[i].min > e[aux]) this.centr.betNess[i].min = e[aux];
          aux = `${this.fromSocioType.get(i)}_degree`;
          if (this.centr.deg[i].max < e[aux]) this.centr.deg[i].max = e[aux];
          if (this.centr.deg[i].min > e[aux]) this.centr.deg[i].min = e[aux];
          aux = `${this.fromSocioType.get(i)}_closeness`;
          if (this.centr.cloNess[i].max < e[aux]) this.centr.cloNess[i].max = e[aux];
          if (this.centr.cloNess[i].min > e[aux]) this.centr.cloNess[i].min = e[aux];

          let connections = this.numberConnectionsSocioType(e,i);
          radis.push(connections.source);
          // This condition identifies if there are nodes without links
          if (connections.source == 0 && connections.target == 0) this.noLinkNodes[i][e.id] = e.node_id;
          con.push(connections.target);
        }
        // Stroring the con array to the node in the json graph.nodes
        e.con = con;

      });
      this.markersRadius = Array.from(new Set(radis));

    },
    radiusChange: function(radius){
      return radius+6;
    },
    // Function that test if value is undefined
    isUndefined: function(value){
        return value === undefined;
    },

    // Function that is used in calcNodes to calculate:
    // 1. the links that receives a node in one socioType.
    // 2. the links that gives one node in one socioType.
    numberConnectionsSocioType: function(a, m) {
      let t = 0;
      let s = 0;
      if (!this.isUndefined(this.sortedLinksByKey[m]))
        this.sortedLinksByKey[m].values.forEach(function(e) {
            if ((e.target.id == a.id)) t++;
            if ((e.source.id == a.id)) s++;
        });
      return {
        'source': s,
        'target': t
      };
    },

    // Function that is redefined in subclasses. Every subclass has different markers.
    addMarkers: function(){
    },

    // Function that starts the simulation calling startSimulation and restart.
    simulation: function(){
      this.startSimulation();
      this.restart();
    },

    // Function that stores in the links variable the links of the socioType choosen.
    startSimulation: function(){
      if (!this.isUndefined(this.sortedLinksByKey[this.socioType]))
          this.links = this.sortedLinksByKey[this.socioType].values;
    },

    // Function that loops when there are changes in the graph to change nodes and links positions in the svg.
    ticked: function() {
      let that = this;
      this.link.attr("x1", function(d) { return d.source.x; })
          .attr("y1", function(d) { return d.source.y; })
          //.attr("x2", function(d) { return d.target.x; })
          //.attr("y2", function(d) { return d.target.y; });
          .attr("x2", function(d) {
            var x1 = d.source.x,y1 = d.source.y,x2 = d.target.x,y2 = d.target.y,angle = Math.atan2(y2 - y1, x2 - x1);
            return x2 - Math.cos(angle) * (d.target.con[that.socioType]+6);
           })
          .attr("y2", function(d) {
            var x1 = d.source.x,y1 = d.source.y,x2 = d.target.x,y2 = d.target.y,angle = Math.atan2(y2 - y1, x2 - x1);
            return y2 - Math.sin(angle) * (d.target.con[that.socioType]+6);
           });

      this.node.attr("transform", d => `translate(${d.x}, ${d.y})`);
    },
    // Function that executes when simulation is finished.
   finishedSim: function(){
     console.log(this);
     if (this.center){
       //let positions = [];
       // This is the way to access the x and y values of the translate and scale transform attribute in g tag "centerGraph".
       //let centerGraph = d3.select("#centerGraph");
       //let transform = centerGraph.node().transform.baseVal
       //let scale = 1;
       //if(transform.numberOfItems == 2) scale = transform.getItem(1).matrix.a;

       // x value is in e attribute and y value is in f attribute.
       //let translate = transform.getItem(0).matrix;
       let maxX = Math.max();
       let maxY = Math.max();
       let minX = Math.min();
       let minY = Math.min();

       this.node.each(function(d){
         if (maxX < d.x) maxX = d.x;
         if (maxY < d.y) maxY = d.y;
         if (minX > d.x) minX = d.x;
         if (minY > d.y) minY = d.y;

       });

       let scaleArray = [1,1,1,1];

       if ((maxX+this.w/2) > this.w) scaleArray[0]=this.w/(maxX+this.w/2+50);
       if ((maxY+this.h/2) > this.h) scaleArray[1]=this.h/(maxY+this.h/2+50);
       if ((minX+this.w/2) < 0) scaleArray[2]=this.w/(Math.abs(minX)+this.w/2+this.w+50);
       if ((maxY+this.h/2) < 0) scaleArray[3]=this.h/(Math.abs(minY)+this.h/2+this.h+50);
       let minScale = Math.min(...scaleArray);

       let centerX = (maxX+minX)/2;
       let centerY = (maxY+minY)/2;

       d3.select('svg')
    	  .transition()
    	  .call(this.zoom.translateTo, 0.5 * this.w +centerX , 0.5 * this.h+centerY)
        .transition().call(this.zoom.scaleTo, minScale);

    }
   },
    // Function that verifies if 2 nodes are connected using the hash created before.
    isConnected: function(a, b) {
        return this.linkedHash[`${a.id},${b.id},${this.socioType}`] || this.linkedHash[`${b.id},${a.id},${this.socioType}`] || a.id == b.id;
    },

    // Function that highlight the nodes connected to a node.
    set_highlight: function(d){
      var that = this;
      this.circle.attr("stroke", function(o) {
                return that.isConnected(d, o) ? "blue" : "grey";
      });
      this.circle.attr("stroke-width", function(o) {
        return that.isConnected(d, o) ? "2" : "1";
      });
      this.circle.attr("opacity", function(o) {
        return that.isConnected(d, o) ? "1" : "0.5";
      });

      this.text.style("font-weight", function(o) {
        return that.isConnected(d, o) ? "bold" : "normal";
      });
      this.text.attr("opacity", function(o) {
        return that.isConnected(d, o) ? 1 : 0.5;
      });

      this.link.style("stroke", function(o) {
        return o.source.index == d.index || o.target.index == d.index ? "blue" : "#eee";
      });
      this.link.attr("opacity", function(o) {
        return o.source.index == d.index || o.target.index == d.index ? 1 : 0;
      });

      this.markerSVG.attr("fill", "blue");
    },

    // Function that puts all the nodes in the same color border when the node is exit.
    exit_highlight: function(){
      this.circle.attr("stroke", "grey").attr("stroke-width", 1).attr("opacity",1);
      this.text.style("font-weight", "normal").attr("opacity",1);
      this.link.style("stroke", "grey").attr("opacity",1);
      this.markerSVG.attr("fill", "grey");
    },

    // Function to color the nodes depending on the number of connections
    colorNumConnections: function(){
      let color = d3.scaleLinear()
        .domain([0,7])
        .range(['blue','white']);

      let colorRed = d3.scaleLinear()
        .domain([0,7])
        .range(['red','white']);

      this.circle = this.circle
                  .attr("fill", (d) => { return ( (this.socioType == 0 || this.socioType == 1) ? color(this.numberConnections(d)) : colorRed(this.numberConnections(d))); })
                  .attr("stroke", "grey");
    },

    // Function to color the nodes depending on the Betweeness centrality
    colorBetweenNess: function(){
      let aux = `${this.fromSocioType.get(this.socioType)}_betweenness`;

      let colorBet = d3.scaleLinear()
                      .domain([this.centr.betNess[this.socioType].min,this.centr.betNess[this.socioType].max])
                      .range(['DarkGreen','white'])
                      .unknown("DarkGreen");
      this.circle = this.circle
                  .attr("fill", function(d) { return  colorBet(d[aux]);})
                  .attr("stroke", "grey");
    },

    // Function to color the nodes depending on the Degree centrality
    colorDegree: function(){
      let aux = `${this.fromSocioType.get(this.socioType)}_degree`;
      let colorDeg = d3.scaleLinear()
                      .domain([this.centr.deg[this.socioType].min,this.centr.deg[this.socioType].max])
                      .range(['blue','white'])
                      .unknown("blue");
      this.circle = this.circle
                  .attr("fill", function(d) { return  colorDeg(d[aux]);})
                  .attr("stroke", "grey");
    },

    // Function to color the nodes depending on the Closeness centrality
    colorCloseNess: function(){
      let aux = `${this.fromSocioType.get(this.socioType)}_closeness`;
      let colorClo = d3.scaleLinear()
                      .domain([this.centr.cloNess[this.socioType].min,this.centr.cloNess[this.socioType].max])
                      .range(['orange','white'])
                      .unknown("orange");
      this.circle = this.circle
                  .attr("fill", function(d) { return  colorClo(d[aux]);})
                  .attr("stroke", "grey");
    },

    // Function to color the nodes depending on the sex of the nodes.
    colorSex: function(){
      this.circle = this.circle
                  .attr("fill", function(d) { return d.sex === "F" ? "brown" : "steelblue"; })
                  .attr("stroke", "black");
    },


    // Function that calculate the links of a node in the current socioType.
    numberConnections: function(a) {
      return this.numberConnectionsSocioType(a,this.socioType).target;
    },

    // Function that calculates the number of links of the nodes and return as a array.
    linksPerNode: function(a){
      let aux = [];
      let linksSocioType = [];
      if (!this.isUndefined(a[this.socioType])) linksSocioType = a[this.socioType].values;
      this.graph.nodes.forEach(function (node) {
        aux[node.node_id] = 0;
      });
      linksSocioType.forEach(function (link) {
        aux[link.target.node_id] = aux[link.target.node_id] + 1;
      });

      return aux;
    },

    // Function that find the max value of an array
    maxLinksPerNode: function(a){
      let aux = -1;
      for (var key in a){
        if (a[key] > aux) aux = a[key];
      }
      return aux;
    },
    // Function that find the min value of an array
    minLinksPerNode: function(a){
      let aux = this.graph.nodes.length*2;
      for (var key in a){
        if (a[key] < aux) aux = a[key];
      }
      return aux;
    },
    dragstarted:  function(d){
      if (!d3.event.active) this.simulation.alphaTarget(0.3).restart();
      d.fx = d.x;
      d.fy = d.y;
    },
    dragged: function(d){
      d.fx = d3.event.x;
      d.fy = d3.event.y;
    },
    dragended: function(d){
      if (!d3.event.active) this.simulation.alphaTarget(0);
      d.fx = null;
      d.fy = null;
    }

  };

  //import * as d3 from "d3";

  // Subclass to generate forced group graph.
  function ForcedGraph(options){
    this.manageOptions(options);

    this.addProperties = function(){
        this.svg = this.svg.style("cursor","move");
    };

    this.addMarkers = function(){
      // Defining Arrows
      this.markerSVG = this.svg.append("defs").selectAll("marker")
         .data(this.markersRadius)
         .join("marker")
           .attr("id", function(d){return `arrow-${(d*2 + 8)}`;})
           //.attr("id", function(d){return `arrow-${that.radius(d)}`;})
           .attr("viewBox", "0 -5 10 10")
           //.attr("refX", function(d){return d + 20;})
           .attr("refX", function(d){return (d*2 + 15);})
           .attr("refY", 0)
           .attr("markerWidth", 12)
           .attr("markerHeight", 6)
           .attr("orient", "auto")
           .append("path")
           .attr("fill", "grey")
           .attr("d", "M0,-5L10,0L0,5"); //M0,-5L10,0L0,5
    };
    this.restart = function(){
      if(this.isForceActive){
          // Apply the general update pattern to the nodes.
          this.node = this.node.data(this.graph.nodes, function(d) { return d.node_id;});
          d3.selectAll("circle").remove();
          d3.selectAll("text").remove();

          this.node = this.node.join("g").attr("class","node");


          this.node.exit().transition()
              .attr("r", 0)
              .remove();
          var that = this;
          this.circle = this.node.append("circle")
                   .attr("stroke-width", 1).attr("class", "nodesCircles")
                   .attr("node_id", function(d){return d.node_id; }).attr("id", function(d){return d.id; })
                   .call((node) => { node.transition().attr("r", (d) => { return this.radius(d); }); });


          this.node.call(d3.drag()
                   .on("start", function(d){ that.dragstarted(d); })
                   .on("drag", function(d){ that.dragged(d); })
                   .on("end", function(d){ that.dragended(d); }))
                   .on("dblclick", function(d){ that.dblclickAction(d); })
                   .on("mouseover", function(d){ that.set_highlight(d); })
                   .on("mouseout",  function(){ that.exit_highlight(); });


          // Es concreta el color dels nodes depenen de la variable colorFunction
          this[this.colorFunction]();


          this.text = this.node.append("text")
              .text(function(d) { return d.node_id; })
              .style("fill","#000")
              .style("font-size", "14px")
              .attr("x", function(d) { return that.radius(d); }) //-8
              .attr("y", 0);

          this.node.merge(this.node);

          //add zoom capabilities
          this.zoom = d3.zoom()
            .extent([[0, 0], [this.w, this.h]])
            .scaleExtent([0.1, 10])
            .on("zoom", function() {
              that.g.attr("transform", d3.event.transform.translate(that.w/2,that.h/2));
            });
          this.svg.call(this.zoom);


          // Apply the general update pattern to the links.
          this.link = this.link.data(this.links, function(d) { return d.source.id + "-" + d.target.id; });
          // Keep the exiting links connected to the moving remaining nodes.
          // When no links and no nodes, this.link is Array. Normally it is an Object.
          if (!Array.isArray(this.link)){
              this.link.exit().transition()
                  .attr("stroke-opacity", 0)
                  .attrTween("x1", function(d) { return function() { return d.source.x; }; })
                  .attrTween("x2", function(d) { return function() { return d.target.x; }; })
                  .attrTween("y1", function(d) { return function() { return d.source.y; }; })
                  .attrTween("y2", function(d) { return function() { return d.target.y; }; })
                  .remove();
              this.link = this.link.join("line").attr("stroke-width", 0.8)//1.5
                  .call((link) => { link.transition().attr("stroke-opacity", 1); })
                  //.attr("marker-end", (d) => {return `url(#arrow-${this.radius(d.target)}`;})
                  .attr("marker-end", (d) => {return `url(${new URL(`#arrow-${(d.target.con[that.socioType])*2+8}`, location)})`})
                  .merge(this.link);

          }

          this.simulation = d3.forceSimulation(this.graph.nodes);
          if (this.availables.forceManyBody) this.simulation.force("charge", d3.forceManyBody().strength(this.forces.forceStrength).distanceMin(this.forces.chargeDistanceMin).distanceMax(this.forces.chargeDistanceMax));
          if (this.availables.forceLink) this.simulation.force("link", d3.forceLink(this.links).id(function(d) { return d.id; }).distance(this.forces.forceLinks).strength(this.forces.forceLinkStrength));
          if (this.availables.forceCenter) this.simulation.force("center", d3.forceCenter()).alphaMin(0.01);
          if (this.availables.forceCollide) this.simulation.force("collide", d3.forceCollide().strength(this.forces.forceCollide).radius(function(d) { return d.r; }));
          this.simulation.on("tick", this.ticked).on("end", this.finishedSim);


          // Esto hay que ponerlo para que dentro de ticked se pudean ver node y link
          this.simulation.node = this.node;
          this.simulation.link = this.link;
          this.simulation.w = this.w;
          this.simulation.h = this.h;
          this.simulation.zoom = this.zoom;
          this.simulation.center = this.center;
          this.simulation.socioType = this.socioType;

          // Update and restart the simulation.
          this.simulation.nodes(this.graph.nodes);
          //if (this.availables.forceLink) this.simulation.force("link").links(this.links);
          this.simulation.alpha(1).restart();
        }

    };

    this.radius = function(a){
      return this.numberConnections(a)*2 + 8;
    };
    this.dblclickAction = function(d){
      alert(`x:${d.x}, y:${d.y}, id:${d.id}, node_id:${d.node_id}`);
    };
  }
  ForcedGraph.prototype = new Graph();

  //import * as d3 from "d3";

  // Subclass to generate radial group graph.
  function RadialGroupGraph(options){
    this.manageOptions(options);

    // Defining Arrows
    this.addMarkers = function(){
      let markers = ["triangle"];
      this.markerSVG = this.svg.append("defs").selectAll("marker")
         .data(markers)
         .join("marker")
          .attr("id", "triangle")
          .attr("viewBox", "0 -5 10 10")
          .attr("refX", 22)
          .attr("refY", 0)
          .attr("markerWidth", 12)
          .attr("markerHeight", 8)
          .attr("orient", "auto")
          .attr("fill", "grey")
          .append("path")
          .attr("d", "M0,-4L10,0L0,4");
    };

    this.restart = function(){

      // The number of links that is received by every node
      let linksReceived,max,min,scaleCircle;
      linksReceived = this.linksPerNode(this.sortedLinksByKey);
      max = this.maxLinksPerNode(linksReceived);
      min = this.minLinksPerNode(linksReceived);
      scaleCircle = d3.scaleLinear()
      .domain([min,max])
      .range([0, 200]);

      // Delete nodes and texts
      this.node = this.node.data(this.graph.nodes, function(d) { return d.node_id;});
      d3.selectAll("circle").remove();
      d3.selectAll("text").remove();

      // Adding the concentric circles (diana)
      for (let i = 1; i <= (max-min); i++){
        this.diana.append("circle").attr("stroke","brown").attr("r",i*(200/(max-min))).attr("stroke-opacity",0.5).attr("fill","none");
      }

      // Event definitions for the nodes
      this.node = this.node.join("g").attr("class","node");

      // Defining the links properties
      this.link = this.link.data(this.links, function(d) { return d.source.id + "-" + d.target.id; });

      // When no links and no nodes, this.link is Array. Normally it is an Object.
      if (!Array.isArray(this.link)){
          this.link.exit().transition()
              .attr("stroke-opacity", 0)
              .attrTween("x1", function(d) { return function() { return d.source.x; }; })
              .attrTween("x2", function(d) { return function() { return d.target.x; }; })
              .attrTween("y1", function(d) { return function() { return d.source.y; }; })
              .attrTween("y2", function(d) { return function() { return d.target.y; }; })
              .remove();

          // Defining the link svg properties
          this.link = this.link.join("line").attr("stroke-width", 1).attr("stroke", "grey")
              .call(function(link) { link.transition().attr("stroke-opacity", 1); })
              .attr("marker-end", "url(#triangle)")
            .merge(this.link);
      }

      // Defining the nodes circles
      this.circle = this.node.append("circle")
           .attr("fill", function(d) { return d.sex === "F" ? "brown" : "steelblue"; })
           .attr("stroke", "grey").attr("stroke-width", 1.5)
           .call(function(node) { node.transition().attr("r", 8); });

      var that = this;
      this.circle.on("mouseover", function(d){ that.set_highlight(d); })
                 .on("mouseout",  function(){ that.exit_highlight(); });


      // Coloring the nodes
      this[this.colorFunction]();

      // Showing the node texts
      this.text = this.node.append("text")
           .text(function(d) { return d.node_id; })
           .style("fill","#000")
           .style("font-size","10px")
           .attr("x", 10)
           .attr("y", 0);


      this.node.merge(this.node);

      // Starting the simulation
      this.simulation = d3.forceSimulation(this.graph.nodes)
            .force("r", d3.forceRadial(function(d) { return scaleCircle(max-linksReceived[d.node_id]); }).strength(0.4))
            .alpha(1).alphaMin(0.001).velocityDecay(0.2)
            .on("tick", this.ticked);

      // This is necessary to pass the node and link variables to the ticked function
      this.simulation.node = this.node;
      this.simulation.link = this.link;
    };

  }
  RadialGroupGraph.prototype = new Graph();

  //import * as d3 from "d3";

  function RadialEgoDijkstraGraph(options){
    this.manageOptions(options);
    this.member = options.member;
    this.max = 0;

    this.addMarkers = function(){
      let markers = ["triangle"];
      // Defining Arrows
      this.markerSVG = this.svg.append("defs").selectAll("marker")
         .data(markers)
         .join("marker")
          .attr("id", "triangle")
          .attr("viewBox", "0 -5 10 10")
          .attr("refX", 12)
          .attr("refY", 0)
          .attr("markerWidth", 6)
          .attr("markerHeight", 6)
          .attr("orient", "auto")
          .attr("fill", "grey")
          .append("path")
          .attr("d", "M0,-4L10,0L0,4");
    },
    this.startSimulation = function(){
      this.links = this.sortedLinksByKey[0].values;
      this.links = this.dijkstra(this.memberOnNodes(this.member));
      this.max = this.maxDistanceThatIsNotInfinity();
      this.eraseInfinity(this.max);
      this.graph.nodes.forEach(function (d) {
        console.log("nodeId:"+d.node_id+ " distance:"+d.distance);
      });
    },
    this.memberOnNodes = function(memberId){
      var i = 0;
      var result = this.graph.nodes[0];
      while (this.graph.nodes[i].id != memberId && i < this.graph.nodes.length){
        i++;
      }
      if (i < this.graph.nodes.length) result = this.graph.nodes[i];
      return result;
    },
    this.maxDistanceThatIsNotInfinity = function(){
      let aux = -1;
      this.graph.nodes.forEach(function (d) {
        if (d.distance > aux && d.distance != Infinity) aux = d.distance;
      });
      return aux;
    },
    this.eraseInfinity = function(max){
      this.graph.nodes.forEach(function (d) {
        if (d.distance === Infinity)
          d.distance = max + 1;
      });

    },

    this.restart = function(){
      let scaleCircle = d3.scaleLinear()
      .domain([0,this.max+1])
      .range([0, 200]);

      this.node = this.node.data(this.graph.nodes).join("g").attr("class","node");

      // Afegim els cercles concentrics
      for (let i = 1; i <= (this.max+1); i++){
        this.diana.append("circle").attr("stroke","brown").attr("r",i*(200/(this.max+1))).attr("stroke-opacity",0.5).attr("fill","none");
      }

      this.circle = this.node.append("circle")
           .attr("fill", function(d) { return d.sex === "F" ? "brown" : "steelblue"; })
           .attr("stroke", "grey").attr("stroke-width", 1.5)
           .call(function(node) { node.transition().attr("r", 8); });

           var that = this;
           this.circle.call(d3.drag()
                    .on("start", function(d){ that.dragstarted(d); })
                    .on("drag", function(d){ that.dragged(d); })
                    .on("end", function(d){ that.dragended(d); }));


       this.text = this.node.append("text")
           .text(function(d) { return d.node_id; })
           .style("fill","#000")
           .style("font-size","10px")
           .attr("x", 10)
           .attr("y", 0);

      // Definim com seran les propietats de les arestes
      this.link = this.link.data(this.links)
                  .enter().append("line").attr("stroke-width", 2).attr("marker-end", "url(#triangle)")
                  .call(function(link) { link.transition().attr("stroke-opacity", 1); });

        this.simulation = d3.forceSimulation(this.graph.nodes)
            .force('link', d3.forceLink().links(this.links).distance(200/(this.max+1)))
            //.force('charge', d3.forceManyBody().strength(-1))
            .force("charge", d3.forceCollide().radius(15))
            .force("r", d3.forceRadial(function(d) { return scaleCircle(d.distance); }).strength(0.7))
            .alpha(1).alphaMin(0.005).velocityDecay(0.1)
            .on("tick", this.ticked);

        // Esto hay que ponerlo para que dentro de ticked se pudean ver node y link
        this.simulation.node = this.node;
        this.simulation.link = this.link;

    },
    this.dijkstra = function(src) {
        var linksDijkstra = [];
        var source = src;
        // Creamos una cola q
        var q = [];
        // Agregamos origen a la cola Q
        source.distance = 0;
        q.push(source);
        // Creamos una cola para los nodos no visitados
  //        var unvisited = [];
        // los añadimos todos excepto el src
        this.graph.nodes.forEach(function (d) {
            if (d != src) {
                d.distance = Infinity;
  //              unvisited.push(d);
                d.visited = false;
            }
        });

        //  mientras Q no este vacío:
        while (q.length != 0){
          //sacamos un elemento de la cola Q llamado v
          var v = q.shift();
          // para cada vertice w adyacente a v en el Grafo:
          var linksV = [];
          if (!this.isUndefined(this.sortedLinksByKey[this.socioType])){
            this.sortedLinksByKey[this.socioType].values.forEach(function(link){
              if (v.id == link.source.id){
                linksV.push(link);
                //console.log("v.id:"+v.id + " link.source.id:"+link.source.id + " link.target.id: "+ link.target.id);
              }
            });
          }

          var that = this;
          linksV.forEach(function(link) {
            var w = link.target;
            //console.log(w);
            //si w no ha sido visitado:
            if (!w.visited){
              // marcamos como visitado w
              w.visited = true;
              linksDijkstra.push({weight: link.weight, source: link.source, target: link.target, key: that.socioType});
              var dist = v.distance + 1;
              w.distance = Math.min(dist, w.distance);

              // insertamos w dentro de la cola Q
              q.push(w);
            }

  /*
  HAY QUE VISITAR LOS NODOS DE DONDE VIENES??????????????????????????????????????
            var x = link.source;
            //si w no ha sido visitado:
            if (!x.visited){
              // marcamos como visitado w
              x.visited = true;
              //linksDijkstra.push({weight: link.weight, source: link.source, target: link.target, key: "preference"});
              dist = v.distance + 1;
              x.distance = Math.min(dist, x.distance);
              // insertamos w dentro de la cola Q
              q.push(x);
            }
  */
          });
        }

  /*      var aux = [];
        this.graph.nodes.forEach(function (d) {
          var p = {id:d.id,distance:d.distance};
          aux.push(p);
        });
        */
        return linksDijkstra;
      };
  }
  RadialEgoDijkstraGraph.prototype = new Graph();

  //import * as d3 from "d3";

  function RadialEgoSocioGraph(options){
    this.manageOptions(options);
    this.member = options.member;
    this.max = 0;
    this.toColorArrow = new Map();
    this.toColorArrow.set("preference","blue").set("perception_preference","blue").set("perception_rejection","red").set("rejection","red");
    this.toDashArrow = new Map();
    this.toDashArrow.set("preference","0").set("perception_preference","5,5").set("perception_rejection","5,5").set("rejection","0");

    this.addMarkers = function(){
      let markers = [];
      for (var valor of this.toColorArrow.values()) {
        markers.push(valor);
      }
      this.markerSVG = this.svg.append("defs").selectAll("marker")
         .data(markers)
         .join("marker")
           .attr("id", function(d){return d;})
           .attr("viewBox", "0 -5 10 10")
           .attr("refX", 15)
           .attr("refY", -0.5)
           .attr("markerWidth", 6)
           .attr("markerHeight", 6)
           .attr("orient", "auto")
         .append("path")
           .attr("stroke-width", 3)
           .attr("fill",  function(d){return d;})
           .attr("d", "M0,-3L10,0L0,3");

    };
    this.addProperties = function(){
      d3.select("#edgesGraph").remove();
      this.link = d3.select("#centerGraph").append("g").attr("id","edgesGraph").attr("fill", "none").attr("stroke", "#888").selectAll("path");
      d3.select("#nodesGraph").remove();
      this.node = d3.select("#centerGraph").append("g").attr("id","nodesGraph").selectAll(".node");
    };
    this.allLinksMember = function(member){
      let newLinks = [];
      for(var i =0; i < 4; i++){
        for(var j =0; j < this.sortedLinksByKey[i].values.length; j++){
          if (this.sortedLinksByKey[i].values[j].source.id == member.id || this.sortedLinksByKey[i].values[j].target.id == member.id)
            newLinks.push(this.sortedLinksByKey[i].values[j]);
        }
      }
      return newLinks;
    };
    this.startSimulation = function(){
      if (!this.isUndefined(this.sortedLinksByKey[0]))
          this.links = this.allLinksMember(this.memberOnNodes(this.member));
      this.distanceNodes(this.memberOnNodes(this.member));
    };
    this.memberOnNodes = function(memberId){
      var i = 0;
      var result = this.graph.nodes[0];
      while (this.graph.nodes[i].id != memberId && i < this.graph.nodes.length){
        i++;
      }
      if (i < this.graph.nodes.length) result = this.graph.nodes[i];
      return result;
    },

    this.restart = function(){
      var that = this;
      let colorCircle = d3.scaleLinear()
      .domain([1,8])
      .range(["#3f51b5","white"]);

      this.node = this.node.data(this.graph.nodes).join("g").attr("class","node");

      // Afegim els cercles concentrics
      for (let i = 8; i >= 1 ; i--){
        this.diana.append("circle")
            .attr("r",i*(200/8))
            .attr("stroke","brown")
            .attr("stroke-opacity",0.5)
            .attr("fill",colorCircle(i));
      }

      this.circle = this.node.append("circle")
           .attr("fill", function(d) { return d.sex === "F" ? "brown" : "steelblue"; })
           .attr("stroke", "grey").attr("stroke-width", 1.5)
           .call(function(node) { node.transition().attr("r", 8); });

           this.circle.call(d3.drag()
                    .on("start", function(d){ that.dragstarted(d); })
                    .on("drag", function(d){ that.dragged(d); })
                    .on("end", function(d){ that.dragended(d); }));


       this.text = this.node.append("text")
           .text(function(d) { return d.node_id; })
        //   .text(function(d) { return d.node_id +":"+d.id; })
           .style("fill","#000")
           .style("font-size","10px")
           .attr("x", 10)
           .attr("y", 0);

           this.link = this.link.data(this.links, function(d) { return d.source.id + "-" + d.target.id; })
                       .enter().append("path").attr("stroke-width", 2)
                       .attr("stroke-dasharray",  function(d) { return that.toDashArrow.get(d.key); })
                       .attr("marker-end", function(d) { return "url(#"+that.toColorArrow.get(d.key)+")" })
                       .attr("stroke", function(d) { return that.toColorArrow.get(d.key); })
                       .call(function(link) { link.transition().attr("stroke-opacity", 1); });

        this.simulation = d3.forceSimulation(this.graph.nodes)
      //      .force('link', d3.forceLink().links(this.links))
            .force('charge', d3.forceManyBody().strength(-3))
        //    .force("charge", d3.forceCollide().radius(15))
            .force("r", d3.forceRadial(function(d) { return -(d.distance-5)*(200/8); }).strength(0.7))
            .alpha(1).alphaMin(0.005).velocityDecay(0.1)
            .on("tick", this.ticked2);

        // Esto hay que ponerlo para que dentro de ticked se pudean ver node y link
        this.simulation.node = this.node;
        this.simulation.link = this.link;

    },
    this.ticked2 = function(){
      this.node.attr("transform", d => `translate(${d.x}, ${d.y})`);

      this.link.attr("d", function(d) {
        const r = Math.hypot(d.target.x*5 - d.source.x*5, d.target.y - d.source.y);
        return "M"+d.source.x+","+d.source.y+ " A"+r+","+r+" 0 0,1 "+d.target.x+","+d.target.y;
      });


    },

    this.distanceNodes = function(src) {

      var that = this;
      this.graph.nodes.forEach(function (d) {
        d.distance = 0;
          for (let i= 0; i < 4 ; i++){
            if (that.linkedHash[`${d.id},${src.id},${i}`] || that.linkedHash[`${src.id},${d.id},${i}`]) {
              if (i == 0 || i == 1){
                d.distance++;
              } else {
                d.distance--;
              }
            }
          }
      });
      src.distance = 5;
    };
  }
  RadialEgoSocioGraph.prototype = new Graph();

  //import * as d3 from "d3";

  // Subclass to generate forced group graph.
  function ClassGraph(options){
    this.groupNum = options.groupNum || 2;
    this.classe = [];
    this.xCenter = [];
    this.yCenter = [];

    this.manageOptions(options);

    this.addProperties = function(){
        this.svg = this.svg.style("cursor","move");
    };

    // Function that starts the simulation calling startSimulation and restart.
    this.simulation = function(){
      this.startSimulation();
      this.algorithmClass();
      this.startSimulation();
      this.restart();
    };

    this.addMarkers = function(){
      // Defining Arrows
      this.markerSVG = this.svg.append("defs").selectAll("marker")
         .data(this.markersRadius)
         .join("marker")
           .attr("id", function(d){return `arrow-${(d*2 + 8)}`;})
           .attr("viewBox", "0 -5 10 10")
           .attr("refX", function(d){return d + 20;})
           .attr("refY", 0)
           .attr("markerWidth", 12)
           .attr("markerHeight", 8)
           .attr("orient", "auto")
           .append("path")
           .attr("fill", "grey")
           .attr("d", "M0,-2L20,0L0,2");
    };
    this.restart = function(){

          // Apply the general update pattern to the nodes.
          this.node = this.node.data(this.graph.nodes, function(d) { return d.node_id;});
          d3.selectAll("circle").remove();
          d3.selectAll("text").remove();

          this.node = this.node.join("g").attr("class","node");

          this.node.exit().transition()
              .attr("r", 0)
              .remove();

          this.circle = this.node.append("circle")
                   .attr("stroke-width", 1).attr("class", "nodesCircles")
                   .attr("node_id", function(d){return d.node_id; }).attr("id", function(d){return d.id; })
                   .call((node) => { node.transition().attr("r", (d) => { return this.radius(d); }); });

          var that = this;
          this.circle.call(d3.drag()
                   .on("start", function(d){ that.dragstarted(d); })
                   .on("drag", function(d){ that.dragged(d); })
                   .on("end", function(d){ that.dragended(d); }))
                   .on("dblclick", function(d){ that.dblclickAction(d); })
                   .on("mouseover", function(d){ that.set_highlight(d); })
                   .on("mouseout",  function(){ that.exit_highlight(); });

          // Es concreta el color dels nodes depenen de la variable colorFunction
          this[this.colorFunction]();

          this.text = this.node.append("text")
              .text(function(d) { return d.node_id; })
              .style("fill","#000")
              .style("font-size", "10px")
              .attr("x", 10)
              .attr("y", 0);

          this.node.merge(this.node);

          // Apply the general update pattern to the links.
          this.link = this.link.data(this.links, function(d) { return d.source.id + "-" + d.target.id; });
          // Keep the exiting links connected to the moving remaining nodes.
          // When no links and no nodes, this.link is Array. Normally it is an Object.
          if (!Array.isArray(this.link)){
              this.link.exit().transition()
                  .attr("stroke-opacity", 0)
                  .attrTween("x1", function(d) { return function() { return d.source.x; }; })
                  .attrTween("x2", function(d) { return function() { return d.target.x; }; })
                  .attrTween("y1", function(d) { return function() { return d.source.y; }; })
                  .attrTween("y2", function(d) { return function() { return d.target.y; }; })
                  .remove();
              this.link = this.link.join("line").attr("stroke-width", 1.5)
                  .call((link) => { link.transition().attr("stroke-opacity", 1); })
                  .attr("marker-end", (d) => {return `url(${new URL("#arrow-"+this.radius(d.target), location)})`;})
                  .merge(this.link);

          }


  /*        this.simulation = d3.forceSimulation(this.graph.nodes)
              .force("charge", d3.forceManyBody().strength(-85.6).distanceMin(30.4).distanceMax(207.8))
              .force("collide", d3.forceCollide().strength(80).radius(function(d) { return d.r; }))
              .force("link", d3.forceLink(this.links).id(function(d) { return d.id; }).distance(50))
                  .force("center", d3.forceCenter()).alphaMin(0.01)
                  .on("tick", this.ticked);

          // Esto hay que ponerlo para que dentro de ticked se pudean ver node y link
          this.simulation.node = this.node;
          this.simulation.link = this.link;

          // Update and restart the simulation.
          this.simulation.nodes(this.graph.nodes);
          this.simulation.force("link").links(this.links);
          this.simulation.alpha(1).restart();
  */
  console.log("Adios");
        this.simulation = d3.forceSimulation(this.graph.nodes)
           .force('x', d3.forceX().x(function(d) {
             return that.xCenter[d.categoryX];
           }))
           .force('y', d3.forceY().y(function(d) {
             return that.yCenter[d.categoryY];
           }))
           .force('collision', d3.forceCollide().radius(30))
           .alphaMin(0.0001).alphaDecay(0.002).velocityDecay(0.2)
           .on('tick', that.ticked);

           // Esto hay que ponerlo para que dentro de ticked se pudean ver node y link
           this.simulation.node = this.node;
           this.simulation.link = this.link;

    };
    this.algorithmClass = function(){
      // Possibles groups (k combinatorics)
      var possiblesGroups = this.k_combinations(this.nodeIdsGraph(), this.groupNum);
      var that = this;

      // Scoring every group
      var possibleGroupsWithPoints = [];
      possiblesGroups.forEach(function(group){
        var points = that.pointsGroup(group);
        possibleGroupsWithPoints.push({
          group: group,
          points: points
        });
      });

      // Order by Scoring
      possibleGroupsWithPoints.sort(function(a, b){return  b.points-a.points});

      // Create Classes
      var classNumber = this.graph.nodes.length;
      var notFinish = true;
      possibleGroupsWithPoints.every(function(grup){
        // Si la classe està plena o quasi plena
        if (that.classe.length > (classNumber-that.groupNum) ) notFinish = false;

        // Comprobem que no hi hagi cap element a la classe
        var trobat = false;

        grup.group.forEach(function(x){
          if (that.classe.indexOf(x) != -1) trobat = true;
        });

        // Si no hi ha cap element, l'insertem a la classe
        if (!trobat)
          that.classe = that.classe.concat(grup.group);
        return notFinish;
      });

      // Acabar d'omplir
      if (this.classe.length < classNumber){
        var nodeIds = this.nodeIdsGraph();
        this.classe.forEach(function(el){
          var aux = nodeIds.indexOf(el);
          if (aux != -1)nodeIds.splice(aux, 1);
        });
        this.classe = this.classe.concat(nodeIds);
      }

      console.log(this.classe);

      // Creating the subclasses for simulation
      var numGroups = Math.ceil(classNumber / this.groupNum);

      for (var i = 0; i < numGroups; i ++){
        this.xCenter.push(i*(this.w/numGroups)-this.w/2+60);
      }
      this.yCenter = [-this.h/4,this.h/4];

      this.graph.nodes.forEach(function(node){
        node.categoryX = Math.floor(that.classe.indexOf(node.id)/that.groupNum)*2%numGroups;
      });
      this.graph.nodes.forEach(function(node){
        node.categoryY = Math.floor(that.classe.indexOf(node.id)/(that.graph.nodes.length/2));
      });

      // Erase extra links
      // Create groups
      var groups = [];
      for(var j= 0;j<this.classe.length;j=j+this.groupNum){
        groups.push(this.classe.slice(j,j+this.groupNum));
      }

      // Copia de seguretat
      this.graph.links2 = this.graph.links.slice();

      for(var k = 0; k <this.graph.links.length;k++){
        var link = this.graph.links[k];
        if (Math.floor(this.classe.indexOf(link.source)/this.groupNum) != Math.floor(this.classe.indexOf(link.target)/this.groupNum)) {
          this.graph.links.splice(k,1);
          k--;
        }
      }
      console.log(this.graph);

    // Transformem els links en una estructura ordenada pel tipus de sociograma (preference, ....)
    // Creem un hash per saber si dos nodes estan connectats
    let edges = [];

    this.graph.links.forEach((e) => {
      let sourceNode = this.graph.nodes.filter(function(n) {return n.id === e.source;})[0];
      let targetNode = this.graph.nodes.filter(function(n) { return n.id === e.target; })[0];
      edges.push({source: sourceNode, target: targetNode, weight: e.weight, key: e.key, keyValue: this.toSocioType.get(e.key)});
      this.linkedHash[`${e.source},${e.target},${this.toSocioType.get(e.key)}`] = true;
    });

    this.sortedLinksByKey = d3.nest()
        .key(function(d) { return d.keyValue; }).sortKeys(d3.ascending)
        .entries(edges);

    };

    this.radius = function(a){
      return this.numberConnections(a)*2 + 8;
    };

    this.dblclickAction = function(d){
      alert(`x:${d.x}, y:${d.y}, id:${d.id}, node_id:${d.node_id}`);
    };
    // Return as an Array ids all the graph.nodes
    this.nodeIdsGraph = function(){
      let result = [];
      this.graph.nodes.forEach(function(t){
        result.push(t.id);
      });
      return result;
    };
    this.pointsGroup = function(nodes){
        var point = 0;
        for (var i = 0; i < nodes.length; i++){
            for (var j = i+1; j < nodes.length; j++){
              if (i != j){
                if (this.linkedHash[nodes[i] + "," + nodes[j] + ",0" ] ) point++;
                if (this.linkedHash[nodes[j] + "," + nodes[i] + ",0" ] ) point++;
                if (this.linkedHash[nodes[i] + "," + nodes[j] + ",3" ] ) point--;
                if (this.linkedHash[nodes[j] + "," + nodes[i] + ",3" ] ) point--;
              }
            }
        }
        return point;
    };

    this.k_combinations = function(set, k) {
      var i, j, combs, head, tailcombs;
      // There is no way to take e.g. sets of 5 elements from
      // a set of 4.
      if (k > set.length || k <= 0) {
        return [];
      }

      // K-sized set has only one K-sized subset.
      if (k == set.length) {
        return [set];
      }
      // There is N 1-sized subsets in a N-sized set.
      if (k == 1) {
        combs = [];
        for (i = 0; i < set.length; i++) {
          combs.push([set[i]]);
        }
        return combs;
      }
      // Assert {1 < k < set.length}
      // Algorithm description:
      // To get k-combinations of a set, we want to join each element
      // with all (k-1)-combinations of the other elements. The set of
      // these k-sized sets would be the desired result. However, as we
      // represent sets with lists, we need to take duplicates into
      // account. To avoid producing duplicates and also unnecessary
      // computing, we use the following approach: each element i
      // divides the list into three: the preceding elements, the
      // current element i, and the subsequent elements. For the first
      // element, the list of preceding elements is empty. For element i,
      // we compute the (k-1)-computations of the subsequent elements,
      // join each with the element i, and store the joined to the set of
      // computed k-combinations. We do not need to take the preceding
      // elements into account, because they have already been the i:th
      // element so they are already computed and stored. When the length
      // of the subsequent list drops below (k-1), we cannot find any
      // (k-1)-combs, hence the upper limit for the iteration:
      combs = [];
      for (i = 0; i < set.length - k + 1; i++) {
        // head is a list that includes only our current element.
        head = set.slice(i, i + 1);
        // We take smaller combinations from the subsequent elements
        tailcombs = this.k_combinations(set.slice(i + 1), k - 1);
        // For each (k-1)-combination we join it with the current
        // and store it to the set of k-combinations.
        for (j = 0; j < tailcombs.length; j++) {
          combs.push(head.concat(tailcombs[j]));
        }
      }
      return combs;
    };

  }
  ClassGraph.prototype = new Graph();

  // Function that manage all kind of graphs.

  var socio = (function() {
          // Store the graph
          var graph;
          // Stores the id of the div element where d3 will create the graph
          var svg;
          // Stores the graph type
          var graphType;
          // Map to translate graphType to idGraphType
          var fromGraphType = new Map();
          fromGraphType.set(0,"forced").set(1,"radialGroup").set(2,"radialEgo").set(3,"radialEgoDijkstra").set(4,"class");

          // Main function to create the graph. It receives the options to create the graph
          // graphType: 0: ForcedGraph, 1: RadialGroupGraph, 2: RadialEgoGraph
          // sociotype: 0: Preference, 1: Perception Preference, 2: Perception Rejection, 3: Rejection
          // svg: Id of the div element
          // w: weight of the svg element
          // h: height of the svg element
          // jsonVar: Json storing the graph
          // file: Alternative way to store the json graph via file
          // member: Id for EgoGraph
          //color: Number that defines the coloring of the nodes
          function createGraph(options){
            options.graphType = options.graphType || 0;
            svg = options.svg || "graph";
            graphType = options.graphType;

            switch(fromGraphType.get(graphType)){
                case "forced":
                  graph = new ForcedGraph(options);
                  break;
                case "radialGroup":
                  graph = new RadialGroupGraph(options);
                  break;
                case "radialEgo":
                  graph = new RadialEgoSocioGraph(options);
                  break;
                case "radialEgoDijkstra":
                  graph = new RadialEgoDijkstraGraph(options);
                  break;
                case "class":
                  graph = new ClassGraph(options);
                  break;

            }

            // The json graph var by file or by jsonVar
            if (options.file){
              d3.json(options.file).then((json) => {
                sequence(json);
              });
            } else if (options.jsonVar){
              sequence(options.jsonVar);
            }
          }

          // Sequence of the steps to create and visualize the graph. The core of the graph creation
          function sequence(json){
            graph.createSVG(svg);
            graph.addProperties();
            graph.setGraph(json);
            graph.calcEdges();
            graph.calcNodes();
            graph.addMarkers();
            graph.simulation();
          }

          // Can change the color and the socioType when the graph is already created.
          function changeParameters(options){
            graph.color = options.color !== undefined ? options.color : graph.color || 0;
            graph.socioType = options.socioType !== undefined ? options.socioType : graph.socioType || 0;

            //graph.socioType = options.socioType;
            //graph.color = options.color;
            graph.colorFunction= graph.fromColor.get(graph.color);

            if (!graph.isUndefined(graph.sortedLinksByKey[graph.socioType]))
                graph.links = graph.sortedLinksByKey[graph.socioType].values;
            graph.restart();
          }
          // Can change the forces that are interacting
          function changeAvailables(availables){
            if (graph){
              graph.setAvailabilityForces(availables);
              graph.updateForces();
            }
          }

          // Getter for graphType
          function getGraphType(){
            return graphType;
          }
          // Function that changes the charge force of the graph.
          function setForces(forces){
            if (graph){
              graph.setForces(forces);
              graph.updateForces();
            }
          }
          //Function that restart the forces to the original values.
          function restartForces(){
            if (graph){
              graph.restartForces();
              graph.updateForces();
            }
          }

          //Function that enables to restart the simulation.
          function restart(){
            if (graph){
              graph.restart();
            }
          }

          //Function that switch between enable and disable the forces in the graph.
          function forceSwitch(){
            if (graph){
              graph.forceSwitch();
            }
          }
          //Function that change between enable and disable the autoCenter in the graph.
          function autoCenter(b){
            if (graph){
              graph.autoCenter(b);
            }
          }

          // Functions that are public for the object socio
          return {
            createGraph: createGraph,
            changeParameters: changeParameters,
            changeAvailables: changeAvailables,
            getGraphType: getGraphType,
            setForces: setForces,
            restartForces: restartForces,
            restart: restart,
            forceSwitch: forceSwitch,
            autoCenter: autoCenter,
          };
  })();
  //var test = (function Graph(){
  //  var g = socio;

  //  function getGraph(){
  //    return g;
  //  }
  //  return {
  //    getGraph: getGraph,
  //  };

  //})();

  return socio;

}));
