from PySide6.QtSql import QSqlQuery


class ResponseModel(object):
    def __init__(self, db, testId):
        self.db = db
        self.testId = testId

    def getResponses(self):
        query = QSqlQuery(self.db)
        query.prepare(
            ('select r.member_id, r.choice, q.category, q.criterion '
             'from responses r, questions q '
             'where r.test_id = :testId AND '
             'q.id = r.question_id'))
        query.bindValue(":testId", self.testId)
        if not query.exec():
            raise Exception("Could not execute select")
        responses = []
        while query.next():
            r = {"member_id": query.value(0),
                 "choice": query.value(1),
                 "category": query.value(2),
                 "criterion": query.value(3)}

            responses.append(r)
        return responses
