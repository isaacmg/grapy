from PySide6.QtSql import QSqlQuery
from PySide6.QtSql import QSqlTableModel


class MemberModel(object):
    def getMembers(self, db, idGroup):
        self.db = db
        self.groupId = idGroup
        self.model = QSqlTableModel(db=self.db)
        self.model.setTable('members')
        self.model.setFilter("group_id = %d" % idGroup)
        self.model.setEditStrategy(QSqlTableModel.OnManualSubmit)
        self.model.select()
        return self.model

    def getMembersSets(self):
        query = QSqlQuery(self.db)
        query.prepare(
            "SELECT name, surname, abbreviation FROM members WHERE group_id = :groupId")
        query.bindValue(":groupId", self.groupId)
        if not query.exec():
            raise Exception("Could not execute select")
        members = MemberSets()
        while query.next():
            members.nameSurname.add(query.value(0)+query.value(1))
            members.abbreviation.add(query.value(2))
        return members

    def getMembersSetsUpdated(self, notIncluded):
        query = QSqlQuery(self.db)
        query.prepare(
            "SELECT name, surname, abbreviation FROM members WHERE group_id = :groupId AND id != :idUpdated")
        query.bindValue(":groupId", self.groupId)
        query.bindValue(":idUpdated", notIncluded)
        if not query.exec():
            raise Exception("Could not execute select")
        members = MemberSets()
        while query.next():
            members.nameSurname.add(query.value(0)+query.value(1))
            members.abbreviation.add(query.value(2))
        return members
    def checkInsertMembersUpload(self, membersSet, row):
        sex = {'F', 'M', 'O'}
        # Checking that member's name and surname is not already in the member's table
        nameSurname = row[1]+row[2]
        if nameSurname in membersSet.nameSurname:
            raise Exception("Name and Surname already in the group")
        else:
            membersSet.nameSurname.add(nameSurname)
        if not row[4]:
            row[4] = 40

        # Sex has only three possible values
        if not row[5] in sex:
            raise Exception("F, M or O are the only possible values for sex")

    def checkInsertMembers(self, membersSet, row):
        sex = {'F', 'M', 'O'}
        # Checking that member's name and surname is not already in the member's table
        nameSurname = row[1]+row[2]
        if nameSurname in membersSet.nameSurname:
            raise Exception("Name and Surname already in the group")
        else:
            membersSet.nameSurname.add(nameSurname)

        # some abbreviations repeats
        if row[3] in membersSet.abbreviation:
            raise Exception("Abbreviation already in the group")
        else:
            membersSet.abbreviation.add(row[3])

        # Some fields are incorrect.
        if not row[0] or not row[4] or not row[5]:
            raise Exception("Some fields are empty")

        # Sex has only to possible values
        if not row[5] in sex:

            raise Exception("F, M or O are the only possible values for sex")


class MemberSets:
    def __init__(self):
        self.nameSurname = set()
        self.abbreviation = set()
