from PySide6.QtSql import QSqlQueryModel, QSqlQuery


class ParticipantModel(object):
    def __init__(self, db, testId):
        self.db = db
        self.testId = testId

    def getParticipantsAnswered(self):
        self.model = QSqlQueryModel()
        self.model.setQuery("select m.id, m.name, m.surname, m.email, r.code, COUNT(re.id) FROM responsecodes r, members m LEFT JOIN responses re  ON re.test_id = r.test_id AND re.member_id = r.member_id  WHERE r.test_id=%1 AND  m.id = r.member_id GROUP BY r.member_id HAVING COUNT(re.id) >0".replace("%1", str(self.testId)), self.db)
        return self.model

    def getParticipantsNotAnswered(self):
        self.model = QSqlQueryModel()
        self.model.setQuery("select m.id, m.name, m.surname, m.email, r.code, COUNT(re.id) FROM responsecodes r, members m LEFT JOIN responses re  ON re.test_id = r.test_id AND re.member_id = r.member_id  WHERE r.test_id=%1 AND  m.id = r.member_id GROUP BY r.member_id HAVING COUNT(re.id) == 0".replace("%1", str(self.testId)), self.db)
        return self.model

    def getParticipantNotAnsweredSet(self):
        query = QSqlQuery(self.db)
        query.prepare(
            ('select m.id, m.name, m.surname, m.email, r.code, COUNT(re.id) '
             'FROM responsecodes r, members m '
             'LEFT JOIN responses re  '
             'ON re.test_id = r.test_id AND re.member_id = r.member_id  '
             'WHERE r.test_id=:testId AND  m.id = r.member_id '
             'GROUP BY r.member_id HAVING COUNT(re.id) == 0'))
        query.bindValue(":testId", self.testId)
        if not query.exec():
            raise Exception("Could not execute select")
        participants = set()
        while query.next():
            p = Participant(query.value(0), query.value(1),
                            query.value(2), query.value(3), query.value(4))
            participants.add(p)
        return participants

    def getParticipantsExceptWhoIsAnswering(self, choosenOne):
        query = QSqlQuery(self.db)
        query.prepare(
            ('select m.id, m.name, m.surname, m.email, r.code, COUNT(re.id) '
             'FROM responsecodes r, members m '
             'LEFT JOIN responses re  '
             'ON re.test_id = r.test_id AND re.member_id = r.member_id  '
             'WHERE r.test_id=:testId AND  m.id = r.member_id AND '
             'm.id !=:choosenId '
             'GROUP BY r.member_id '))
        query.bindValue(":testId", self.testId)
        query.bindValue(":choosenId", choosenOne.id)
        if not query.exec():
            raise Exception("Could not execute select")
        participants = []
        while query.next():
            p = Participant(query.value(0), query.value(1),
                            query.value(2), query.value(3), query.value(4))
            participants.append(p)
        return participants

    def getParticipantAnsweredDict(self):
        query = QSqlQuery(self.db)
        query.prepare(
            ('select m.id, m.name, m.surname, m.email, r.code, m.abbreviation, m.sex, m.age, COUNT(re.id) '
             'FROM responsecodes r, members m '
             'LEFT JOIN responses re  '
             'ON re.test_id = r.test_id AND re.member_id = r.member_id  '
             'WHERE r.test_id=:testId AND  m.id = r.member_id '
             'GROUP BY r.member_id HAVING COUNT(re.id) > 0'))
        query.bindValue(":testId", self.testId)
        if not query.exec():
            raise Exception("Could not execute select")
        participants = []
        while query.next():
            p = {"id": query.value(0),
                 "name": query.value(1),
                 "surname": query.value(2),
                 "email": query.value(3),
                 "code": query.value(4),
                 "abbreviation": query.value(5),
                 "sex": query.value(6),
                 "age": query.value(7)}

            participants.append(p)
        return participants

    def getParticipantsDict(self):
        query = QSqlQuery(self.db)
        query.prepare(
            ('select m.id, m.name, m.surname, m.email, r.code, m.abbreviation, m.sex, m.age '
             'FROM responsecodes r, members m '
             'WHERE r.test_id=:testId AND  m.id = r.member_id '))
        query.bindValue(":testId", self.testId)
        if not query.exec():
            raise Exception("Could not execute select")
        participants = []
        while query.next():
            p = {"id": query.value(0),
                 "name": query.value(1),
                 "surname": query.value(2),
                 "email": query.value(3),
                 "code": query.value(4),
                 "abbreviation": query.value(5),
                 "sex": query.value(6),
                 "age": query.value(7)}

            participants.append(p)
        return participants

class Participant:
    def __init__(self, id, name, surname, email, answered):
        self.id = id
        self.name = name
        self.surname = surname
        self.email = email
        self.answered = answered
