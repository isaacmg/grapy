from PySide6.QtSql import QSqlQuery
from PySide6.QtSql import QSqlTableModel


class TestModel(object):
    def getTests(self, db, idGroup):
        self.db = db
        self.groupId = idGroup
        self.model = QSqlTableModel(db=self.db)
        self.model.setTable('tests')
        self.model.setFilter("group_id = %d" % idGroup)
        self.model.setEditStrategy(QSqlTableModel.OnManualSubmit)
        self.model.select()
        return self.model

    def getTestsSets(self):
        query = QSqlQuery(self.db)
        query.prepare(
            "SELECT name FROM tests WHERE group_id = :groupId")
        query.bindValue(":groupId", self.groupId)
        if not query.exec():
            raise Exception("Could not execute select")
        members = TestSets()
        while query.next():
            members.name.add(query.value(0))
        return members

    def getTestsSetsUpdated(self, notIncluded):
        query = QSqlQuery(self.db)
        query.prepare(
            "SELECT name FROM tests WHERE group_id = :groupId AND id != :idUpdated")
        query.bindValue(":groupId", self.groupId)
        query.bindValue(":idUpdated", notIncluded)
        if not query.exec():
            raise Exception("Could not execute select")
        members = TestSets()
        while query.next():
            members.name.add(query.value(0))
        return members

    def checkInsertTests(self, testsSet, row):
        # Checking that member's name and surname is not already in the member's table
        name = row[0]
        if name in testsSet.name:
            raise Exception("Name already in the group")
        else:
            testsSet.name.add(name)

        # Some fields are incorrect.
        if not row[0]:
            raise Exception("Some fields are empty")

    def insertResponseCodes(self, testId):
        # Insering the active members in the responsecodes
        query = QSqlQuery(self.db)

        query.prepare(
            "INSERT INTO responsecodes (test_id, member_id, code) SELECT :testId, m.id, 'hola' FROM members m WHERE m.group_id = :groupId AND active = 1")
        query.bindValue(":groupId", self.groupId)
        query.bindValue(":testId", testId)

        if not query.exec():
            raise Exception("Could not execute Insert")


class TestSets:
    def __init__(self):
        self.name = set()
