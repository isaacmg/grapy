from PySide6.QtWidgets import (QMessageBox)
import pyAesCrypt
from PySide6.QtCore import Slot
import tempfile
from db import Db
from os.path import isfile, getsize
import shutil
import os

class FileManagement(object):
    def __init__(self):
        self.path = None
        self.temp = None
        self.isEditing = False
        # create DataBase
        self.db = Db()
        f = open("password.txt", "r")
        self.password = f.read()
        #print(self.password)
        # encrypt
        #pyAesCrypt.encryptFile("survey.db", "survey.sog", self.password)

    def createNewFile(self):
        # Copy survey.db.sql to temporaryFile
        self.createTempFile()
        # Rellenarlo con la BBDD de ejemplo
        self.fillTempFile("db/test.db")

    def createNewTempFile(self,path):
        # Copy survey.db.sql to temporaryFile
        self.createTempFile()
        # Rellenarlo con la BBDD de ejemplo
        self.fillTempFile(path)

    def createTempFile(self):
        self.temp = tempfile.NamedTemporaryFile()

    def fillTempFile(self, path):
        shutil.copy2(path, self.temp.name)

    def fillFile(self, pathOrig, pathDest):
        pyAesCrypt.encryptFile(pathOrig, pathDest, self.password)
        #shutil.copy2(pathOrig, pathDest)

    def getTempFileName(self):
        if self.temp is None:
            return None
        else:
            return self.temp.name

    def putDatabase(self, name):
        return self.db.putDatabase(name)

    def putDatabaseTemp(self):
        return self.db.putDatabase(self.temp.name)

    def isAFile(self, filename):
        if not os.path.isfile(filename):
            raise OSError("File Not Found")

    def isCorrectFile(self, path):
        # decrypt and check if it is a sqlLite file
        pyAesCrypt.decryptFile(path, path+".tmp", self.password)
        self.isSQLite3(path+".tmp")
        # creem un arxiu temporal, que s'ha guardat a self.fm.temp
        self.createNewTempFile(path+".tmp")
        # Borramos el fichero .tmp creado para comprobar el fichero
        #if os.path.exists(path+".tmp"):
        #    os.remove(path+".tmp")
        #else:
        #    print("The file does not exist")

    def isSQLite3(self, filename):
        a = True

        if getsize(filename) < 100: # SQLite database file header is 100 bytes
            a = False
        else:
            with open(filename, 'rb') as fd:
                header = fd.read(100)

            a = header[:16] == b'SQLite format 3\x00'

        if not a:
            raise OSError("File type is incorrect. Can not read this file. ")

    def needsSave(self):
        # Comprovar abans si hi ha un altre fitxer editant-se.
        if self.isEditing:
            button = QMessageBox.question(self, "Save tha actual file?",
                              "The actual file is modified. Do you want to save it?")
            if button == QMessageBox.Yes:
                return True;
        return False;

    @Slot()
    def modifiedFile(self):
        self.isEditing = True
