from PySide6.QtGui import QAction, QIcon, QKeySequence
import sys
import os
from PySide6.QtWidgets import (QApplication, QMainWindow,
                               QStackedWidget, QMessageBox, QFileDialog)
from PySide6.QtCore import (QLibraryInfo, QLocale, QTranslator)
from PySide6.QtUiTools import QUiLoader

from FileManagement import FileManagement
from db import Db
from StartView import StartView
from AnswerView import AnswerView
from AnsweringView import AnsweringView
from GroupView import GroupView
from MemberTestView import MemberTestView
from ParticipantView import ParticipantView
from ResultsViewD3 import ResultsViewD3
from Waiting import Waiting


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        # Settings
        # settings = QSettings()
        self.trans = QTranslator(self)

        # Finding how many languages we have
        self.languages = self.findingLanguages()
        print(self.languages)

        # File management
        self.fm = FileManagement()

        # Database
        self.db = self.fm.db;

        # CREATION OF THE DIFFERENT VIEWS
        # startView
        self.start = StartView()
        self.startView = self.start.getView()

        # ResultsView
        self.results = ResultsViewD3()
        self.resultView = self.results.getView()

        # ParticipantView
        self.participant = ParticipantView()
        self.participantView = self.participant.getView()

        # AnswerView
        self.answer = AnswerView()
        self.answerView = self.answer.getView()

        # AnsweringView
        self.answering = AnsweringView()
        self.answeringView = self.answering.getView()

        # MembersTestsView
        self.memberTest = MemberTestView()
        self.memberTestView = self.memberTest.getView()

        # Group View & Model
        self.groups = GroupView()
        self.groupsView = self.groups.getView()

        # waiting
        self.waiting = Waiting()

        self.stackedWidget = QStackedWidget()
        self.stackedWidget.addWidget(self.groupsView)
        self.stackedWidget.addWidget(self.memberTestView)
        self.stackedWidget.addWidget(self.answerView)
        self.stackedWidget.addWidget(self.answeringView)
        self.stackedWidget.addWidget(self.participantView)
        self.stackedWidget.addWidget(self.resultView)
        self.stackedWidget.addWidget(self.startView)
        self.stackedWidget.addWidget(self.waiting.view)

        self.stackedWidget.setCurrentWidget(self.startView)
        self.setCentralWidget(self.stackedWidget)

        # SET ==> WINDOW TITLE
        self.setWindowTitle('Grapy')

        self.create_actions()
        self.create_menus()
        self.create_connections()

        # Store the language
        self.lang = "en"

        self.statusBar().showMessage(self.tr("Ready"))

    def create_connections(self):
        self.groupsView.tableGroups.doubleClicked.connect(self.doubleClick)
        self.groupsView.openButton.clicked.connect(self.doubleClick)
        self.memberTestView.returnButtonMembers.clicked.connect(self.returnButton)
        self.memberTestView.answerButtonTests.clicked.connect(self.answerConnect)
        self.memberTestView.participantsButtonTests.clicked.connect(self.participantsButtonTests)
        self.memberTestView.resultsButtonTests.clicked.connect(self.resultsButtonTests)
        self.participantView.returnToTestButton.clicked.connect(self.returntoTestView)
        self.answerView.startAnsweringButton.clicked.connect(self.startAnswering)
        self.answerView.returnToTestButton.clicked.connect(self.returntoTestView)
        self.answeringView.returnToTestButton.clicked.connect(self.returntoTestView)
        self.resultView.returnButtonMembers.clicked.connect(self.returntoTestView)

        # Signals for modified file
        self.groups.modified.sign.connect(self.modifiedFile)
        self.answering.modified.sign.connect(self.modifiedFile)
        self.memberTest.modified.sign.connect(self.modifiedFile)
        self.results.modified.sign.connect(self.changeWidget)

        #Signal for finishing answering
        self.answering.finished.sign.connect(self.finishedAnswering)

    def finishedAnswering(self):
        self.choosenOne = self.answer.startAnswering()
        self.stackedWidget.setCurrentWidget(self.answerView)

    def modifiedFile(self):
        self.fm.modifiedFile()
        self.statusBar().showMessage(self.tr("File was modified"))

    def resultsButtonTests(self):
        if self.memberTestView.tableTests.selectionModel().selection().count() == 0:
            QMessageBox.warning(self, self.tr("No test selected"),
                                self.tr("To answer a test, you have to select one test."))
        else:
            self.stackedWidget.setCurrentWidget(self.waiting.view)
            curRow = self.memberTestView.tableTests.currentIndex().row()
            record = self.memberTest.modelTableTests.record(curRow)
            self.testId = record.value(0)
            self.testName = record.value(4)
            #self.stackedWidget.setCurrentWidget(self.resultView)
            self.results.loadGraphs(self.db.getConnection(), self.groupName, self.testId, self.testName)

    def changeWidget(self):
        self.stackedWidget.setCurrentWidget(self.resultView)

    def startAnswering(self):
        self.stackedWidget.setCurrentWidget(self.answeringView)
        self.answering.loadQuestions(self.db.getConnection(),self.lang)
        self.answering.loadParticipants(self.db.getConnection(), self.testId, self.choosenOne)

    def returntoTestView(self):
        self.stackedWidget.setCurrentWidget(self.memberTestView)

    def participantsButtonTests(self):
        if self.memberTestView.tableTests.selectionModel().selection().count() == 0:
            QMessageBox.warning(self, self.tr("No test selected"),
                                self.tr("To answer a test, you have to select one test."))
        else:
            curRow = self.memberTestView.tableTests.currentIndex().row()
            record = self.memberTest.modelTableTests.record(curRow)
            self.testId = record.value(0)
            self.testName = record.value(4)

            self.participant.loadParticipants(self.db.getConnection(), self.groupName, self.testId, self.testName)
            self.stackedWidget.setCurrentWidget(self.participantView)

    def answerConnect(self):
        if self.memberTestView.tableTests.selectionModel().selection().count() == 0:
            QMessageBox.warning(self, self.tr("No test selected"),
                                self.tr("To answer a test, you have to select one test."))
        else:
            curRow = self.memberTestView.tableTests.currentIndex().row()
            record = self.memberTest.modelTableTests.record(curRow)
            self.testId = record.value(0)
            self.testName = record.value(4)

            if self.answer.loadAnswers(self.groupIndex, self.groupName,
                                       self.testId, self.testName, self.db.getConnection()):
                self.stackedWidget.setCurrentWidget(self.answerView)
                self.choosenOne = self.answer.startAnswering()
            else:
                QMessageBox.information(self.view, self.tr("No more participants"),
                                        self.tr("All the participans have answered the survey."))

    def returnButton(self):
        self.stackedWidget.setCurrentWidget(self.groupsView)

    def doubleClick(self, index):
        if self.groupsView.tableGroups.selectionModel().selection().count() == 0:
            QMessageBox.warning(self, self.tr("No group selected"),
                                self.tr("To open a group, you have to select one group."))
        else:
            curRow = self.groupsView.tableGroups.currentIndex().row()
            updateRecord = self.groups.modelTable.record(curRow)
            self.groupIndex = updateRecord.value(0)
            self.groupName = updateRecord.value(1)

            self.stackedWidget.setCurrentWidget(self.memberTestView)
            self.memberTest.loadGroup(updateRecord, self.db.getConnection())
            self.memberTest.loadTests(updateRecord, self.db.getConnection())

    def closeEvent(self, event):
        # Comprovar abans si hi ha un altre fitxer editant-se.
        if self.fm.isEditing:
            button = QMessageBox.question(self, self.tr("Save tha actual file?"),
                              self.tr("The actual file is modified. Do you want to save it?"),
                              buttons=QMessageBox.Discard | QMessageBox.Save | QMessageBox.Cancel,
                              defaultButton=QMessageBox.Discard)
            if button == QMessageBox.Save:
                self.save()
            if button == QMessageBox.Cancel:
                return
        event.accept()

    def new_file(self):
        # Comprovar abans si hi ha un altre fitxer editant-se.
        if self.fm.isEditing:
            button = QMessageBox.question(self, self.tr("Save tha actual file?"),
                              self.tr("The actual file is modified. Do you want to save it?"),
                              buttons=QMessageBox.Discard | QMessageBox.Save | QMessageBox.Cancel,
                              defaultButton=QMessageBox.Discard)
            if button == QMessageBox.Save:
                self.save()
            if button == QMessageBox.Cancel:
                return

        self.fm.createNewFile()
        if (self.fm.putDatabaseTemp()):
            # Load the first view when file is charged
            self.groups.loadGroups(self.db)
            self.stackedWidget.setCurrentWidget(self.groupsView)
            self.update_title(self.tr("Unnamed"))
            self.statusBar().showMessage(self.tr("New File"))

            # Starting variable from fm
            self.fm.isEditing = False
            self.fm.path = None

    def open(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        path,_ = QFileDialog.getOpenFileName(self, self.tr("Select File to Open"), "", self.tr("Socio Graph(*.sog );;All Files (*.*)"), options=options)

        # Si ha acceptat el botó amb un fitxer correcte
        if path:
            # Comprovar abans si hi ha un altre fitxer editant-se.
            if self.fm.isEditing:
                button = QMessageBox.question(self, self.tr("Save tha actual file?"),
                                  self.tr("The actual file is modified. Do you want to save it?"),
                                  buttons=QMessageBox.Discard | QMessageBox.Save | QMessageBox.Cancel,
                                  defaultButton=QMessageBox.Discard)
                if button == QMessageBox.Save:
                    self.save()
                if button == QMessageBox.Cancel:
                    return
            # Obrir el nou fitxer
            try:
                self.fm.isAFile(path)
                self.fm.isCorrectFile(path)

                if (self.fm.putDatabaseTemp()):
                    # Load the first view when file is charged
                    self.groups.loadGroups(self.db)
                    self.stackedWidget.setCurrentWidget(self.groupsView)
                    self.fm.path = path
                    self.fm.isEditing = False
                    self.statusBar().showMessage(self.tr("File Opened"))
                    self.update_title(path)
                else:
                    self.dialog_critical(self.tr("Incorrect File"))
            except Exception as e:
                self.dialog_critical(str(e))
        else:
            # If dialog is cancelled, will return ''
            return

    def save(self):
        if self.fm.path is None:
            # If we do not have a path, we need to use Save As.
            return self.saveAs()

        # Encriptamos y guardamos, y borramos el fichero temporal
        self.saveToPath(self.fm.path)

    def saveAs(self):
        path, _ = QFileDialog.getSaveFileName(self, self.tr("Save file"), "", self.tr("Grapy Documents (*.sog);;All files (*.*)"))
        if not path:
            # If dialog is cancelled, will return ''
            return
        # if not has .sg extension, add it in the path
        if path.find(".sog", len(path)-4, len(path)) == -1:
            path = path + ".sog"

        self.saveToPath(path)

    def saveToPath(self, path):
        try:
            self.fm.fillFile(self.fm.temp.name, path)

        except Exception as e:
            self.dialog_critical(str(e))

        else:
            self.fm.path = path
            self.update_title(path)
            self.fm.isEditing = False
            self.statusBar().showMessage(self.tr("Saved"))


    def about(self):
        QMessageBox.about(self, "About Application",
                          "The <b>Application</b> example demonstrates how to write "
                          "modern GUI applications using Qt, with a menu bar, "
                          "toolbars, and a status bar.")

    def dialog_critical(self, s):
        dlg = QMessageBox(self)
        dlg.setText("Error:"+ s)
        dlg.setIcon(QMessageBox.Critical)
        dlg.show()

    def update_title(self, path):
        self.setWindowTitle("%s - Grapy" % (os.path.basename(path) if path else self.tr("Untitled")))

    def findingLanguages(self):
        # Iterate directory
        languages = []
        for file_path in os.listdir("translations"):
            # check if current file_path is a file
            if file_path.endswith('.qm'):
                # add filename to list
                languages.append(file_path.split('.qm')[0])
        return languages

    def change_es(self):
         self.trans.load("es","translations")
         QApplication.instance().installTranslator(self.trans)
         self.changeStaticMessages()
         self.lang = "es"

    def change_en(self):
         QApplication.instance().removeTranslator(self.trans)
         self.changeStaticMessages()
         self.lang = "en"

    def changeLanguage(self, index):
         self.trans.load(self.languages[index],"translations")
         QApplication.instance().installTranslator(self.trans)
         self.changeStaticMessages()
         self.lang = self.languages[index]

    def changeStaticMessages(self):
        # Erasing the menu, and recreat it
         self.menuBar().clear()
         self._file_menu.clear()
         self.create_actions()
         self.create_menus()
         # Change table changeHeaders
         self.groups.changeHeaders()
         if hasattr(self.memberTest, 'modelTableMembers'):
             self.memberTest.changeHeaders()

    def create_actions(self):
        self._languages = []
        icon = QIcon.fromTheme("document-new", QIcon(':/images/new.png'))
        self._new_act = QAction(icon, QApplication.translate("MainWindow","&New"), self, shortcut=QKeySequence.New,
                                statusTip=self.tr("Create a new file"), triggered=self.new_file)

        icon = QIcon.fromTheme("document-open", QIcon(':/images/open.png'))
        self._open_act = QAction(icon, self.tr("&Open..."), self,
                                 shortcut=QKeySequence.Open, statusTip=self.tr("Open an existing file"),
                                 triggered=self.open)

        icon = QIcon.fromTheme("document-save", QIcon(':/images/save.png'))
        self._save_act = QAction(icon, self.tr("&Save"), self,
                                 shortcut=QKeySequence.Save,
                                 statusTip=self.tr("Save the document to disk"), triggered=self.save)

        self._save_as_act = QAction(self.tr("Save &As..."), self,
                                    shortcut=QKeySequence.SaveAs,
                                    statusTip=self.tr("Save the document under a new name"),
                                    triggered=self.saveAs)

        self._exit_act = QAction(self.tr("E&xit"), self, shortcut="Ctrl+Q",
                                 statusTip=self.tr("Exit the application"),
                                 triggered=self.close)

        self._about_act = QAction(self.tr("&About"), self,
                                  statusTip=self.tr("Show the application's About box"),
                                  triggered=self.about)

        self._about_qt_act = QAction(self.tr("About &Qt"), self,
                                     statusTip=self.tr("Show the Qt library's About box"),
                                     triggered=qApp.aboutQt)
        self._change_en = QAction(self.tr("&English"), self,
                                     statusTip=self.tr("Change the language to English"),
                                     triggered=self.change_en)

        for index, language in enumerate(self.languages):
            self._languages.append(QAction(language, self,
                                         statusTip=self.tr("Change the language to: " + language),
                                         triggered=lambda index=index: self.changeLanguage(index)))

    def create_menus(self):
        self._file_menu = self.menuBar().addMenu(self.tr("&File"))
        self._file_menu.addAction(self._new_act)
        self._file_menu.addAction(self._open_act)
        self._file_menu.addAction(self._save_act)
        self._file_menu.addAction(self._save_as_act)
        self._file_menu.addSeparator()
        self._file_menu.addAction(self._exit_act)

        self.menuBar().addSeparator()

        self._settings_menu = self.menuBar().addMenu(self.tr("&Settings"))
        self._languages_menu = self._settings_menu.addMenu(self.tr("&Languages"))
        self._languages_menu.addAction(self._change_en)

        for index, language in enumerate(self.languages):
            self._languages_menu.addAction(self._languages[index])


        self.menuBar().addSeparator()

        self._help_menu = self.menuBar().addMenu(self.tr("&Help"))
        self._help_menu.addAction(self._about_act)
        self._help_menu.addAction(self._about_qt_act)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setStyle('Fusion')

    window = MainWindow()
    window.show()
    sys.exit(app.exec())
