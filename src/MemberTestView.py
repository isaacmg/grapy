import sys
import csv
import os

from PySide6.QtCore import QFile, QIODevice,QObject, Qt, QDateTime
from PySide6.QtUiTools import QUiLoader
from PySide6.QtWidgets import (QAbstractItemView, QAbstractScrollArea,
                               QMessageBox, QFileDialog)

from datetime import datetime
from TestModel import TestModel
from MemberModel import MemberModel
from Communicate import Communicate


class MemberTestView(QObject):
    def __init__(self):
        # combobox Values-Text Dictionary.
        self.comboBoxMembersDict = {
            "F": "Female",
            "M": "Male",
            "O": "Other"
        }
        # checkbox Values-numbers Array.
        self.checkBoxMembersDict = [Qt.Unchecked, Qt.Checked, Qt.Checked]
        self.cbMembers = [0, 1, 1]

        # List of abbreviations
        self.abr = {}

        ui_file_name = "ui/groupAdm.ui"
        ui_file = QFile(ui_file_name)
        if not ui_file.open(QIODevice.ReadOnly):
            print(f"Cannot open {ui_file_name}: {ui_file.errorString()}")
            sys.exit(-1)
        loader = QUiLoader()
        loader.setLanguageChangeEnabled(True)
        self.view = loader.load(ui_file)
        ui_file.close()
        if not self.view:
            print(loader.errorString())
            sys.exit(-1)
        # Connect
        self.connect()
        self.modified = Communicate()

        # Set properties of the two views in the Widget
        self.setViewProperties(self.view.tableMembers)
        self.setViewProperties(self.view.tableTests)

    def getView(self):
        return self.view

    def loadGroup(self, recordGroup, db):
        self.modelMembers = MemberModel()

        self.groupId = recordGroup.value(0)

        #Creating the list of abbreviations for this group in particular
        self.abr[self.groupId] = {}

        self.modelTableMembers = self.modelMembers.getMembers(
            db, self.groupId)
        self.changeHeadersMembers()

        self.view.groupNameLabel.setText(" " + recordGroup.value(1))
        # self.groupId = recordGroup.value(0)

        self.view.tableMembers.setModel(self.modelTableMembers)
        self.view.tableMembers.hideColumn(0)
        self.view.tableMembers.hideColumn(7)
        self.view.tableMembers.hideColumn(9)
        self.view.tableMembers.horizontalHeader().swapSections(3,4)
        self.view.tableMembers.horizontalHeader().swapSections(3,8)

    def changeHeaders(self):
        self.changeHeadersMembers()
        self.changeHeadersTest()

    def changeHeadersMembers(self):
        self.modelTableMembers.setHeaderData(0, Qt.Horizontal, "Id")
        self.modelTableMembers.setHeaderData(1, Qt.Horizontal, self.tr("Name"))
        self.modelTableMembers.setHeaderData(2, Qt.Horizontal, self.tr("Surname"))
        self.modelTableMembers.setHeaderData(3, Qt.Horizontal, self.tr("Genre"))
        self.modelTableMembers.setHeaderData(4, Qt.Horizontal, self.tr("Age"))
        self.modelTableMembers.setHeaderData(5, Qt.Horizontal, self.tr("Active"))
        self.modelTableMembers.setHeaderData(6, Qt.Horizontal, self.tr("Email"))
        self.modelTableMembers.setHeaderData(8, Qt.Horizontal, self.tr("Abbreviation"))

    def loadTests(self, recordGroup, db):
        self.modelTests = TestModel()
        self.modelTableTests = self.modelTests.getTests(
            db, self.groupId)
        self.changeHeadersTest()

        self.view.tableTests.setModel(self.modelTableTests)
        self.view.tableTests.hideColumn(0)
        self.view.tableTests.hideColumn(2)
        self.view.tableTests.hideColumn(3)
        self.view.tableTests.horizontalHeader().swapSections(1,4)

        self.view.tableTests.show()

    def changeHeadersTest(self):
            self.modelTableTests.setHeaderData(0, Qt.Horizontal, "Id")
            self.modelTableTests.setHeaderData(1, Qt.Horizontal, self.tr("Created_at"))
            self.modelTableTests.setHeaderData(2, Qt.Horizontal, "Group_Id")
            self.modelTableTests.setHeaderData(3, Qt.Horizontal, "Survey_Id")
            self.modelTableTests.setHeaderData(4, Qt.Horizontal, self.tr("Name"))

    def connect(self):
        # ViewMembers
        self.view.tableMembers.clicked.connect(self.selectRowMember)
        self.view.addButtonMember.clicked.connect(self.addButtonMember)
        self.view.updateButtonMember.clicked.connect(self.updateButtonMember)
        self.view.uploadButtonMember.clicked.connect(self.uploadButtonMember)
        self.view.deleteButtonMember.clicked.connect(self.deleteButtonMember)

        # ViewTests
        self.view.tableTests.clicked.connect(self.selectRowTests)
        self.view.addButtonTests.clicked.connect(self.addButtonTests)
        self.view.updateButtonTests.clicked.connect(self.updateButtonTests)
        self.view.deleteButtonTests.clicked.connect(self.deleteButtonTests)

    def generateAbbreviation(self, name, surname, abbreviation):
        # Si no tenemos abbreviation o la abbreviación ya está en la lista
        newAbr = abbreviation
        if len(abbreviation) == 0 or self.abr[self.groupId].get(abbreviation) is not None:
            # Tenemos que crear una abbreviación
            number = 1;
            newAbr = ''.join(word[0] for word in (name+" "+surname).split())
            while (self.abr[self.groupId].get((newAbr+str(number))) is not None):
                number += 1
            self.abr[self.groupId][newAbr+str(number)] = 1
            return newAbr+str(number)
        else:
            self.abr[self.groupId][newAbr] = 1
            return newAbr

    def uploadButtonMember(self):
        fname = QFileDialog.getOpenFileName(self.view, self.tr('Open profile'), '.',
                                            filter=('CSV Files (*.csv)'))[0]
        if os.path.isfile(fname):
            # Manage the CSV file
            try:
                membersSet = self.modelMembers.getMembersSets()

                with open(fname, newline='') as csvfile:
                    spamreader = csv.reader(csvfile, delimiter=',', quotechar='"')
                    for row in spamreader:
                        # Scaping the header row
                        if not row[1] == "Name":
                            # Generating abbreviation from name, and surname
                            abbr = self.generateAbbreviation(row[1],row[2],row[3])
                            self.modelMembers.checkInsertMembersUpload(membersSet, row)

                            newRecord = self.modelTableMembers.record()
                            newRecord.remove(newRecord.indexOf("id"))
                            newRecord.setValue("name", row[1])
                            newRecord.setValue("surname", row[2])
                            newRecord.setValue("sex", row[5])
                            newRecord.setValue("age", row[4])
                            newRecord.setValue("active", 1)
                            newRecord.setValue("email", row[0])
                            newRecord.setValue("abbreviation", abbr)
                            newRecord.setValue("created_at", QDateTime.currentDateTime().toString("dd/MM/yyyy h:m:s AP"))
                            newRecord.setValue("group_id", self.groupId)

                            if not self.modelTableMembers.insertRecord(-1, newRecord):
                                raise Exception(self.tr("One row was not inserted"))
            except Exception as e:
                self.modelTableMembers.revertAll()
                QMessageBox.warning(self.view, self.tr("Error reading CSV File"),
                                    self.tr("The CSV File has not the correct format. Try to see the example in this program. The error was: ")+str(e))
            else:
                QMessageBox.information(self.view, self.tr("CSV File correctly inserted"),
                                        self.tr("The CSV File has correct format."))
                self.modified.sign.emit()
                self.modelTableMembers.submitAll()

    def addButtonTests(self):
        # check if the fields has data
        if not self.view.nameEditTests.text():
            QMessageBox.warning(self.view, self.tr("No values to add"),
                                self.tr("The field Name is not filled"))
        else:
            try:
                testSet = self.modelTests.getTestsSets()
                row = [self.view.nameEditTests.text()]
                self.modelTests.checkInsertTests(testSet, row)
                newRecord = self.modelTableTests.record()
                newRecord.remove(newRecord.indexOf("id"))
                newRecord.setValue("name", row[0])
                newRecord.setValue("created_at", QDateTime.currentDateTime().toString("dd/MM/yyyy h:m:s AP"))
                newRecord.setValue("group_id", self.groupId)
                newRecord.setValue("survey_id", 2)

                if not self.modelTableTests.insertRecord(-1, newRecord):
                    raise Exception(self.tr("Could not insert the new member"))
                self.modelTableTests.submitAll()
                self.modified.sign.emit()
                QMessageBox.information(self.view, self.tr("Test inserted"),
                                        self.tr("The test data was inserted correctly."))

                self.testId = self.modelTableTests.query().lastInsertId()

                # INSERT THE PARTICIPANTS
                QMessageBox.information(self.view, self.tr("Adding participants"),
                                        self.tr("Adding the active members of this group as a participants in this new test. Later, you can not add new participants. To add a new participant, please create a new test. "))

                self.modelTests.insertResponseCodes(self.testId)

            except Exception as e:
                self.modelTableTests.revertAll()
                QMessageBox.warning(self.view, self.tr("Error Adding the new test"),
                                    self.tr("Some fields are not correct. The error was: ")+str(e))

    def addButtonMember(self):
        # check if the fields has data
        if not self.view.nameEditMembers.text() or not self.view.surnameEditMembers.text() or self.view.ageSpinBoxMembers.value() < 1 or not self.view.emailEditMembers.text() or not self.view.abbreviationEditMembers.text():
            QMessageBox.warning(self.view, self.tr("No values to add"),
                                self.tr("The fields Name or Organization or Email or Abbreviation are not filled or Age is negative"))
        else:
            try:
                membersSet = self.modelMembers.getMembersSets()
                row = [self.view.emailEditMembers.text(),
                       self.view.nameEditMembers.text(),
                       self.view.surnameEditMembers.text(),
                       self.view.abbreviationEditMembers.text(),
                       self.view.ageSpinBoxMembers.value(),
                       self.view.sexComboBoxMembers.currentText()[0]]
                self.modelMembers.checkInsertMembers(membersSet, row)
                newRecord = self.modelTableMembers.record()
                newRecord.remove(newRecord.indexOf("id"))
                newRecord.setValue("name", row[1])
                newRecord.setValue("surname", row[2])
                newRecord.setValue("sex", row[5])
                newRecord.setValue("age", row[4])
                newRecord.setValue(
                    "active", self.cbMembers[self.view.activeCheckBoxMembers.checkState()])
                newRecord.setValue("email", row[0])
                newRecord.setValue("abbreviation", row[3])
                newRecord.setValue("created_at", QDateTime.currentDateTime().toString("dd/MM/yyyy h:m:s AP"))
                newRecord.setValue("group_id", self.groupId)

                if not self.modelTableMembers.insertRecord(-1, newRecord):
                    raise Exception(self.tr("Could not insert the new member"))
            except Exception as e:
                self.modelTableMembers.revertAll()
                QMessageBox.warning(self.view, self.tr("Error Adding the new member"),
                                    self.tr("Some fields are not correct. The error was: ")+str(e))
            else:
                self.modelTableMembers.submitAll()
                self.modified.sign.emit()
                QMessageBox.information(self.view, self.tr("Member inserted"),
                                        self.tr("The member data was inserted correctly."))

    def updateButtonTests(self):
        if self.view.tableTests.selectionModel().selection().count() == 0:
            QMessageBox.warning(self.view, self.tr("No test selected"),
                                self.tr("To update a test, you have to select one test."))
        else:
            try:
                curRow = self.view.tableTests.currentIndex().row()
                updateRecord = self.modelTableTests.record(curRow)

                testSet = self.modelTests.getTestsSetsUpdated(updateRecord.value(0))
                row = [self.view.nameEditTests.text()]

                self.modelTests.checkInsertTests(testSet, row)
                updateRecord.setValue("name", row[0])

                if not self.modelTableTests.setRecord(curRow, updateRecord):
                    raise Exception(self.tr("Could not update the new test"))

            except Exception as e:
                self.modelTableTests.revertAll()
                QMessageBox.warning(self.view, self.tr("Error Adding the new test"),
                                    self.tr("Some fields are not correct. The error was: ")+str(e))
            else:
                self.modelTableTests.submitAll()
                self.modified.sign.emit()
                QMessageBox.information(self.view, self.tr("Test Updated"),
                                        self.tr("The test data was inserted correctly."))

    def updateButtonMember(self):
        if self.view.tableMembers.selectionModel().selection().count() == 0:
            QMessageBox.warning(self.view, self.tr("No member selected"),
                                self.tr("To update a member, you have to select one member."))
        else:
            try:
                curRow = self.view.tableMembers.currentIndex().row()
                updateRecord = self.modelTableMembers.record(curRow)
                membersSet = self.modelMembers.getMembersSetsUpdated(updateRecord.value(0))
                row = [self.view.emailEditMembers.text(),
                       self.view.nameEditMembers.text(),
                       self.view.surnameEditMembers.text(),
                       self.view.abbreviationEditMembers.text(),
                       self.view.ageSpinBoxMembers.value(),
                       self.view.sexComboBoxMembers.currentText()[0]]

                self.modelMembers.checkInsertMembers(membersSet, row)
                updateRecord.setValue("name", row[1])
                updateRecord.setValue("surname", row[2])
                updateRecord.setValue("sex", row[5])
                updateRecord.setValue("age", row[4])
                updateRecord.setValue("active",
                                      self.cbMembers[self.view.activeCheckBoxMembers.checkState()])
                updateRecord.setValue("email", row[0])
                updateRecord.setValue("abbreviation", row[3])

                if not self.modelTableMembers.setRecord(curRow, updateRecord):
                    raise Exception(self.tr("Could not update the new member"))

            except Exception as e:
                self.modelTableMembers.revertAll()
                QMessageBox.warning(self.view, self.tr("Error Adding the new member"),
                                    self.tr("Some fields are not correct. The error was: ")+str(e))
            else:
                self.modelTableMembers.submitAll()
                self.modified.sign.emit()
                QMessageBox.information(self.view, self.tr("Member inserted"),
                                        self.tr("The member data was inserted correctly."))

    def deleteButtonTests(self):
        # TODO: Delete on cascade de responseCodes y responses que se hayan hecho con ese test
        if self.view.tableTests.selectionModel().selection().count() == 0:
            QMessageBox.warning(self.view, self.tr("No test selected"),
                                self.tr("To delete a test, you have to select one test."))
        else:
            curRow = self.view.tableTests.currentIndex().row()
            self.modelTableTests.removeRow(curRow)
            button = QMessageBox.question(self.view, self.tr("Delete the current line?"),
                                          self.tr("Are you sure?"))
            if button == QMessageBox.Yes:
                self.modelTableTests.submitAll()
            else:
                self.modelTableTests.revertAll()

    def deleteButtonMember(self):
        if self.view.tableMembers.selectionModel().selection().count() == 0:
            QMessageBox.warning(self.view, self.tr("No member selected"),
                                self.tr("To delete a member, you have to select one member."))
        else:
            curRow = self.view.tableMembers.currentIndex().row()
            self.modelTableMembers.removeRow(curRow)
            button = QMessageBox.question(self.view, self.tr("Delete the current line?"),
                                          self.tr("Are you sure?"))
            if button == QMessageBox.Yes:
                self.modelTableMembers.submitAll()
                self.modified.sign.emit()
            else:
                self.modelTableMembers.revertAll()

    def selectRowMember(self):
        selection = self.view.tableMembers.selectionModel().selectedRows()
        self.model = self.view.tableMembers.model()

        for sel in selection:
            self.view.nameEditMembers.setText(
                self.model.record(sel.row()).field(1).value())
            self.view.surnameEditMembers.setText(
                self.model.record(sel.row()).field(2).value())
            self.view.sexComboBoxMembers.setCurrentText(
                self.comboBoxMembersDict[self.model.record(sel.row()).field(3).value()])
            self.view.ageSpinBoxMembers.setValue(
                self.model.record(sel.row()).field(4).value())
            self.view.activeCheckBoxMembers.setCheckState(
                self.checkBoxMembersDict[self.model.record(sel.row()).field(5).value()])
            self.view.emailEditMembers.setText(
                self.model.record(sel.row()).field(6).value())
            self.view.abbreviationEditMembers.setText(
                self.model.record(sel.row()).field(8).value())

    def selectRowTests(self):
        selection = self.view.tableTests.selectionModel().selectedRows()
        self.model = self.view.tableTests.model()

        for sel in selection:
            self.view.nameEditTests.setText(
                self.model.record(sel.row()).field(4).value())

    def setViewProperties(self, view):
        view.setSelectionMode(
            QAbstractItemView.SingleSelection)
        view.setSelectionBehavior(
            QAbstractItemView.SelectRows)
        view.setEditTriggers(
            QAbstractItemView.NoEditTriggers)
        view.verticalHeader().setVisible(False)
        view.setSizeAdjustPolicy(
            QAbstractScrollArea.AdjustToContents)
        view.setSortingEnabled(True)
