import sys

from PySide6.QtCore import QFile, QIODevice,QObject
from PySide6.QtUiTools import QUiLoader
from PySide6.QtWidgets import (QMessageBox)

from QuestionModel import QuestionModel
from ParticipantModel import ParticipantModel


class AnswerView(QObject):
    def __init__(self):
        ui_file_name = "ui/answer.ui"
        ui_file = QFile(ui_file_name)
        if not ui_file.open(QIODevice.ReadOnly):
            print(f"Cannot open {ui_file_name}: {ui_file.errorString()}")
            sys.exit(-1)
        loader = QUiLoader()
        loader.setLanguageChangeEnabled(True)
        self.view = loader.load(ui_file)
        ui_file.close()
        if not self.view:
            print(loader.errorString())
            sys.exit(-1)

    def getView(self):
        return self.view

    def loadAnswers(self, groupId, groupName, testId, testName, db):
        self.db = db
        self.groupId = groupId
        self.groupName = groupName
        self.testId = testId
        self.testName = testName

        # Get the participants that have not answered yet as a Set
        self.participant = ParticipantModel(db, testId)
        self.notAnswered = self.participant.getParticipantNotAnsweredSet()

        # Get the questions of the survey
        self.questionsModel = QuestionModel(db)
        self.questions = self.questionsModel.getQuestionsQuery()

        return not self.isParticipantsNotAnsweredEmpty()

    def isParticipantsNotAnsweredEmpty(self):
        return (len(self.notAnswered) == 0)

    def startAnswering(self):
        self.view.groupNameLabel.setText(self.groupName)
        self.view.testLabel.setText(self.testName)
        #QMessageBox.information(self.view, self.tr("Starting Answering"),
                                #self.tr("You will start to answer."))

        # Choose the student from the students that has not answered.
        participant = self.notAnswered.pop()

        self.view.nextStudentLabel.setText(str(participant.name) + " " + str(participant.surname))
        # Check what members have not answered yet.

        # check the members that have not the responsescodes and the responses of this group

        return participant
