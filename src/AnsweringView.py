import sys
import json

from PySide6.QtCore import QFile, QIODevice, QObject
from PySide6.QtUiTools import QUiLoader
from PySide6.QtWidgets import (QPushButton, QMessageBox)

from ParticipantModel import ParticipantModel
from AnsweringModel import AnsweringModel
from QuestionModel import QuestionModel
from Communicate import Communicate

from functools import partial


class AnsweringView(QObject):
    def __init__(self):
        ui_file_name = "ui/answering.ui"
        ui_file = QFile(ui_file_name)
        if not ui_file.open(QIODevice.ReadOnly):
            print(f"Cannot open {ui_file_name}: {ui_file.errorString()}")
            sys.exit(-1)
        loader = QUiLoader()
        loader.setLanguageChangeEnabled(True)
        self.view = loader.load(ui_file)
        ui_file.close()
        if not self.view:
            print(loader.errorString())
            sys.exit(-1)
        self.connect()
        self.modified = Communicate()
        self.finished = Communicate()

    def getView(self):
        return self.view

    def connect(self):
        self.view.backButton.clicked.connect(self.back)
        self.view.clearButton.clicked.connect(self.clear)
        self.view.nextButton.clicked.connect(self.next)

    def back(self):
        # Activate nextButton if not activated
        self.view.nextButton.setEnabled(True)

        # Check if it is the first question
        if self.actualQuestion > 0:
            self.actualQuestion = self.actualQuestion - 1
            self.changeNumberQuestion()

            # Clear the color buttons
            self.clearButtonColors()
            # Put the color of the actual response
            self.chargeButtonColors()
            # Change the text of the Question
            self.changeQuestion()
            # Change the text of the nextButton
            self.changeNextButton()

        else:
            self.view.backButton.setEnabled(False)
            self.changeNextButton()

    def next(self):
        # It has to choose at least one person selected.
        if len(self.selection[self.actualQuestion]) == 0:
            QMessageBox.information(self.view, self.tr("Error"),
                                    self.tr("You have to choose at least one person."))
        else:
            # Activate backButton if not activated
            self.view.backButton.setEnabled(True)

            # Check if it is the last question
            if self.actualQuestion < self.numberQuestions-1:
                self.actualQuestion = self.actualQuestion + 1
                self.changeNumberQuestion()
                # Clear the color buttons
                self.clearButtonColors()
                # Put the color of the actual response
                self.chargeButtonColors()
                # Change the text of the Question
                self.changeQuestion()
                # Change the text of the nextButton
                self.changeNextButton()

            else:
                # Prerequisit: if all the questions have to be answered.
                button = QMessageBox.question(
                    self.view, self.tr("Confirm Finish"), self.tr("Are you sure that you finish answering?"))
                if button == QMessageBox.Yes:
                    # Save the results!!!!!!!!!!!!!
                    self.saveResults()
                    self.inhabilitateButtons()

    def inhabilitateButtons(self):
        self.clearButtonColors()
        self.view.backButton.setEnabled(False)
        self.view.clearButton.setEnabled(False)
        self.view.nextButton.setEnabled(False)

    def habilitateButtons(self):
        self.clearButtonColors()
        self.view.backButton.setEnabled(True)
        self.view.clearButton.setEnabled(True)
        self.view.nextButton.setEnabled(True)

    def clear(self):
        # Clear the dict selection and put all the buttons with default color.
        self.initDictSelection()
        self.clearButtonColors()
        self.actualQuestion = 0
        self.changeQuestion()
        self.changeNumberQuestion()
        self.changeNextButton()

    def loadQuestions(self, db, lang):
        self.lang = lang
        questions = QuestionModel(db)
        self.questions = questions.getQuestionsQuery()
        self.numberQuestions = len(self.questions)
        self.actualQuestion = 0
        self.changeNumberQuestion()
        # TODO: Change Questions languages to not use json.
        self.changeQuestion()
        self.changeNextButton()

    def loadParticipants(self, db, testId, choosenOne):
        self.db = db
        self.testId = testId
        self.choosenOne = choosenOne
        self.initDictSelection()

        self.defaultButtonColor = self.view.backButton.palette().button().color()

        # Write the Student Name that is answering.
        self.view.studentNameLabel.setText(self.choosenOne.name + " " + self.choosenOne.surname)

        # Get the participants with no the choosen to answer
        self.participant = ParticipantModel(self.db, testId)
        participants = self.participant.getParticipantsExceptWhoIsAnswering(self.choosenOne)
        self.buttons = []

        i = 1
        j = 1
        for part in participants:
            button = QPushButton(part.name + " " + part.surname)
            self.buttons.append(button)

            button.clicked.connect(partial(self.selectParticipant, button, part))
            self.view.studentsLayout.addWidget(button, j, i)
            if (i == 3):
                i = 1
                j = j + 1
            else:
                i = i + 1
        self.habilitateButtons()

    # When click on a participant to choose him/her.
    def selectParticipant(self, b, part):
        # If the button is selected, change background & exclude it in the dict
        if part.id in self.selection[self.actualQuestion]:
            b.setStyleSheet("background-color:"+str(self.defaultButtonColor.rgb()))

            # Erase the element of the dictionary
            self.selection[self.actualQuestion].pop(part.id)
        # If the button is deselected, change background & include it in the dict
        else:
            # Change the background
            b.setStyleSheet("background-color: red")

            # Add the element in the dictionary
            self.selection[self.actualQuestion][part.id] = b
        #print(self.selection)

    def initDictSelection(self):
        # Initialize the List of Dictionaries that are the answers.
        self.selection = []
        for x in range(self.numberQuestions):
            self.selection.append({})

    def clearButtonColors(self):
        for button in self.buttons:
            button.setStyleSheet("background-color:"+str(self.defaultButtonColor.rgb()))

    def chargeButtonColors(self):
        for id, b in self.selection[self.actualQuestion].items():
            b.setStyleSheet("background-color: red")

    def changeQuestion(self):
        text = json.loads(self.questions[self.actualQuestion].text)
        # COMO SABER EL IDIOMA???????????????????
        self.view.questionLabel.setText(text[self.lang])

    def changeNumberQuestion(self):
        self.view.questionNumberLabel.setText(
            str(self.actualQuestion+1) + "/" + str(self.numberQuestions))

    def changeNextButton(self):
        if self.actualQuestion == self.numberQuestions - 1:
            self.view.nextButton.setText(self.tr("Finish >"))
        else:
            self.view.nextButton.setText(self.tr("Next >"))

    def saveResults(self):
        self.answering = AnsweringModel(self.db)
        # member_id, test_id, question_id, choice
        # member_id = self.choosenOne
        # test_id = self.testId
        # question_id = self.questions[x].id
        # choice = self.selection
        # Insert all the selections in the table responses.
        self.answering.saveResults(self.choosenOne.id, self.testId, self.questions, self.selection)
        self.modified.sign.emit()
        self.finished.sign.emit()
