import sys

from PySide6.QtCore import QFile, QIODevice,QObject,Qt, QDateTime
from PySide6.QtUiTools import QUiLoader
from PySide6.QtWidgets import (QAbstractItemView, QAbstractScrollArea,
                               QMessageBox)

from datetime import datetime

from GroupModel import GroupModel
from Communicate import Communicate

class GroupView(QObject):

    def __init__(self):
        ui_file_name = "ui/groups.ui"
        ui_file = QFile(ui_file_name)
        if not ui_file.open(QIODevice.ReadOnly):
            print(f"Cannot open {ui_file_name}: {ui_file.errorString()}")
            sys.exit(-1)
        loader = QUiLoader()
        loader.setLanguageChangeEnabled(True)
        self.view = loader.load(ui_file)
        ui_file.close()
        if not self.view:
            print(loader.errorString())
            sys.exit(-1)
        self.view.tableGroups.setSelectionMode(
            QAbstractItemView.SingleSelection)
        self.view.tableGroups.setSelectionBehavior(
            QAbstractItemView.SelectRows)
        self.view.tableGroups.setEditTriggers(
            QAbstractItemView.NoEditTriggers)
        self.view.tableGroups.verticalHeader().setVisible(False)
        self.view.tableGroups.setSizeAdjustPolicy(
            QAbstractScrollArea.AdjustToContents)
        self.view.tableGroups.setSortingEnabled(True)

        self.modified = Communicate()
        self.connect()
        #self.createMessages()

    def getView(self):
        return self.view

    def loadGroups(self, db):
        self.modelGroup = GroupModel(db.getConnection())
        self.modelTable = self.modelGroup.getGroups()
        self.changeHeaders()

        self.view.tableGroups.setModel(self.modelTable)
        self.view.tableGroups.hideColumn(0)
        self.view.tableGroups.hideColumn(3)
        self.view.tableGroups.hideColumn(4)

    def changeHeaders(self):
        self.modelTable.setHeaderData(0, Qt.Horizontal, "Id")
        self.modelTable.setHeaderData(1, Qt.Horizontal, self.tr("Name"))
        self.modelTable.setHeaderData(2, Qt.Horizontal, self.tr("Organization"))
        self.modelTable.setHeaderData(3, Qt.Horizontal, "created_at")
        self.modelTable.setHeaderData(4, Qt.Horizontal, "tutor_id")

    def connect(self):
        self.view.tableGroups.clicked.connect(self.selectRow)
        self.view.deleteButton.clicked.connect(self.onDelete)
        self.view.addButton.clicked.connect(self.onAdd)
        self.view.updateButton.clicked.connect(self.onUpdate)

    def selectRow(self):
        selection = self.view.tableGroups.selectionModel().selectedRows()
        for sel in selection:
            self.view.editNom.setText(
                self.modelTable.record(sel.row()).field(1).value())
            self.view.editOrganitzacio.setText(
                self.modelTable.record(sel.row()).field(2).value())

    def onAdd(self):
        # check if the fields has data
        if not self.view.editNom.text() or not self.view.editOrganitzacio.text():
            QMessageBox.warning(self.view, self.tr("No values to add"),
                                self.tr("The fields Name or Organization are not filled"))
        else:
            try:
                groupSet = self.modelGroup.getGroupSets()
                row = [self.view.editNom.text()]
                self.modelGroup.checkInsertGroups(groupSet, row)

                newRecord = self.modelTable.record()
                newRecord.remove(newRecord.indexOf("id"))
                newRecord.setValue("name", self.view.editNom.text())
                newRecord.setValue("organization", self.view.editOrganitzacio.text())
                newRecord.setValue("created_at", QDateTime.currentDateTime().toString("dd/MM/yyyy h:m:s AP"))
                newRecord.setValue("tutor_id", 6)

                if not self.modelTable.insertRecord(-1, newRecord):
                    raise Exception(self.tr("Could not insert the new group"))

            except Exception as e:
                self.modelTable.revertAll()
                QMessageBox.warning(self.view, self.tr("Error Adding the new group"),
                                    self.tr("Some fields are not correct. The error was: ")+str(e))
            else:
                self.modelTable.submitAll()
                self.modified.sign.emit()
                QMessageBox.information(self.view, self.tr("Group inserted"),
                                        self.tr("The group data was inserted correctly."))

    def onUpdate(self):
        if self.view.tableGroups.selectionModel().selection().count() == 0:
            QMessageBox.warning(self.view, self.tr("No group selected"),
                                self.tr("To update a group, you have to select one group."))
        else:
            try:
                curRow = self.view.tableGroups.currentIndex().row()
                updateRecord = self.modelTable.record(curRow)

                groupSet = self.modelGroup.getGroupsSetsUpdated(updateRecord.value(0))
                row = [self.view.editNom.text()]

                self.modelGroup.checkInsertGroups(groupSet, row)

                updateRecord.setValue("name", self.view.editNom.text())
                updateRecord.setValue("organization", self.view.editOrganitzacio.text())

                if not self.modelTable.setRecord(curRow, updateRecord):
                    raise Exception(self.tr("Could not update the new test"))

            except Exception as e:
                self.modelTable.revertAll()
                QMessageBox.warning(self.view, self.tr("Error Updating the new group"),
                                    self.tr("Some fields are not correct. The error was: ")+str(e))
            else:
                self.modelTable.submitAll()
                self.modified.sign.emit()
                QMessageBox.information(self.view, self.tr("Group Updated"),
                                        self.tr("The group data was updated correctly."))

    def onDelete(self):
        if self.view.tableGroups.selectionModel().selection().count() == 0:
            QMessageBox.warning(self.view, self.tr("No group selected"),
                                self.tr("To delete a group, you have to select one group."))
        else:
            curRow = self.view.tableGroups.currentIndex().row()
            self.modelTable.removeRow(curRow)
            button = QMessageBox.question(self.view, self.tr("Delete the current group?"),
                                          self.tr("Are you sure?"))
            if button == QMessageBox.Yes:
                self.modelTable.submitAll()
                self.modified.sign.emit()
            else:
                self.modelTable.revertAll()
