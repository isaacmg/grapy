import sys

from PySide6.QtCore import QFile, QIODevice,QObject, Qt
from PySide6.QtUiTools import QUiLoader
from PySide6.QtWidgets import (QAbstractItemView, QAbstractScrollArea)

from ParticipantModel import ParticipantModel


class ParticipantView(QObject):
    def __init__(self):
        ui_file_name = "ui/participants.ui"
        ui_file = QFile(ui_file_name)
        if not ui_file.open(QIODevice.ReadOnly):
            print(f"Cannot open {ui_file_name}: {ui_file.errorString()}")
            sys.exit(-1)
        loader = QUiLoader()
        loader.setLanguageChangeEnabled(True)
        self.view = loader.load(ui_file)
        ui_file.close()
        if not self.view:
            print(loader.errorString())
            sys.exit(-1)
        self.connect()

        self.configViews()

    def getView(self):
        return self.view

    def loadParticipants(self, db, groupName, testId,testName):
        self.groupName = groupName
        self.participant = ParticipantModel(db, testId)

        self.modelParticipantAnswered = self.participant.getParticipantsAnswered()
        self.modelParticipantNotAnswered = self.participant.getParticipantsNotAnswered()

        self.view.participantsAnsweredView.setModel(self.modelParticipantAnswered)
        self.view.participantsAnsweredView.hideColumn(0)
        self.view.participantsAnsweredView.hideColumn(3)
        self.view.participantsAnsweredView.hideColumn(4)

        self.modelParticipantAnswered.setHeaderData(1, Qt.Horizontal, self.tr("Name"))
        self.modelParticipantAnswered.setHeaderData(2, Qt.Horizontal, self.tr("Surname"))
        self.modelParticipantAnswered.setHeaderData(5, Qt.Horizontal, self.tr("Number of answers"))

        self.view.participantsNotAnsweredView.setModel(self.modelParticipantNotAnswered)
        self.view.participantsNotAnsweredView.hideColumn(0)
        self.view.participantsNotAnsweredView.hideColumn(3)
        self.view.participantsNotAnsweredView.hideColumn(4)


        self.modelParticipantNotAnswered.setHeaderData(1, Qt.Horizontal, self.tr("Name"))
        self.modelParticipantNotAnswered.setHeaderData(2, Qt.Horizontal, self.tr("Surname"))
        self.modelParticipantNotAnswered.setHeaderData(5, Qt.Horizontal, self.tr("Number of answers"))

        self.view.groupNameLabel.setText(self.groupName)
        self.view.testLabel.setText(testName)

    def connect(self):
        self.view.participantsAnsweredView.clicked.connect(self.selectRow)

    def selectRow(self):
        selection = self.view.tableGroups.selectionModel().selectedRows()
        for sel in selection:
            self.view.editNom.setText(
                self.modelTable.record(sel.row()).field(1).value())
            self.view.editOrganitzacio.setText(
                self.modelTable.record(sel.row()).field(2).value())

    def configViews(self):
        self.view.participantsAnsweredView.setSelectionMode(
            QAbstractItemView.SingleSelection)
        self.view.participantsAnsweredView.setSelectionBehavior(
            QAbstractItemView.SelectRows)
        self.view.participantsAnsweredView.setEditTriggers(
            QAbstractItemView.NoEditTriggers)
        self.view.participantsAnsweredView.verticalHeader().setVisible(False)
        self.view.participantsAnsweredView.setSizeAdjustPolicy(
            QAbstractScrollArea.AdjustToContents)
        self.view.participantsAnsweredView.setSortingEnabled(True)

        self.view.participantsNotAnsweredView.setSelectionMode(
            QAbstractItemView.SingleSelection)
        self.view.participantsNotAnsweredView.setSelectionBehavior(
            QAbstractItemView.SelectRows)
        self.view.participantsNotAnsweredView.setEditTriggers(
            QAbstractItemView.NoEditTriggers)
        self.view.participantsNotAnsweredView.verticalHeader().setVisible(False)
        self.view.participantsNotAnsweredView.setSizeAdjustPolicy(
            QAbstractScrollArea.AdjustToContents)
        self.view.participantsNotAnsweredView.setSortingEnabled(True)
