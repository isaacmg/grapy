from PySide6.QtSql import QSqlQuery
from QuestionModel import QuestionModel


class AnswerModel(object):
    def __init__(self, db, testId):
        self.db = db
        self.testId = testId
        questions = QuestionModel(db)
        self.questions = questions.getQuestions()

    def getParticipants(self):
        query = QSqlQuery(self.db)
        query.prepare(
            "select m.id, m.name, m.surname, m.email, r.code, COUNT(re.id) FROM responsecodes r, members m LEFT JOIN responses re  ON re.test_id = r.test_id AND re.member_id = r.member_id  WHERE r.test_id=:testId AND  m.id = r.member_id GROUP BY r.member_id")
        query.bindValue(":testId", self.testId)
        if not query.exec():
            raise Exception("Could not execute select")
        members = TestSets()
        while query.next():
            members.name.add(query.value(0))
        return members

    def getTestsSetsUpdated(self, notIncluded):
        query = QSqlQuery(self.db)
        query.prepare(
            "SELECT name FROM tests WHERE group_id = :groupId AND id != :idUpdated")
        query.bindValue(":groupId", self.groupId)
        query.bindValue(":idUpdated", notIncluded)
        if not query.exec():
            raise Exception("Could not execute select")
        members = TestSets()
        while query.next():
            members.name.add(query.value(0))
        return members

    def checkInsertTests(self, testsSet, row):
        # Checking that member's name and surname is not already in the member's table
        name = row[0]
        if name in testsSet.name:
            raise Exception("Name already in the group")
        else:
            testsSet.name.add(name)

        # Some fields are incorrect.
        if not row[0]:
            raise Exception("Some fields are empty")

    def insertResponseCodes(self, testId):
        # Insering the active members in the responsecodes
        query = QSqlQuery(self.db)
        query.prepare(
            "INSERT INTO responsecodes (test_id, member_id, code) SELECT :testId, m.id, 'hola' FROM members m WHERE m.group_id = :groupId AND active = 1")
        query.bindValue(":groupId", self.groupId)
        query.bindValue(":testId", testId)
        if not query.exec():
            raise Exception("Could not execute select")
        else:
            self.model.submitAll()


class Participants:
    def __init__(self, id, name, ):
        self.name = set()
