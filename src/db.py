from PySide6.QtSql import QSqlDatabase


class Db(object):
    def __init__(self):
        self.db = QSqlDatabase("QSQLITE")

    def getConnection(self):
        return self.db

    def isOpen(self):
        return self.db.isOpen()

    def putDatabase(self, name):
        self.db.close()
        self.db.setDatabaseName(name)
        a = self.db.open()
        return a
