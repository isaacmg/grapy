
from NetworkxUtilities import SocioGraph
from ParticipantModel import ParticipantModel
from ResponseModel import ResponseModel
from Communicate import Communicate
from PySide6.QtWidgets import QPushButton
from PySide6.QtUiTools import QUiLoader
from PySide6.QtCore import QFile, QIODevice, Slot, QUrl
from PySide6.QtWebEngineWidgets import QWebEngineView
from networkx.readwrite import json_graph
from PySide6.QtCore import Qt
from PySide6.QtGui import QPixmap

import json
import os
import sys
import math

class ResultsViewD3(object):
    def __init__(self):
        ui_file_name = "ui/results.ui"
        ui_file = QFile(ui_file_name)
        if not ui_file.open(QIODevice.ReadOnly):
            print(f"Cannot open {ui_file_name}: {ui_file.errorString()}")
            sys.exit(-1)
        loader = QUiLoader()
        loader.setLanguageChangeEnabled(True)
        self.view = loader.load(ui_file)
        self.graph = SocioGraph()
        ui_file.close()
        # checkbox Values-numbers Array.
        self.checkBoxMembersDict = [Qt.Unchecked, Qt.Checked, Qt.Checked]
        self.cbMembers = [0, 1, 1]
        #self.view.forceTextLabel.setText("GroupName: 0.01")
        self.view.forceTextLabel.setText("Force: 0.01")

        if not self.view:
            print(loader.errorString())
            sys.exit(-1)
        self.connect()
        self.modified = Communicate()

    def getView(self):
        return self.view

    def connect(self):
        self.view.preferencesButton.clicked.connect(self.preference)
        self.view.perceptionPrefButton.clicked.connect(self.perceptionPref)
        self.view.perceptionRejButton.clicked.connect(self.perceptionRej)
        self.view.rejectionButton.clicked.connect(self.rejection)
        self.view.degreeButton.clicked.connect(self.degree)
        self.view.betweenessButton.clicked.connect(self.betweeness)
        self.view.closenessButton.clicked.connect(self.closeness)
        self.view.genderButton.clicked.connect(self.gender)
        self.view.downloadGraphButton.clicked.connect(self.download)
        self.view.autoCenterCheckBox.stateChanged.connect(self.autocenter)
        self.view.forceLinkSlider.sliderReleased.connect(self.slider)
        self.view.selectMemberComboBox.currentIndexChanged.connect(self.indCombobox)
        self.view.selectMemberIndexesComboBox.currentIndexChanged.connect(self.indIndexesCombobox)

    def loadGraphs(self, db, groupName, test_id, testName):
        self.db = db
        self.groupName = groupName
        self.testId = test_id

        self.view.groupNameLabel.setText(self.groupName)
        self.view.testLabel.setText(testName)

        self.responses = ResponseModel(self.db, self.testId)

        # Get the Participants that have already answered
        self.participant = ParticipantModel(self.db, self.testId)
        self.participants = self.participant.getParticipantsDict()

        # We fill the select Member Combo Box in the Individual Graph Tab
        self.parList = []
        for x in self.participants:
            self.parList.append(x["name"] + " " + x["surname"])
        self.view.selectMemberComboBox.addItems(self.parList)
        self.view.selectMemberIndexesComboBox.addItems(self.parList)

        self.graph.addParticipants(self.participants)
        weight = math.ceil(len(self.participants) * 0.3)

        # SELECT responses from one test
        responses = self.responses.getResponses()
        for response in responses:
            self.graph.add_relation(response["member_id"], response["choice"], weight, response["category"], response["criterion"])

        if self.graph.graphIsNotEmptyOfLinks():
            self.graph.add_centralities()
            self.graph.fetch_direct_sociometric_indexes_per_member()

        self.webEngineView = QWebEngineView()
        self.loadPage()

        if self.view.resultsHorizontalLayout.count() > 0:
            self.view.resultsHorizontalLayout.removeWidget(self.view.resultsHorizontalLayout.takeAt(0).widget())

        self.view.resultsHorizontalLayout.addWidget(self.webEngineView)

        # Creem les altres pestanyes del widget. Començant per Group Indexes
        self.getGroupIndexes()

    def loadPage(self):

        with open('d3graph/examples/graphs1.html', 'r') as f:
            html = f.read()
            self.webEngineView.page().loadFinished.connect(self.onLoadFinished)
            self.webEngineView.setHtml(html, baseUrl=QUrl.fromLocalFile(os.getcwd()+os.path.sep))

    @Slot(bool)
    def onLoadFinished(self, ok):
        if ok:
            #self.webEngineView.page().runJavaScript("let graph3 = {'name':'Hola'};window.getInfo(graph3);");
            #print(json.dumps(json_graph.node_link_data(self.graph.G)))
            self.webEngineView.page().runJavaScript("window.getInfo('"+json.dumps(json_graph.node_link_data(self.graph.G))+"');" );
            self.modified.sign.emit()

    def preference(self):
        #sociotype: 0: Preference, 1: Perception Preference, 2: Perception Rejection, 3: Rejection
        self.webEngineView.page().runJavaScript("window.changeSocioType(0);" )
    def perceptionPref(self):
        self.webEngineView.page().runJavaScript("window.changeSocioType(1);" )
    def perceptionRej(self):
        self.webEngineView.page().runJavaScript("window.changeSocioType(2);" )
    def rejection(self):
        self.webEngineView.page().runJavaScript("window.changeSocioType(3);" )
    def degree(self):
        self.webEngineView.page().runJavaScript("window.changeColor(0);" )
    def betweeness(self):
        self.webEngineView.page().runJavaScript("window.changeColor(1);" )
    def closeness(self):
        self.webEngineView.page().runJavaScript("window.changeColor(2);" )
    def gender(self):
        self.webEngineView.page().runJavaScript("window.changeColor(3);" )
    def download(self):
        size = self.webEngineView.contentsRect()
        img = QPixmap(size.width(), size.height())
        self.webEngineView.render(img)
        img.save("prova.png")
        print("hola")

    def autocenter(self):
        self.webEngineView.page().runJavaScript("window.center("+str(self.cbMembers[self.view.autoCenterCheckBox.checkState()])+");" )
    def slider(self):
        self.webEngineView.page().runJavaScript("window.setForceLink("+str(self.view.forceLinkSlider.value()*0.01)+");" )
        print(self.view.forceLinkSlider.value()*0.01)
        self.view.forceTextLabel.setText("Force: "+str(self.view.forceLinkSlider.value()*0.01))

    # Bringind the data for the Group Indexes in the second tab.
    def getGroupIndexes(self):
        graph = self.graph.G.graph
        self.view.associationIndexValueLabel.setText(str(graph["AI"]))
        self.view.dissociationIndexValueLabel.setText(str(graph["DI"]))
        self.view.socialCohesionValueLabel.setText(str(graph["CI"]))
        self.view.socialIntensityValueLabel.setText(str(graph["SI"]))
        self.view.selectMemberComboBox.currentIndex()

    def indIndexesCombobox(self):
        graph = json_graph.node_link_data(self.graph.G)["nodes"]
        if len(graph):
            participant = self.participants[self.view.selectMemberIndexesComboBox.currentIndex()]

            par = {}
            for node in graph:
                if node["id"] == participant["id"]:
                    par = node
            #Direct
            self.view.electionStatusLabel.setText(str(par["Sp"]))
            self.view.perceptionOfElectionStatusLabel.setText(str(par["Pp"]))
            self.view.rejectionStatusLabel.setText(str(par["Sn"]))
            self.view.perceptionOfRejectionStatusLabel.setText(str(par["Pn"]))
            self.view.reciprocalElectionsLabel.setText(str(par["Rp"]))
            self.view.reciprocalRejectionsLabel.setText(str(par["Rn"]))
            self.view.feelingOppositionLabel.setText(str(par["OS"]))
            self.view.positiveExpansionLabel.setText(str(par["Ep"]))
            self.view.negativeExpansionLabel.setText(str(par["En"]))
            self.view.guessedRightElectionsPerceptionLabel.setText(str(par["PAp"]))
            self.view.guessedRightRejectionsPerceptionLabel.setText(str(par["PAn"]))
            self.view.dyadicOverestimationLabel.setText(str(par["DO"]))
            self.view.dyadicUnderestimationLabel.setText(str(par["DU"]))
            self.view.dyadicDiscrepancyLabel.setText(str(par["DD"]))
            #Compound
            self.view.popularityCoefficientLabel.setText(str(par["Pop"]))
            self.view.antipathyCoefficientLabel.setText(str(par["Ant"]))
            self.view.affectiveConnectionLabel.setText(str(par["CA"]))
            self.view.sociometricStatusLabel.setText(str(par["SS"]))
            self.view.popularityCoefficientLabel.setText(str(par["Pop"]))
            self.view.positiveExpansionCoefficientLabel.setText(str(par["Expp"]))
            self.view.negativeExpansionCoefficientLabel.setText(str(par["Expn"]))
            self.view.realisticPerceptionLabel.setText(str(par["RPER"]))



    def indCombobox(self):
        #print(self.view.selectMemberComboBox.currentIndex())
        self.par = self.participants[self.view.selectMemberComboBox.currentIndex()]
        #print(self.par["name"])

        self.webEngineViewInd = QWebEngineView()

        with open('d3graph/examples/graphs1.html', 'r') as f:
            html = f.read()
            self.webEngineViewInd.page().loadFinished.connect(self.onLoadFinishedInd)
            self.webEngineViewInd.setHtml(html, baseUrl=QUrl.fromLocalFile(os.getcwd()+os.path.sep))

        if self.view.resultsIndHorizontalLayout.count() > 0:
            self.view.resultsIndHorizontalLayout.removeWidget(self.view.resultsIndHorizontalLayout.takeAt(0).widget())

        self.view.resultsIndHorizontalLayout.addWidget(self.webEngineViewInd)

    @Slot(bool)
    def onLoadFinishedInd(self, ok):
        if ok:
            self.webEngineViewInd.page().runJavaScript("window.setMember("+str(self.par["id"])+");window.getIndGraph('"+json.dumps(json_graph.node_link_data(self.graph.G))+"');" );
