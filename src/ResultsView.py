
from NetworkxUtilities import SocioGraph
from ParticipantModel import ParticipantModel
from ResponseModel import ResponseModel
from PySide6.QtWidgets import QPushButton
from PySide6.QtUiTools import QUiLoader
from PySide6.QtCore import QFile, QIODevice
from networkx.readwrite import json_graph

import sys
import math
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
import networkx as nx

matplotlib.use('Qt5Agg')


class ResultsView(object):
    def __init__(self):
        ui_file_name = "ui/results.ui"
        ui_file = QFile(ui_file_name)
        if not ui_file.open(QIODevice.ReadOnly):
            print(f"Cannot open {ui_file_name}: {ui_file.errorString()}")
            sys.exit(-1)
        loader = QUiLoader()
        loader.setLanguageChangeEnabled(True)
        self.view = loader.load(ui_file)
        self.graph = SocioGraph()
        ui_file.close()
        if not self.view:
            print(loader.errorString())
            sys.exit(-1)
        self.connect()

    def getView(self):
        return self.view

    def map(self, minFrom, maxFrom, minTo, maxTo, value):
        mappedValue = minTo + (maxTo - minTo) * ((value - minFrom) / (maxFrom - minFrom))
        return mappedValue

    def connect(self):
        self.view.preferencesButton.clicked.connect(self.preference)
        self.view.perceptionPrefButton.clicked.connect(self.perceptionPref)
        self.view.perceptionRejButton.clicked.connect(self.perceptionRej)
        self.view.rejectionButton.clicked.connect(self.rejection)

    def loadGraphs(self, db, test_id):
        self.db = db
        self.testId = test_id

        self.responses = ResponseModel(self.db, self.testId)

        # Get the Participants that have already answered
        self.participant = ParticipantModel(self.db, self.testId)
        participants = self.participant.getParticipantsDict()

        self.graph.addParticipants(participants)
        weight = math.ceil(len(participants) * 0.3)

        # SELECT responses from one test
        responses = self.responses.getResponses()
        for response in responses:
            self.graph.add_relation(response["member_id"], response["choice"], weight, response["category"], response["criterion"])

        if self.graph.graphIsNotEmptyOfLinks():
            self.graph.add_centralities()
            self.graph.fetch_direct_sociometric_indexes_per_member()

        if not hasattr(self, 'figure'):
            self.figure = plt.figure()
            self.canvas = FigureCanvas(self.figure)
            self.view.resultsHorizontalLayout.addWidget(self.canvas)

        self.draw("preference")

    def draw(self, proj):
        G_proj_multigraph = self.graph.G.edge_subgraph(
            [(i, j, k) for i, j, k in self.graph.G.edges if k == proj])
        G_proj = nx.DiGraph(G_proj_multigraph)
        print(json_graph.node_link_data(G_proj))
        #print(dict(G_preference.nodes(data="node_id")))
        degrees = [val for (node, val) in G_proj.degree()]
        maxD = max(degrees)
        minD = min(degrees)
        # Calculem la grandaria dels nodes i la escalem al rang 100-500
        map = [self.map(minD, maxD, 300, 1000,degree) for degree in degrees]
        #print(map)

        colorDegree =[prefDegree[1] for prefDegree in G_proj.nodes(data=proj+"_degree", default=1)]
        maxD = max(colorDegree)
        minD = min(colorDegree)
        # Calculem el color dels nodes i la escalem al rang 0-100
        colorMap = [self.map(minD, maxD, 0, 100, prefDegree) for prefDegree in colorDegree]
        #print([val for (node, val) in G_preference.degree()])
        #print([node for (node, val) in G_preference.degree()])
        nx.write_gexf(G_proj, "test.gexf")
        self.figure.clf()
        pos = nx.spring_layout(G_proj, k=5, scale=34)
        #pos = nx.graphviz_layout(G_proj,prog='neato')
        #pos = nx.kamada_kawai_layout(G_proj)
        nx.draw(G_proj, pos=pos, with_labels=True, labels=dict(G_proj.nodes(data="node_id")), arrowsize=7, node_size=map, node_color=colorMap, cmap='Blues', font_size=9, edgecolors='lightgray')
        #nx.draw(G_proj, pos=pos)
        self.canvas.draw_idle()

    def preference(self):
        self.draw("preference")
        print("preference")
    def perceptionPref(self):
        self.draw("perception_preference")
        print("perception_preference")
    def perceptionRej(self):
        self.draw("perception_rejection")
        print("perception_rejection")
    def rejection(self):
        self.draw("rejection")
        print("rejection")
