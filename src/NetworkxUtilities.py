import networkx as nx
import json

class SocioGraph(object):
    def __init__(self):
        self.G = nx.MultiDiGraph()

    def addParticipants(self, members):
        for member in members:
            self.G.add_node(member['id'], node_id=member['abbreviation'],
                       age=member['age'], sex=member['sex'])


    def draw(self, graph):
        nx.draw(graph)


    def graphIsNotEmptyOfLinks(self):
        return nx.number_of_edges(self.G)


    def add_centralities(self):
        #G = json_graph.node_link_graph(emosociogram)

        # create subgraphs
        G_preference_multigraph = self.G.edge_subgraph(
            [(i, j, k) for i, j, k in self.G.edges if k == 'preference'])
        G_preference = nx.DiGraph(G_preference_multigraph)

        G_perception_preference_multigraph = self.G.edge_subgraph(
            [(i, j, k) for i, j, k in self.G.edges if k == 'perception_preference'])
        G_perception_preference = nx.DiGraph(G_perception_preference_multigraph)

        G_rejection_multigraph = self.G.edge_subgraph([(i, j, k) for i, j, k in self.G.edges if k == 'rejection'])
        G_rejection = nx.DiGraph(G_rejection_multigraph)

        G_perception_rejection_multigraph = self.G.edge_subgraph(
            [(i, j, k) for i, j, k in self.G.edges if k == 'perception_rejection'])
        G_perception_rejection = nx.DiGraph(G_perception_rejection_multigraph)

        # create G_preference simple graph and add betweeness centrality attribute on nodes
        preference_btwnCent = nx.betweenness_centrality(G_preference, normalized=True, endpoints=False)
        # print(preference_btwnCent)
        #nx.set_node_attributes(G_preference, btwnCent,'preference_betweenness')
        # json_graph.node_link_data(G_preference)
        nx.set_node_attributes(self.G, preference_btwnCent, 'preference_betweenness')
        # G.node[1]['betweenness']

        perception_preference_btwnCent = nx.betweenness_centrality(
            G_perception_preference, normalized=True, endpoints=False)
        nx.set_node_attributes(self.G, perception_preference_btwnCent, 'perception_preference_betweenness')

        rejection_btwnCent = nx.betweenness_centrality(G_rejection, normalized=True, endpoints=False)
        nx.set_node_attributes(self.G, rejection_btwnCent, 'rejection_betweenness')

        perception_rejection_btwnCent = nx.betweenness_centrality(
            G_perception_rejection, normalized=True, endpoints=False)
        nx.set_node_attributes(self.G, perception_rejection_btwnCent, 'perception_rejection_betweenness')

        # calculate degree centrality
        preference_degreeCent = nx.degree_centrality(G_preference)
        nx.set_node_attributes(self.G, preference_degreeCent, 'preference_degree')

        perception_preference_degreeCent = nx.degree_centrality(G_perception_preference)
        nx.set_node_attributes(self.G, perception_preference_degreeCent, 'perception_preference_degree')

        rejection_degreeCent = nx.degree_centrality(G_rejection)
        nx.set_node_attributes(self.G, rejection_degreeCent, 'rejection_degree')

        perception_rejection_degreeCent = nx.degree_centrality(G_perception_rejection)
        nx.set_node_attributes(self.G, perception_rejection_degreeCent, 'perception_rejection_degree')

        # calculate closeness centrality
        preference_clsCent = nx.closeness_centrality(G_preference)
        nx.set_node_attributes(self.G, preference_clsCent, 'preference_closeness')

        perception_preference_clsCent = nx.closeness_centrality(G_perception_preference)
        nx.set_node_attributes(self.G, perception_preference_clsCent, 'perception_preference_closeness')

        rejection_clsCent = nx.closeness_centrality(G_rejection)
        nx.set_node_attributes(self.G, rejection_clsCent, 'rejection_closeness')

        perception_rejection_clsCent = nx.closeness_centrality(G_perception_rejection)
        nx.set_node_attributes(self.G, perception_rejection_clsCent, 'perception_rejection_closeness')

        # return json_graph.node_link_data(G)
        #return G


    def add_relation(self,source, target, weight, key, criterion):
        self.G.add_edge(source, target, weight=weight, key=key, criterion=criterion)
        # return json_graph.node_link_data(G)
        #return G


    def fetch_direct_sociometric_indexes_per_member(self):
        # G = json_graph.node_link_graph(emosociogram)

        G_with_no_edges = nx.DiGraph(nx.create_empty_copy(self.G, with_data=True))
        # Sp -- Election Status
        G_preference_multigraph = self.G.edge_subgraph(
            [(i, j, k) for i, j, k in self.G.edges if k == 'preference'])
        G_preference_without_all_nodes = nx.DiGraph(G_preference_multigraph)
        G_preference = nx.compose(G_with_no_edges, G_preference_without_all_nodes)
        #print('--------G_preference exei mono ta preference edges?---------')
        # print(json_graph.node_link_data(G_preference))
        #sp = nx.degree(G_preference)
        sp_iter = G_preference.in_degree()
        sp_list = list(sp_iter)
        sp = {key: value for key, value in sp_list}
        nx.set_node_attributes(self.G, sp, 'Sp')

        # Pp -- Perception of election status
        G_perception_preference_multigraph = self.G.edge_subgraph(
            [(i, j, k) for i, j, k in self.G.edges if k == 'perception_preference'])
        G_perception_preference_without_all_nodes = nx.DiGraph(G_perception_preference_multigraph)
        G_perception_preference = nx.compose(G_with_no_edges, G_perception_preference_without_all_nodes)
        pp_iter = G_perception_preference.in_degree()
        pp_list = list(pp_iter)
        pp = {key: value for key, value in pp_list}
        #pp = nx.in_degree_centrality(G_perception_preference)
        nx.set_node_attributes(self.G, pp, 'Pp')

        # Sn -- Rejection status
        G_rejection_multigraph = self.G.edge_subgraph([(i, j, k) for i, j, k in self.G.edges if k == 'rejection'])
        G_rejection_without_all_nodes = nx.DiGraph(G_rejection_multigraph)
        G_rejection = nx.compose(G_with_no_edges, G_rejection_without_all_nodes)
        #sn = nx.in_degree_centrality(G_rejection)
        sn_iter = G_rejection.in_degree()
        sn_list = list(sn_iter)
        sn = {key: value for key, value in sn_list}
        nx.set_node_attributes(self.G, sn, 'Sn')

        # Pn -- Perception of rejection status
        G_perception_rejection_multigraph = self.G.edge_subgraph(
            [(i, j, k) for i, j, k in self.G.edges if k == 'perception_rejection'])
        G_perception_rejection_without_all_nodes = nx.DiGraph(G_perception_rejection_multigraph)
        G_perception_rejection = nx.compose(G_with_no_edges, G_perception_rejection_without_all_nodes)
        #pn = nx.in_degree_centrality(G_perception_rejection)
        pn_iter = G_perception_rejection.in_degree()
        pn_list = list(pn_iter)
        pn = {key: value for key, value in pn_list}
        nx.set_node_attributes(self.G, pn, 'Pn')

        # Rp -- Reciprocal elections
        G_reciprocal_elections = G_preference.to_undirected(reciprocal=True, as_view=False)
        #rp = nx.degree(G_reciprocal_elections_multigraph)
        #rp = G_reciprocal_elections_multigraph.degree()
        rp_iter = G_reciprocal_elections.degree()
        rp_list = list(rp_iter)
        rp = {key: value for key, value in rp_list}
        nx.set_node_attributes(self.G, rp, 'Rp')

        # Rn -- Reciprocal rejections
        G_reciprocal_rejections = G_rejection.to_undirected(reciprocal=True, as_view=False)
        rn_iter = G_reciprocal_rejections.degree()
        rn_list = list(rn_iter)
        rn = {key: value for key, value in rn_list}
        nx.set_node_attributes(self.G, rn, 'Rn')

        # Os -- Feeling Opposition
        G_reverse_rejection = G_rejection.reverse()
        G_intersect_preference_reverse_rejection = nx.intersection(G_preference, G_reverse_rejection)
        G_feeling_opposition = G_intersect_preference_reverse_rejection.to_undirected(
            reciprocal=False, as_view=False)
        os_iter = G_feeling_opposition.degree()
        os_list = list(os_iter)
        os = {key: value for key, value in os_list}
        nx.set_node_attributes(self.G, os, 'OS')

        # Ep -- Positive Expansion
        ep_iter = G_preference.out_degree()
        ep_list = list(ep_iter)
        ep = {key: value for key, value in ep_list}
        nx.set_node_attributes(self.G, ep, 'Ep')

        # En -- Negative Expansion
        en_iter = G_rejection.out_degree()
        en_list = list(en_iter)
        en = {key: value for key, value in en_list}
        nx.set_node_attributes(self.G, en, 'En')

        # SI -- Social Impact
        G_social_impact = nx.compose(G_preference, G_rejection)
        si_iter = G_social_impact.in_degree()
        si_list = list(si_iter)
        si = {key: value for key, value in si_list}
        nx.set_node_attributes(self.G, si, 'SI')

        # PAp -- Guessed right elections perception
        G_right_elections_perception = nx.intersection(G_preference, G_perception_preference)
        pap_iter = G_right_elections_perception.in_degree()
        pap_list = list(pap_iter)
        pap = {key: value for key, value in pap_list}
        nx.set_node_attributes(self.G, pap, 'PAp')

        # PAn -- Guessed right rejections perception
        G_right_rejections_perception = nx.intersection(G_rejection, G_perception_rejection)
        pan_iter = G_right_rejections_perception.in_degree()
        pan_list = list(pan_iter)
        pan = {key: value for key, value in pan_list}
        nx.set_node_attributes(self.G, pan, 'PAn')

        # DO -- Dyadic Overestimation
        G_wrong_rejections_perception = nx.difference(G_perception_rejection, G_rejection)
        do_iter = G_wrong_rejections_perception.in_degree()
        do_list = list(do_iter)
        do = {key: value for key, value in do_list}
        nx.set_node_attributes(self.G, do, 'DO')

        # DU -- Dyadic Underestimation
        G_non_identified_rejections = nx.difference(G_rejection, G_perception_rejection)
        du_iter = G_non_identified_rejections.in_degree()
        du_list = list(du_iter)
        du = {key: value for key, value in du_list}
        nx.set_node_attributes(self.G, du, 'DU')

        # DD -- Dyadic Discrepancy
        G_dyadic_discrepancy = nx.compose(G_wrong_rejections_perception, G_non_identified_rejections)
        dd_iter = G_dyadic_discrepancy.in_degree()
        dd_list = list(dd_iter)
        dd = {key: value for key, value in dd_list}
        nx.set_node_attributes(self.G, dd, 'DD')

        # SoP -- Social Preference
        sop = {key: sp.get(key, 0) - sn.get(key, 0) for key in set(sp) | set(sn)}
        nx.set_node_attributes(self.G, sop, 'SoP')

        # AI -- Association Index - The cohesion within the group
        reciprocal_preference_sum = sum(rp.values())
        ai = round(reciprocal_preference_sum / (self.G.number_of_nodes() * (self.G.number_of_nodes() - 1)), 3)

        # DI -- Dissociation Index
        reciprocal_rejections_sum = sum(rn.values())
        di = round(reciprocal_rejections_sum / (self.G.number_of_nodes() * (self.G.number_of_nodes() - 1)), 3)

        # CI -- Cohesion Index
        ci = 0
        preference_sum = sum(sp.values())
        if (preference_sum != 0):
            ci = round(reciprocal_preference_sum / preference_sum, 3)

        # SI -- Social Intensity Index
        rejections_sum = sum(sn.values())
        si = round((preference_sum + rejections_sum) / (self.G.number_of_nodes() - 1), 3)

        # Pop -- Popularity coefficient
        nodes_minus_one = self.G.number_of_nodes() - 1
        pop = {k: round(v / nodes_minus_one, 3) for k, v in sp.items()}
        nx.set_node_attributes(self.G, pop, 'Pop')

        # Ant -- Antipathy coefficient
        ant = {k: round(v / nodes_minus_one, 3) for k, v in sn.items()}
        nx.set_node_attributes(self.G, ant, 'Ant')

        # CA -- Affective connection
        ca = {k: round(rp[k] / sp[k], 3) if sp[k] != 0 else .0 for k in rp if k in sp}
        nx.set_node_attributes(self.G, ca, 'CA')

        # SS -- Sociometric Status
        ss = {k: round(sp[k] + pp[k] - sn[k] - pn[k], 3) for k in set(sp) | set(pp) | set(sn) | set(pn)}
        nx.set_node_attributes(self.G, ss, 'SS')

        # Expp -- Positive Expansion coefficient
        expp = {k: round(v / nodes_minus_one, 3) for k, v in ep.items()}
        nx.set_node_attributes(self.G, expp, 'Expp')

        # Expn -- Negative Expansion coefficient
        expn = {k: round(v / nodes_minus_one, 3) for k, v in en.items()}
        nx.set_node_attributes(self.G, expn, 'Expn')

        # RPER -- Realistic Perception
        rper = {k: round((pap[k] + pan[k])/(sp[k] + sn[k]), 3) if (sp[k] + sn[k])
                != 0 else .0 for k in set(pap) | set(pan) | set(sp) | set(sn)}
        nx.set_node_attributes(self.G, rper, 'RPER')

        # G.graph = {
        # 'AI':ai,
        # 'DI':di,
        # 'CI':ci,
        # 'SI': si,
        # }
        self.G.graph['AI'] = ai
        self.G.graph['DI'] = di
        self.G.graph['CI'] = ci
        self.G.graph['SI'] = si
        # print(json_graph.node_link_data(G))

        # return json_graph.node_link_data(G)
        # return G


    def add_emosociogram_CEI(self, graph, cei_competences):
        dump = json.dumps(graph)
        # print('dump',type(dump))
        json_object = json.loads(dump)
        # print(type(json_object))
        # print('json_object',json_object['nodes'])

        cei_competences_dump = json.dumps(cei_competences)
        cei_competences_json_object = json.loads(cei_competences_dump)
        # print('cei_competences_json_object',cei_competences_json_object)

        group_emotional_awareness = 0
        group_emotional_regulation = 0
        group_cohesion = 0
        for member_competences in cei_competences_json_object:
            # print('member_competences',member_competences)
            node = next((x for x in json_object['nodes'] if x['id']
                        == member_competences['member_id']), None)
            #node['cei_competences'] = member_competences['cei_competences']
            node['gea'] = json.loads(member_competences['cei_competences'])[0]["value"]
            node['ger'] = json.loads(member_competences['cei_competences'])[1]["value"]
            node['gc'] = json.loads(member_competences['cei_competences'])[2]["value"]
            node['ces'] = json.loads(member_competences['cei_competences'])[3]["value"]

            member_cei_competences_json_object = json.loads(member_competences['cei_competences'])
            for cei_competence in member_cei_competences_json_object:
                if cei_competence['competence'] == 'Group Emotional Awareness':
                    group_emotional_awareness += float(cei_competence['value'])
                if cei_competence['competence'] == 'Group Emotional Regulation':
                    group_emotional_regulation += cei_competence['value']
                if cei_competence['competence'] == 'Group Cohesion':
                    group_cohesion += cei_competence['value']

        number_of_members = len(cei_competences_json_object)
        # cei_set = {"Group Emotional Awareness": round(group_emotional_awareness/number_of_members,2),
        #            "Group Emotional Regulation": round(group_emotional_regulation/number_of_members,2),
        #            "Group Cohesion": round(group_cohesion/number_of_members,2)}
        #json_dump_cei_set = json.dumps(cei_set)
        #json_object['graph']['cei'] = json_dump_cei_set
        if (number_of_members > 0):
            json_object['graph']['gea'] = round(group_emotional_awareness/number_of_members, 2)
            json_object['graph']['ger'] = round(group_emotional_regulation/number_of_members, 2)
            json_object['graph']['gc'] = round(group_cohesion/number_of_members, 2)
        else:
            json_object['graph']['gea'] = 0
            json_object['graph']['ger'] = 0
            json_object['graph']['gc'] = 0
        return json_object


    def calculate_ei_competences(self, response, questions):

        competences = [{"competence": "Self - Awareness", "value": 0},
                       {"competence": "Empathy", "value": 0},
                       {"competence": "Emotional Regulation", "value": 0},
                       {"competence": "Flexibility", "value": 0},
                       {"competence": "Influence", "value": 0},
                       {"competence": "Emotion Expression", "value": 0},
                       {"competence": "Optimism", "value": 0},
                       {"competence": "Assertiveness", "value": 0},
                       {"competence": "Self-motivation", "value": 0},
                       {"competence": "Relationships", "value": 0},
                       {"competence": "Self - esteem", "value": 0},
                       {"competence": "Teamwork", "value": 0},
                       {"competence": "EmoSocio Total", "value": 0}, ]

        for option in response:
            resp = json.loads(json.dumps(option))
            question = questions[resp["question_id"]]
            # print(question.category)
            for competence in competences:
                # print(competence['competence'])
                if competence['competence'] in question.get("category"):
                    # print(competence['competence'],question.category)
                    if (question.get("sign") == "+"):
                        competence["value"] = int(competence["value"]) + int(resp['choice'])
                        # print(competence['competence'],'+',competence["value"])
                    if (question.get("sign") == "-"):
                        competence["value"] = competence["value"] + 6 - int(resp['choice'])
                        # print(competence['competence'],'-',competence["value"])

        total_ei = 0
        for competence in competences:
            if competence["competence"] == "Self - Awareness":
                competence["value"] = round(20 * competence["value"]/6, 2)
            if competence["competence"] == "Empathy":
                competence["value"] = round(20 * competence["value"]/7, 2)
            if competence["competence"] == "Emotional Regulation":
                competence["value"] = round(20 * competence["value"]/10, 2)
            if competence["competence"] == "Flexibility":
                competence["value"] = round(20 * competence["value"]/6, 2)
            if competence["competence"] == "Influence":
                competence["value"] = round(20 * competence["value"]/7, 2)
            if competence["competence"] == "Emotion Expression":
                competence["value"] = round(20 * competence["value"]/7, 2)
            if competence["competence"] == "Optimism":
                competence["value"] = round(20 * competence["value"]/6, 2)
            if competence["competence"] == "Assertiveness":
                competence["value"] = round(20 * competence["value"]/8, 2)
            if competence["competence"] == "Self-motivation":
                competence["value"] = round(20 * competence["value"]/7, 2)
            if competence["competence"] == "Relationships":
                competence["value"] = round(20 * competence["value"]/10, 2)
            if competence["competence"] == "Self - esteem":
                competence["value"] = round(20 * competence["value"]/9, 2)
            if competence["competence"] == "Teamwork":
                competence["value"] = round(20 * competence["value"]/9, 2)
            total_ei += competence["value"]
            if competence["competence"] == "EmoSocio Total":
                competence["value"] = round(total_ei/12, 2)

        # print(competences)

        return competences


    def calculate_collective_competences(self, response, questions):

        competences = [
            {"competence": "Group Emotional Awareness", "value": 0},
            {"competence": "Group Emotional Regulation", "value": 0},
            {"competence": "Group Cohesion", "value": 0},
            {"competence": "Collective EmoSocio", "value": 0}, ]

        for option in response:
            resp = json.loads(json.dumps(option))
            question = questions[resp["question_id"]]
            # print(question.category)
            for competence in competences:
                # print(competence['competence'])
                if competence['competence'] in question.get("category"):
                    # print(competence['competence'],question.category)
                    if (question.get("sign") == "+"):
                        competence["value"] = int(competence["value"]) + int(resp['choice'])
                        # print(competence['competence'],'+',competence["value"])
                    if (question.get("sign") == "-"):
                        competence["value"] = competence["value"] + 6 - int(resp['choice'])
                        # print(competence['competence'],'-',competence["value"])

        total_collective_ei = 0
        for competence in competences:
            if competence["competence"] == "Group Emotional Awareness":
                competence["value"] = round(20 * competence["value"]/4, 2)
            if competence["competence"] == "Group Emotional Regulation":
                competence["value"] = round(20 * competence["value"]/10, 2)
            if competence["competence"] == "Group Cohesion":
                competence["value"] = round(20 * competence["value"]/15, 2)
            total_collective_ei += competence["value"]
            if competence["competence"] == "Collective EmoSocio":
                competence["value"] = round(total_collective_ei/3, 2)

        # print(competences)

        return competences
