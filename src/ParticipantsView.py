# SEMBLA QUE NO S'ESTÀ UTILTZANT
# BORRAR MÉS ENDAVANT
import sys

from PySide6.QtSql import QSqlQuery
from PySide6.QtCore import QFile, QIODevice
from PySide6.QtUiTools import QUiLoader


class ParticipantsView(object):
    def __init__(self):
        ui_file_name = "ui/answer.ui"
        ui_file = QFile(ui_file_name)
        if not ui_file.open(QIODevice.ReadOnly):
            print(f"Cannot open {ui_file_name}: {ui_file.errorString()}")
            sys.exit(-1)
        loader = QUiLoader()
        loader.setLanguageChangeEnabled(True)
        self.view = loader.load(ui_file)
        ui_file.close()
        if not self.view:
            print(loader.errorString())
            sys.exit(-1)

    def getView(self):
        return self.view

    def loadParticipants(self):
        # Check number of questions

        # Check what members have not answered yet.
        # select m.id, m.name, m.surname, m.email, r.code, COUNT(re.id) FROM responsecodes r, members m LEFT JOIN responses re  ON re.test_id = r.test_id AND re.member_id = r.member_id  WHERE r.test_id=111 AND  m.id = r.member_id GROUP BY r.member_id

        # Insering the active members in the responsecodes. HACER ESTO CUANDO SE CREA EL TEST!!!!!!!
        query = QSqlQuery(self.db.getConnection())
        query.prepare(
            "INSERT INTO responsecodes (test_id, member_id, code) SELECT :testId, m.id, 'hola' FROM members m WHERE m.group_id = :groupId AND active = 1")
        query.bindValue(":groupId", self.groupId)
        query.bindValue(":testId", self.testId)
        if not query.exec():
            raise Exception("Could not execute select")
