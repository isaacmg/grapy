from PySide6.QtSql import QSqlTableModel
from PySide6.QtSql import QSqlQuery


class QuestionModel(object):
    def __init__(self, db):
        self.db = db

    def getQuestions(self):
        self.model = QSqlTableModel(db=self.db)
        self.model.setTable('questions')
        self.model.setFilter("survey_id = 2")
        self.model.setEditStrategy(QSqlTableModel.OnManualSubmit)
        self.model.select()
        return self.model

    def getQuestionsQuery(self):
        query = QSqlQuery(self.db)

        if not query.exec(('SELECT id, text, category '
                           'FROM questions '
                           'WHERE survey_id == 2')):
            raise Exception("Could not execute select update")
        questions = []
        while query.next():
            q = Question(query.value(0), query.value(1), query.value(2))
            questions.append(q)
        return questions

        return questions


class Question:
    def __init__(self, id, text, category):
        self.id = id
        self.text = text
        self.category = category
