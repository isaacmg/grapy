from PySide6.QtSql import QSqlQuery


class AnsweringModel(object):
    def __init__(self, db):
        self.db = db

    def saveResults(self, member_id, test_id, questions, selections):

        query = QSqlQuery(self.db)

        # self.selection es un array de diccionarios.
        # Recorremos el array
        for index, sel in enumerate(selections):
            # Recorremos el diccionario
            for choice in sel:
                # From the question list, we take the id of the index
                question_id = questions[index].id
                query.prepare("INSERT INTO responses (member_id, test_id, question_id, choice) "
                              "VALUES (:member_id, :test_id, :question_id, :choice)")

                query.bindValue(":member_id", member_id)
                query.bindValue(":test_id", test_id)
                query.bindValue(":question_id", question_id)
                query.bindValue(":choice", choice)
                if not query.exec():
                    raise Exception("Could not execute insert")
