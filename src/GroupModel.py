from PySide6.QtSql import QSqlTableModel
from PySide6.QtSql import QSqlQuery


class GroupModel(object):
    def __init__(self, db):
        self.db = db

    def getGroups(self):
        self.model = QSqlTableModel(db=self.db)
        self.model.setTable('groups')
        self.model.setEditStrategy(QSqlTableModel.OnManualSubmit)
        self.model.select()
        return self.model

    def getGroupSets(self):
        query = QSqlQuery(self.db)

        if not query.exec("SELECT name FROM groups"):
            raise Exception("Could not execute select")
        members = GroupSets()
        while query.next():
            members.name.add(query.value(0))
        return members

    def getGroupsSetsUpdated(self, notIncluded):
        query = QSqlQuery(self.db)

        query.prepare("SELECT name FROM groups WHERE id != :idUpdated")
        query.bindValue(":idUpdated", notIncluded)
        if not query.exec():
            raise Exception("Could not execute select update")
        members = GroupSets()
        while query.next():
            members.name.add(query.value(0))
        return members

    def checkInsertGroups(self, groupSet, row):
        # Checking that member's name and surname is not already in the member's table
        name = row[0]
        if name in groupSet.name:
            raise Exception("It exists a group with this name")
        else:
            groupSet.name.add(name)

        # Some fields are incorrect.
        if not row[0]:
            raise Exception("Some fields are empty")


class GroupSets:
    def __init__(self):
        self.name = set()
