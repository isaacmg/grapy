import sys

from PySide6.QtCore import QFile, QIODevice
from PySide6.QtUiTools import QUiLoader

class Waiting(object):
    def __init__(self):
        ui_file_name = "ui/waiting.ui"
        ui_file = QFile(ui_file_name)
        if not ui_file.open(QIODevice.ReadOnly):
            print(f"Cannot open {ui_file_name}: {ui_file.errorString()}")
            sys.exit(-1)
        loader = QUiLoader()
        loader.setLanguageChangeEnabled(True)
        self.view = loader.load(ui_file)
        ui_file.close()
        if not self.view:
            print(loader.errorString())
            sys.exit(-1)
