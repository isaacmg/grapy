//import * as d3 from "d3";
import Graph from "./Graph.js";

// Subclass to generate forced group graph.
export default function ForcedGraph(options){
  this.manageOptions(options);

  this.addProperties = function(){
      this.svg = this.svg.style("cursor","move");
  };

  this.addMarkers = function(){
    // Defining Arrows
    this.markerSVG = this.svg.append("defs").selectAll("marker")
       .data(this.markersRadius)
       .join("marker")
         .attr("id", function(d){return `arrow-${(d*2 + 8)}`;})
         .attr("viewBox", "0 -5 10 10")
         .attr("refX", function(d){return d + 20;})
         .attr("refY", 0)
         .attr("markerWidth", 12)
         .attr("markerHeight", 8)
         .attr("orient", "auto")
         .append("path")
         .attr("fill", "grey")
         .attr("d", "M0,-2L20,0L0,2");
  }
  this.restart = function(){
    if(this.isForceActive){
        // Apply the general update pattern to the nodes.
        this.node = this.node.data(this.graph.nodes, function(d) { return d.node_id;});
        d3.selectAll("circle").remove();
        d3.selectAll("text").remove();

        this.node = this.node.join("g").attr("class","node");


        this.node.exit().transition()
            .attr("r", 0)
            .remove();
        var that = this;
        this.circle = this.node.append("circle")
                 .attr("stroke-width", 1).attr("class", "nodesCircles")
                 .attr("node_id", function(d){return d.node_id; }).attr("id", function(d){return d.id; })
                 .call((node) => { node.transition().attr("r", (d) => { return this.radius(d); }); });


        this.node.call(d3.drag()
                 .on("start", function(d){ that.dragstarted(d); })
                 .on("drag", function(d){ that.dragged(d); })
                 .on("end", function(d){ that.dragended(d); }))
                 .on("dblclick", function(d){ that.dblclickAction(d); })
                 .on("mouseover", function(d){ that.set_highlight(d); })
                 .on("mouseout",  function(){ that.exit_highlight(); });


        // Es concreta el color dels nodes depenen de la variable colorFunction
        this[this.colorFunction]();

        this.text = this.node.append("text")
            .text(function(d) { return d.node_id; })
            .style("fill","#000")
            .style("font-size", "10px")
            .attr("x", -8)
            .attr("y", 2);

        // Adding the id to the node in order to facilitate the recognition of the no link nodes in th eticked function.
        this.node.merge(this.node)
          .attr("id", function(d){return d.id; })
          .attr("nn", function(d){return that.numberConnections(d); });

        // Zoom???
        //add zoom capabilities
        this.svg.call(d3.zoom()
          .extent([[0, 0], [this.w, this.h]])
          .scaleExtent([0.1, 10])
          .on("zoom", function() {
            that.g.attr("transform", d3.event.transform.translate(that.w/2,that.h/2))
            })
        );

        // Apply the general update pattern to the links.
        this.link = this.link.data(this.links, function(d) { return d.source.id + "-" + d.target.id; });
        // Keep the exiting links connected to the moving remaining nodes.
        // When no links and no nodes, this.link is Array. Normally it is an Object.
        if (!Array.isArray(this.link)){
            this.link.exit().transition()
                .attr("stroke-opacity", 0)
                .attrTween("x1", function(d) { return function() { return d.source.x; }; })
                .attrTween("x2", function(d) { return function() { return d.target.x; }; })
                .attrTween("y1", function(d) { return function() { return d.source.y; }; })
                .attrTween("y2", function(d) { return function() { return d.target.y; }; })
                .remove();
            this.link = this.link.join("line").attr("stroke-width", 1.5)
                .call((link) => { link.transition().attr("stroke-opacity", 1); })
                .attr("marker-end", (d) => {return `url(#arrow-${this.radius(d.target)}`;})
                .merge(this.link);

        }

        this.simulation = d3.forceSimulation(this.graph.nodes);
        if (this.availables.forceManyBody) this.simulation.force("charge", d3.forceManyBody().strength(this.forces.forceStrength).distanceMin(this.forces.chargeDistanceMin).distanceMax(this.forces.chargeDistanceMax));
        if (this.availables.forceLink) this.simulation.force("link", d3.forceLink(this.links).id(function(d) { return d.id; }).distance(this.forces.forceLinks).strength(this.forces.forceLinkStrength));
        if (this.availables.forceCenter) this.simulation.force("center", d3.forceCenter()).alphaMin(0.01);
        if (this.availables.forceCollide) this.simulation.force("collide", d3.forceCollide().strength(this.forces.forceCollide).radius(function(d) { return d.r; }));
        this.simulation.on("tick", this.ticked);


        // Esto hay que ponerlo para que dentro de ticked se pudean ver node y link
        this.simulation.node = this.node;
        this.simulation.link = this.link;
        this.simulation.noLinkNodes = this.noLinkNodes;
        this.simulation.socioType = this.socioType;

        // Update and restart the simulation.
        this.simulation.nodes(this.graph.nodes);
        //if (this.availables.forceLink) this.simulation.force("link").links(this.links);
        this.simulation.alpha(1).restart();
      }

  }

  this.radius = function(a){
    return this.numberConnections(a)*2 + 8;
  }
  this.dblclickAction = function(d){
    alert(`x:${d.x}, y:${d.y}, id:${d.id}, node_id:${d.node_id}`);
  }
}
ForcedGraph.prototype = new Graph();
