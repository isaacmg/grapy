// Function that manage all kind of graphs.
// Main function to create different kinds of graphs.
//import * as d3 from "d3";
import ForcedGraph from "./ForcedGraph.js";
import RadialGroupGraph from "./RadialGroupGraph.js";
import RadialEgoDijkstraGraph from "./RadialEgoDijkstraGraph.js";
import RadialEgoSocioGraph from "./RadialEgoSocioGraph.js";
import ClassGraph from "./ClassGraph.js";

var socio = (function() {
        // Store the graph
        var graph;
        // Stores the id of the div element where d3 will create the graph
        var svg;
        // Stores the graph type
        var graphType;
        // Map to translate graphType to idGraphType
        var fromGraphType = new Map();
        fromGraphType.set(0,"forced").set(1,"radialGroup").set(2,"radialEgo").set(3,"radialEgoDijkstra").set(4,"class");

        // Main function to create the graph. It receives the options to create the graph
        // graphType: 0: ForcedGraph, 1: RadialGroupGraph, 2: RadialEgoGraph
        // sociotype: 0: Preference, 1: Perception Preference, 2: Perception Rejection, 3: Rejection
        // svg: Id of the div element
        // w: weight of the svg element
        // h: height of the svg element
        // jsonVar: Json storing the graph
        // file: Alternative way to store the json graph via file
        // member: Id for EgoGraph
        //color: Number that defines the coloring of the nodes
        function createGraph(options){
          options.graphType = options.graphType || 0;
          svg = options.svg || "graph";
          graphType = options.graphType;

          switch(fromGraphType.get(graphType)){
              case "forced":
                graph = new ForcedGraph(options);
                break;
              case "radialGroup":
                graph = new RadialGroupGraph(options);
                break;
              case "radialEgo":
                graph = new RadialEgoSocioGraph(options);
                break;
              case "radialEgoDijkstra":
                graph = new RadialEgoDijkstraGraph(options);
                break;
              case "class":
                graph = new ClassGraph(options);
                break;

          }

          // The json graph var by file or by jsonVar
          if (options.file){
            d3.json(options.file).then((json) => {
              sequence(json);
            });
          } else if (options.jsonVar){
            sequence(options.jsonVar);
          }
        }

        // Sequence of the steps to create and visualize the graph. The core of the graph creation
        function sequence(json){
          graph.createSVG(svg);
          graph.addProperties();
          graph.setGraph(json);
          graph.calcEdges();
          graph.calcNodes();
          graph.addMarkers();
          graph.simulation();
        }

        // Can change the color and the socioType when the graph is already created.
        function changeParameters(options){
          graph.color = options.color !== undefined ? options.color : graph.color || 0;
          graph.socioType = options.socioType !== undefined ? options.socioType : graph.socioType || 0;

          //graph.socioType = options.socioType;
          //graph.color = options.color;
          graph.colorFunction= graph.fromColor.get(graph.color);

          if (!graph.isUndefined(graph.sortedLinksByKey[graph.socioType]))
              graph.links = graph.sortedLinksByKey[graph.socioType].values;
          graph.restart();
        }
        // Can change the forces that are interacting
        function changeAvailables(availables){
          if (graph){
            graph.setAvailabilityForces(availables);
            graph.updateForces();
          }
        }

        // Getter for graphType
        function getGraphType(){
          return graphType;
        }
        // Function that changes the charge force of the graph.
        function setForces(forces){
          if (graph){
            graph.setForces(forces);
            graph.updateForces();
          }
        }
        //Function that restart the forces to the original values.
        function restartForces(){
          if (graph){
            graph.restartForces();
            graph.updateForces();
          }
        }

        //Function that enables to restart the simulation.
        function restart(){
          if (graph){
            graph.restart();
          }
        }

        //Function that switch between enable and disable the forces in the graph.
        function forceSwitch(){
          if (graph){
            graph.forceSwitch();
          }
        }

        // Functions that are public for the object socio
        return {
          createGraph: createGraph,
          changeParameters: changeParameters,
          changeAvailables: changeAvailables,
          getGraphType: getGraphType,
          setForces: setForces,
          restartForces: restartForces,
          restart: restart,
          forceSwitch: forceSwitch,
        };
})();
export default socio;
//var test = (function Graph(){
//  var g = socio;

//  function getGraph(){
//    return g;
//  }
//  return {
//    getGraph: getGraph,
//  };

//})();
