//import * as d3 from "d3";
import Graph from "./Graph.js";

// Subclass to generate radial group graph.
export default function RadialGroupGraph(options){
  this.manageOptions(options);

  // Defining Arrows
  this.addMarkers = function(){
    let markers = ["triangle"];
    this.markerSVG = this.svg.append("defs").selectAll("marker")
       .data(markers)
       .join("marker")
        .attr("id", "triangle")
        .attr("viewBox", "0 -5 10 10")
        .attr("refX", 22)
        .attr("refY", 0)
        .attr("markerWidth", 12)
        .attr("markerHeight", 8)
        .attr("orient", "auto")
        .attr("fill", "grey")
        .append("path")
        .attr("d", "M0,-4L10,0L0,4");
  }

  this.restart = function(){

    // The number of links that is received by every node
    let linksReceived,max,min,scaleCircle;
    linksReceived = this.linksPerNode(this.sortedLinksByKey);
    max = this.maxLinksPerNode(linksReceived);
    min = this.minLinksPerNode(linksReceived);
    scaleCircle = d3.scaleLinear()
    .domain([min,max])
    .range([0, 200]);

    // Delete nodes and texts
    this.node = this.node.data(this.graph.nodes, function(d) { return d.node_id;});
    d3.selectAll("circle").remove();
    d3.selectAll("text").remove();

    // Adding the concentric circles (diana)
    for (let i = 1; i <= (max-min); i++){
      this.diana.append("circle").attr("stroke","brown").attr("r",i*(200/(max-min))).attr("stroke-opacity",0.5).attr("fill","none");
    }

    // Event definitions for the nodes
    this.node = this.node.join("g").attr("class","node")

    // Defining the links properties
    this.link = this.link.data(this.links, function(d) { return d.source.id + "-" + d.target.id; });

    // When no links and no nodes, this.link is Array. Normally it is an Object.
    if (!Array.isArray(this.link)){
        this.link.exit().transition()
            .attr("stroke-opacity", 0)
            .attrTween("x1", function(d) { return function() { return d.source.x; }; })
            .attrTween("x2", function(d) { return function() { return d.target.x; }; })
            .attrTween("y1", function(d) { return function() { return d.source.y; }; })
            .attrTween("y2", function(d) { return function() { return d.target.y; }; })
            .remove();

        // Defining the link svg properties
        this.link = this.link.join("line").attr("stroke-width", 1).attr("stroke", "grey")
            .call(function(link) { link.transition().attr("stroke-opacity", 1); })
            .attr("marker-end", "url(#triangle)")
          .merge(this.link);
    }

    // Defining the nodes circles
    this.circle = this.node.append("circle")
         .attr("fill", function(d) { return d.sex === "F" ? "brown" : "steelblue"; })
         .attr("stroke", "grey").attr("stroke-width", 1.5)
         .call(function(node) { node.transition().attr("r", 8); })

    var that = this;
    this.circle.on("mouseover", function(d){ that.set_highlight(d); })
               .on("mouseout",  function(){ that.exit_highlight(); });


    // Coloring the nodes
    this[this.colorFunction]();

    // Showing the node texts
    this.text = this.node.append("text")
         .text(function(d) { return d.node_id; })
         .style("fill","#000")
         .style("font-size","10px")
         .attr("x", 10)
         .attr("y", 0)


    this.node.merge(this.node);

    // Starting the simulation
    this.simulation = d3.forceSimulation(this.graph.nodes)
          .force("r", d3.forceRadial(function(d) { return scaleCircle(max-linksReceived[d.node_id]); }).strength(0.4))
          .alpha(1).alphaMin(0.001).velocityDecay(0.2)
          .on("tick", this.ticked);

    // This is necessary to pass the node and link variables to the ticked function
    this.simulation.node = this.node;
    this.simulation.link = this.link;
  }

}
RadialGroupGraph.prototype = new Graph();
