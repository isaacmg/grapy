//import * as d3 from "d3";
import Graph from "./Graph.js";

export default function RadialEgoDijkstraGraph(options){
  this.manageOptions(options);
  this.member = options.member;
  this.max = 0;

  this.addMarkers = function(){
    let markers = ["triangle"];
    // Defining Arrows
    this.markerSVG = this.svg.append("defs").selectAll("marker")
       .data(markers)
       .join("marker")
        .attr("id", "triangle")
        .attr("viewBox", "0 -5 10 10")
        .attr("refX", 12)
        .attr("refY", 0)
        .attr("markerWidth", 6)
        .attr("markerHeight", 6)
        .attr("orient", "auto")
        .attr("fill", "grey")
        .append("path")
        .attr("d", "M0,-4L10,0L0,4");
  },
  this.startSimulation = function(){
    this.links = this.sortedLinksByKey[0].values;
    this.links = this.dijkstra(this.memberOnNodes(this.member));
    this.max = this.maxDistanceThatIsNotInfinity();
    this.eraseInfinity(this.max);
    this.graph.nodes.forEach(function (d) {
      console.log("nodeId:"+d.node_id+ " distance:"+d.distance);
    });
  },
  this.memberOnNodes = function(memberId){
    var i = 0;
    var result = this.graph.nodes[0];
    while (this.graph.nodes[i].id != memberId && i < this.graph.nodes.length){
      i++;
    }
    if (i < this.graph.nodes.length) result = this.graph.nodes[i];
    return result;
  },
  this.maxDistanceThatIsNotInfinity = function(){
    let aux = -1;
    this.graph.nodes.forEach(function (d) {
      if (d.distance > aux && d.distance != Infinity) aux = d.distance;
    });
    return aux;
  },
  this.eraseInfinity = function(max){
    this.graph.nodes.forEach(function (d) {
      if (d.distance === Infinity)
        d.distance = max + 1;
    });

  },

  this.restart = function(){
    let scaleCircle = d3.scaleLinear()
    .domain([0,this.max+1])
    .range([0, 200]);

    this.node = this.node.data(this.graph.nodes).join("g").attr("class","node");

    // Afegim els cercles concentrics
    for (let i = 1; i <= (this.max+1); i++){
      this.diana.append("circle").attr("stroke","brown").attr("r",i*(200/(this.max+1))).attr("stroke-opacity",0.5).attr("fill","none");
    }

    this.circle = this.node.append("circle")
         .attr("fill", function(d) { return d.sex === "F" ? "brown" : "steelblue"; })
         .attr("stroke", "grey").attr("stroke-width", 1.5)
         .call(function(node) { node.transition().attr("r", 8); })

         var that = this;
         this.circle.call(d3.drag()
                  .on("start", function(d){ that.dragstarted(d); })
                  .on("drag", function(d){ that.dragged(d); })
                  .on("end", function(d){ that.dragended(d); }))


     this.text = this.node.append("text")
         .text(function(d) { return d.node_id; })
         .style("fill","#000")
         .style("font-size","10px")
         .attr("x", 10)
         .attr("y", 0)

    // Definim com seran les propietats de les arestes
    this.link = this.link.data(this.links)
                .enter().append("line").attr("stroke-width", 2).attr("marker-end", "url(#triangle)")
                .call(function(link) { link.transition().attr("stroke-opacity", 1); });

      this.simulation = d3.forceSimulation(this.graph.nodes)
          .force('link', d3.forceLink().links(this.links).distance(200/(this.max+1)))
          //.force('charge', d3.forceManyBody().strength(-1))
          .force("charge", d3.forceCollide().radius(15))
          .force("r", d3.forceRadial(function(d) { return scaleCircle(d.distance); }).strength(0.7))
          .alpha(1).alphaMin(0.005).velocityDecay(0.1)
          .on("tick", this.ticked);

      // Esto hay que ponerlo para que dentro de ticked se pudean ver node y link
      this.simulation.node = this.node;
      this.simulation.link = this.link;

  },
  this.dijkstra = function(src) {
      var linksDijkstra = [];
      var source = src;
      // Creamos una cola q
      var q = [];
      // Agregamos origen a la cola Q
      source.distance = 0;
      q.push(source);
      // Creamos una cola para los nodos no visitados
//        var unvisited = [];
      // los añadimos todos excepto el src
      this.graph.nodes.forEach(function (d) {
          if (d != src) {
              d.distance = Infinity;
//              unvisited.push(d);
              d.visited = false;
          }
      });

      //  mientras Q no este vacío:
      while (q.length != 0){
        //sacamos un elemento de la cola Q llamado v
        var v = q.shift();
        // para cada vertice w adyacente a v en el Grafo:
        var linksV = [];
        if (!this.isUndefined(this.sortedLinksByKey[this.socioType])){
          this.sortedLinksByKey[this.socioType].values.forEach(function(link){
            if (v.id == link.source.id){
              linksV.push(link);
              //console.log("v.id:"+v.id + " link.source.id:"+link.source.id + " link.target.id: "+ link.target.id);
            }
          });
        }

        var that = this;
        linksV.forEach(function(link) {
          var w = link.target;
          //console.log(w);
          //si w no ha sido visitado:
          if (!w.visited){
            // marcamos como visitado w
            w.visited = true;
            linksDijkstra.push({weight: link.weight, source: link.source, target: link.target, key: that.socioType});
            var dist = v.distance + 1;
            w.distance = Math.min(dist, w.distance);

            // insertamos w dentro de la cola Q
            q.push(w);
          }

/*
HAY QUE VISITAR LOS NODOS DE DONDE VIENES??????????????????????????????????????
          var x = link.source;
          //si w no ha sido visitado:
          if (!x.visited){
            // marcamos como visitado w
            x.visited = true;
            //linksDijkstra.push({weight: link.weight, source: link.source, target: link.target, key: "preference"});
            dist = v.distance + 1;
            x.distance = Math.min(dist, x.distance);
            // insertamos w dentro de la cola Q
            q.push(x);
          }
*/
        });
      }

/*      var aux = [];
      this.graph.nodes.forEach(function (d) {
        var p = {id:d.id,distance:d.distance};
        aux.push(p);
      });
      */
      return linksDijkstra;
    }
}
RadialEgoDijkstraGraph.prototype = new Graph();
