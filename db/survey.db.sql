BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "questions" (
	"id"	INTEGER NOT NULL,
	"text"	VARCHAR(500) NOT NULL,
	"created_at"	DATETIME,
	"survey_id"	INTEGER,
	"category"	TEXT,
	"sign"	INTEGER,
	"criterion"	TEXT,
	"optional"	INTEGER,
	PRIMARY KEY("id"),
	FOREIGN KEY("survey_id") REFERENCES "surveys"("id")
);
CREATE TABLE IF NOT EXISTS "surveys" (
	"id"	INTEGER NOT NULL,
	"name"	TEXT,
	"created_at"	DATETIME,
	"category"	TEXT,
	"max_age"	INTEGER,
	"min_age"	INTEGER,
	"time_to_complete"	INTEGER,
	PRIMARY KEY("id")
);
CREATE TABLE IF NOT EXISTS "members" (
	"id"	INTEGER NOT NULL,
	"name"	VARCHAR(500) NOT NULL,
	"surname"	VARCHAR(500) NOT NULL,
	"sex"	BLOB NOT NULL,
	"age"	INTEGER NOT NULL DEFAULT 40,
	"active"	INTEGER NOT NULL DEFAULT 1,
	"email"	VARCHAR(120),
	"group_id"	INTEGER,
	"abbreviation"	VARCHAR(200) NOT NULL,
	"created_at"	DATETIME NOT NULL,
	FOREIGN KEY("group_id") REFERENCES "groups"("id"),
	PRIMARY KEY("id")
);
CREATE TABLE IF NOT EXISTS "users" (
	"id"	INTEGER NOT NULL,
	"name"	VARCHAR(500) NOT NULL,
	"surname"	VARCHAR(500) NOT NULL,
	"sex"	BLOB NOT NULL,
	"age"	INTEGER NOT NULL DEFAULT 40,
	"email"	VARCHAR(120),
	"role"	VARCHAR(120) NOT NULL DEFAULT 'user',
	"password"	VARCHAR(255),
	"confirmationHash"	VARCHAR(255),
	"confirmed"	INTEGER,
	"recoverHash"	VARCHAR(255),
	"recoverDate"	DATETIME,
	"confirmationDate"	DATETIME,
	"created_at"	DATETIME NOT NULL,
	PRIMARY KEY("id")
);
CREATE TABLE IF NOT EXISTS "tests" (
	"id"	INTEGER NOT NULL,
	"created_at"	DATETIME,
	"group_id"	INTEGER,
	"survey_id"	INTEGER,
	"name"	VARCHAR(500),
	FOREIGN KEY("survey_id") REFERENCES "surveys"("id"),
	FOREIGN KEY("group_id") REFERENCES "groups"("id"),
	PRIMARY KEY("id")
);
CREATE TABLE IF NOT EXISTS "responses" (
	"id"	INTEGER NOT NULL,
	"member_id"	INTEGER,
	"test_id"	INTEGER,
	"question_id"	INTEGER,
	"choice"	INTEGER,
	FOREIGN KEY("test_id") REFERENCES "tests",
	FOREIGN KEY("member_id") REFERENCES "members"("id"),
	FOREIGN KEY("question_id") REFERENCES "questions"("id"),
	PRIMARY KEY("id")
);
CREATE TABLE IF NOT EXISTS "responsecodes" (
	"id"	INTEGER NOT NULL,
	"test_id"	INTEGER,
	"member_id"	INTEGER,
	"code"	VARCHAR(500),
	PRIMARY KEY("id"),
	FOREIGN KEY("test_id") REFERENCES "tests"("id"),
	FOREIGN KEY("member_id") REFERENCES "members"("id")
);
CREATE TABLE IF NOT EXISTS "groups" (
	"id"	INTEGER NOT NULL,
	"name"	TEXT,
	"organization"	TEXT,
	"created_at"	DATETIME,
	PRIMARY KEY("id")
);
INSERT INTO "surveys" ("id","name","created_at","category","max_age","min_age","time_to_complete") VALUES (2,'Socio(S)','2020-10-05 03:15:28.434835','Sociometric',NULL,NULL,15);
INSERT INTO "surveys" ("id","name","created_at","category","max_age","min_age","time_to_complete") VALUES (4,'SocioDigitalCommunity','2020-09-25 11:51:43.253329','Sociometric',NULL,NULL,30);
INSERT INTO "surveys" ("id","name","created_at","category","max_age","min_age","time_to_complete") VALUES (7,'Socio(W)','2020-10-05 03:15:28.434835','Sociometric',NULL,NULL,15);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (119,'{"en": "Who would you like to socialize with in your spare time?",
"es": "¿Con quién te gustaría socializar en tu tiempo libre?",
"gr": "Με ποιους θα ήθελες να έχεις διάδραση κατά τον ελεύθερο χρόνο σου;"}','2020-09-25 11:51:43.255567',2,'preference','','social',0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (120,'{"en": "Who do you think they would like to socialize with you  in their spare time?","es": "¿Con quién crees que le gustaría socializar contigo en su tiempo libre?","gr": "Ποιοι θεωρείς πως θα ήθελαν να έχουν διάδραση μαζί σου κατά τον ελεύθερο χρόνο τους;"
}','2020-09-25 11:51:43.255567',2,'perception_preference','','social',0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (121,'{"en": "Who would you rather not socialize with in your spare time?",
"es": "¿Con quién preferirías no socializar en tu tiempo libre?",
"gr": "Με ποιους θα προτιμούσες να αποφύγεις τη διάδραση κατά τον ελεύθερο χρόνο σου;"}','2020-09-25 11:51:43.255567',2,'rejection','','social',0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (122,'{"en": " Who do you think they would rather not socialize with you in their spare time?","es": "¿Quién crees que no preferiría socializar contigo en su tiempo libre?","gr": "Ποιοι πιστεύεις πως θα προτιμούσαν να αποφύγουν τη διάδραση μαζί σου κατά τον ελεύθερο χρόνο τους;"}
','2020-09-25 11:51:43.255567',2,'perception_rejection','','social',0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (123,'{"en": "Who would you like to cooperate with?",
"es": "¿Con quién te gustaría cooperar?",
"gr": "Με ποιους θα ήθελες να συνεργαστείς;"}','2020-09-25 11:51:43.255567',7,'wpreference','','work',0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (124,'{"en": "Who do you think they would like to cooperate with you?",
"es": "¿A quién crees que le gustaría cooperar contigo?",
"gr": "Ποιοι πιστεύεις πως θα ήθελαν να συνεργαστούν μαζί σου;"}','2020-09-25 11:51:43.255567',7,'wperception_preference','','work',0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (125,'{"en": "Who would you rather not cooperate with?",
"es": "¿Con quién preferirías no cooperar?",
"gr": "Με ποιους θα προτιμούσες να μην συνεργαστείς;"}','2020-09-25 11:51:43.255567',7,'wrejection','','work',0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (126,'{"en": "Who do you think they would rather not like to cooperate with you?","es": "¿Quién crees que no preferiría cooperar contigo?","gr": "Ποιοι πιστεύεις πως θα προτιμούσαν να αποφύγουν να συνεργαστούν μαζί σου;"}','2020-09-25 11:51:43.255567',7,'wperception_rejection','','work',0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (161,'{"en": "With which members of the community do you feel you share common points of view?",
"es": "¿Con qué miembros de la comunidad te sientes más cerca en términos de opiniones?",
"gr": "Με ποια μέλη απο την κοινότητα νιώθεις πιο κοντά σε επίπεδο απόψεων;"}','2020-09-25 11:51:43.255567',4,'preference','','social',0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (162,'{"en": "Which members of the community do you think they agree with your points of view?",
"es": "¿Qué miembros de la comunidad crees que están de acuerdo con tus opiniones?",
"gr": "Ποια μέλη της κοινότητας νομίζεις ότι συμφωνούν με τις απόψεις σου;"}','2020-09-25 11:51:43.255567',4,'perception_preference','','social',0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (163,'{"en": "With which members of the community you feel you do NOT share common points of view?",
"es": "¿Con qué miembros de la comunidad te sientes más distante en términos de opiniones?",
"gr": "Με ποια μέλη απο την κοινότητα νιώθεις ότι απέχεις σε επίπεδο απόψεων;"}','2020-09-25 11:51:43.255567',4,'rejection','','social',1);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (164,'{"en": "Which members of the community do you think they disagree with your points of view?",
"es": "¿Qué miembros de la comunidad crees que ΝΟ están de acuerdo con tus opiniones?",
"gr": "Ποια μέλη της κοινότητας νομίζεις ότι ΔΕΝ συμφωνούν με τις απόψεις σου;"}','2020-09-25 11:51:43.255567',4,'perception_rejection','','social',1);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (165,'{"en": "Jealousy or rivality are present among the members of our community", "es": "Los celos o la rivalidad están presentes entre los miembros de nuestro grupo", "gr": "Είναι εμφανής η ύπαρξη ζήλιας και ανταγωνιστικότητας ανάμεσα στα μέλη της κοινότητας"}','2020-09-25 11:51:43.255567',4,'Group Emotional Regulation','-',NULL,0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (166,'{"en": "I have a strong sense of belonging to this community", "es": "Tengo un fuerte sentido de pertenencia a este grupo", "gr": "Νιώθω έντονα το αίσθημα του “ανήκειν” σε αυτή την κοινότητα"}','2020-09-25 11:51:43.255567',4,'Group Cohesion','+',NULL,0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (168,'{"en": "Community members are receptive to feedback and criticism", "es": "Los miembros del grupo son receptivos a los comentarios y críticas", "gr": "Τα μέλη της κοινότητας είναι δεκτικά σε ανατροφοδότηση και κριτική"}','2020-09-25 11:51:43.255567',4,'Group Cohesion','-',NULL,0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (169,'{"en": "Personality conflicts are evident in our community", "es": "Existen conflictos de personalidad en nuestro grupo", "gr": "Οι συγκρούσεις προσωπικότητας είναι εμφανείς στην κοινότητα μας"}','2020-09-25 11:51:43.255567',4,'Group Emotional Regulation','-',NULL,0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (170,'{"en": "The group members feel comfortable in expressing disagreements within the community", "es": "Los miembros del grupo se sienten cómodos al expresar desacuerdos en el grupo", "gr": "Τα μέλη της κοινότητας αισθάνονται άνετα να εκφράσουν τη διαφωνία τους σχετικά με πράγματα που συμβαίνουν στην κοινότητα"}','2020-09-25 11:51:43.255567',4,'Group Cohesion','+',NULL,0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (171,'{"en": "Community members always know others’ emotions from their comments", "es": "Como grupo reconocemos las emociones de los demás por sus comportamientos", "gr": "Τα μέλη της κοινότητας αναγνωρίζουν τα συναισθήματα των άλλων μελών με βάση τα σχόλια τους"}','2020-09-25 11:51:43.255567',4,'Group Emotional Awareness','+',NULL,0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (172,'{"en": "I do not enjoy the social interaction occurring in this community", "es": "No disfruto del clima que tenemos como equipo", "gr": "Δεν απολαμβάνω την κοινωνική αλληλεπίδραση με τα υπόλοιπα μέλη της κοινότητας"}','2020-09-25 11:51:43.255567',4,'Group Cohesion','-',NULL,0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (174,'{"en": "Community members are able to control their temper so that they can handle disagreements rationally", "es": "Sabemos gestionar nuestras emociones ante situaciones difíciles de manera racional", "gr": "Τα μέλη της κοινότητας είναι σε θέση να συγκρατούν την ψυχραιμία τους έτσι ώστε να μπορούν να αντιμετωπίσουν τις διαφωνίες που εμφανίζονται με λογικό τρόπο"}','2020-09-25 11:51:43.255567',4,'Group Emotional Regulation','+',NULL,0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (175,'{"en": "We make an effort to understand each others perspectives", "es": "Nos esforzamos por comprender las perspectivas de los demás", "gr": "Σε αυτή την κοινότητα προσπαθούμε να καταλάβουμε την οπτική γωνία σκέψης κάθε μέλους"}','2020-09-25 11:51:43.255567',4,'Group Emotional Awareness','+',NULL,0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (178,'{"en": "Criticism was sometimes thrown without consideration for people feelings", "es": "A veces se crítica sin tener en cuenta los sentimientos de los demás", "gr": "Τα μέλη της κοινότητας ασκούν κριτική χωρίς να σκεφτούν τα συναισθήματα που προκαλούνται σε άλλα μέλη"}','2020-09-25 11:51:43.255567',4,'Group Emotional Regulation','-',NULL,0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (179,'{"en": "We take time to talk about frustrations and other feelings in our community", "es": "Dedicamos un tiempo para hablar de nuestras frustraciones y otros sentimientos", "gr": "Στην κοινότητα μας, συζητάμε για τις απογοητεύσεις και άλλα συναισθήματα που προκύπτουν"}','2020-09-25 11:51:43.255567',4,'Group Emotional Regulation','+',NULL,0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (180,'{"en": "Some members are quiet, and minimal attempts are made to include them", "es": "Algunos miembros están callados y no se hacen los intentos suficientes para incluirlos", "gr": "Ορισμένα μέλη είναι διστακτικά (μη συμμετοχικά) και γίνονται ελάχιστες προσπάθειες για να ενταχθούν στην κοινότητα"}','2020-09-25 11:51:43.255567',4,'Group Cohesion','-',NULL,0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (181,'{"en": "I feel vulnerable in this group", "es": "Me siento vulnerable en este grupo", "gr": "Νιώθω ευάλωτος σε αυτήν την κοινότητα"}','2020-09-25 11:51:43.255567',4,'Group Cohesion','-',NULL,0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (182,'{"en": "We provide constructive feedback to members whose behaviour is unacceptable", "es": "Hacemos comentarios constructivos a los compañeros/as cuyo comportamiento es inaceptable", "gr": "Σε αυτή την κοινότητα, παρέχουμε εποικοδομητικά σχόλια σε μέλη των οποίων η συμπεριφορά είναι απαράδεκτη"}','2020-09-25 11:51:43.255567',4,'Group Emotional Awareness','+',NULL,0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (183,'{"en": "We use humor to help us ease tension in the community", "es": "Utilizamos el humor para aliviar la tensión en el grupo", "gr": "Κάνουμε χρήση του χιούμορ (αστεϊσμών) για να μειώσουμε την ένταση στην κοινότητα"}','2020-09-25 11:51:43.255567',4,'Group Emotional Regulation','+',NULL,0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (184,'{"en": "Members of this community act in a way that show they care about each other", "es": "Los miembros de este grupo expresan su cuidado uno por el otro", "gr": "Τα μέλη αυτής της κοινότητας δείχνουν το ενδιαφέρον τους προς τα υπόλοιπα μέλη"}','2020-09-25 11:51:43.255567',4,'Group Cohesion','+',NULL,0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (185,'{"en": "Often there is tension among the members of our community", "es": "A menudo hay tensión entre los miembros de nuestro grupo", "gr": "Επικρατεί συχνά ένταση μεταξύ των μελών της κοινότητας μας"}','2020-09-25 11:51:43.255567',4,'Group Emotional Regulation','-',NULL,0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (186,'{"en": "Despite community tensions, members tend to stick together", "es": "A pesar de las tensiones del grupo, los miembros tienden a mantenerse unidos", "gr": "Παρά τις ομαδικές εντάσεις, τα μέλη παραμένουν ενωμένα μεταξύ τους"}','2020-09-25 11:51:43.255567',4,'Group Cohesion','+',NULL,0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (187,'{"en": "It was difficult to calm down quickly when we got mad at each other", "es": "Nos cuesta calmarnos rápidamente cuando nos enojamos entre nosotros", "gr": "Είναι δύσκολο να ηρεμήσουμε γρήγορα όταν υπάρχει θυμός και ένταση μεταξύ των μελών της κοινότητας"}','2020-09-25 11:51:43.255567',4,'Group Emotional Regulation','-',NULL,0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (189,'{"en": "We have developed methods to help us tackle emotionally charged issues", "es": "Sabemos cómo gestionar situaciones cargadas emocionalmente", "gr": "Στην κοινότητα μας έχουμε αναπτύξει μεθόδους για να βοηθάμε τα μέλη που είναι συναισθηματικά φορτισμένα"}','2020-09-25 11:51:43.255567',4,'Group Emotional Regulation','+',NULL,0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (190,'{"en": "In this community we usually care about what other members are feeling", "es": "Como grupo nos preocuparnos por lo que sienten nuestros compañeros/as", "gr": "Στην κοινότητα μας, μας ενδιαφέρει πως νιώθουν τα υπόλοιπα μέλη"}','2020-09-25 11:51:43.255567',4,'Group Emotional Awareness','+',NULL,0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (191,'{"en": "We make each other feel better when we are down", "es": "Cuando nos sentimos tristes o deprimidos nos ayudamos para sentirnos mejor", "gr": "Σε αυτή την κοινότητα, όταν κάποιο μέλος δεν είναι καλά, προσπαθούμε να τον/την κάνουμε να αισθάνεται καλύτερα"}','2020-09-25 11:51:43.255567',4,'Group Emotional Regulation','+',NULL,0);
INSERT INTO "questions" ("id","text","created_at","survey_id","category","sign","criterion","optional") VALUES (192,'{"en": "We let members know that we value their contributions", "es": "En este grupo hacemos saber a los miembros que valoramos sus aportaciones", "gr": "Στην κοινότητα μας εκφράζουμε μεταξύ μας συχνά ότι εκτιμούμε τις συνεισφορές των υπολοίπων μελών"}','2020-09-25 11:51:43.255567',4,'Group Cohesion','+',NULL,0);
COMMIT;
