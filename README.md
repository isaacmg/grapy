<div align='center'>
    <h1><b> Grapy </b></h1>
    <p>Project to graph sociograms.</p>

![Python](https://badgen.net/badge/Python/[3.10.12]/blue?)
</div>

---

## 💾 **ABOUT**

The intention of this project is to create an open source Python application to be able to easily make sociograms. The application is in three languages: English, Català and Castellano.

<br />

---
##  **DOCUMENTATION**

 - [Wiki](https://gitlab.com/isaacmg/grapy/-/wikis/home)


## 🗒️ **INSTALLATION**

### previous

1. Create a virtual env

```
python3 -m venv <name of the repository>
```

2. Use virtual env

```
source <name of the repository>/bin/activate
```


### local installation:

1. clone the repo

```
git clone https://gitlab.com/isaacmg/grapy
```

2. cd into grapy

```
cd grapy
```

3. install dependencies

```
pip3 install -r requirements.txt
```
4. create the password file

```
echo 'difficultPassword' > password.txt
```

<br />


5. run the app

```
python3 src/main.py
```

<br />

## 🔎 **SHOWCASE**

<br />

## **TRANSLATIONS**

To create translations for the application, you need:

1. to create the translation file in your language.

To create the file, all you need to do:

Execute this:

```
pyside6-lupdate src/main.py src/GroupView.py src/MemberTestView.py src/AnswerView.py src/AnsweringView.py ui/answer.ui ui/answering.ui ui/groups.ui ui/groupAdm.ui ui/homepage.ui ui/mainwindow.ui ui/members.ui ui/participants.ui ui/results.ui -ts translations/<name_of_your_translation_file>.ts
```

Then, using QtLinguist, yo can translate all the Application.

then, do this:

```
pyside6-lrelease translations/<name_of_your_translation_file>.ts -qm translations/<name_of_your_translation_file>.qm
```

Tha name_of your_translation is important. Because it is the name that will appear in the menu to change the language.
You don't have to do anything else. When your file is in the translations folder, your translation will be available.


<br />

## **LICENSE**

Distributed under GPLv3.0 License. See [LICENSE.txt](https://gitlab.com/isaacmg/grapy/-/blob/main/LICENSE) for more information.

<br />
