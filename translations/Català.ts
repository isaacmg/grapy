<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ca_ES" sourcelanguage="en">
<context>
    <name>AnsweringView</name>
    <message>
        <location filename="../src/AnsweringView.py" line="67"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../src/AnsweringView.py" line="68"/>
        <source>You have to choose at least one person.</source>
        <translation>Has d&apos;escollir almenys una persona.</translation>
    </message>
    <message>
        <location filename="../src/AnsweringView.py" line="89"/>
        <source>Confirm Finish</source>
        <translation>Confirma l&apos;acabament</translation>
    </message>
    <message>
        <location filename="../src/AnsweringView.py" line="89"/>
        <source>Are you sure that you finish answering?</source>
        <translation>Estàs segur que has acabat de respondre?</translation>
    </message>
    <message>
        <location filename="../src/AnsweringView.py" line="200"/>
        <source>Finish &gt;</source>
        <translation>Acabar &gt;</translation>
    </message>
    <message>
        <location filename="../src/AnsweringView.py" line="202"/>
        <source>Next &gt;</source>
        <translation>Següent &gt;</translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="../ui/homepage.ui" line="14"/>
        <source>Form</source>
        <translation>Formulari</translation>
    </message>
    <message>
        <location filename="../ui/homepage.ui" line="22"/>
        <source>Create a new File or Open one using the menu</source>
        <translation>Crea un nou fitxer o obre un utilitzant el menu</translation>
    </message>
    <message>
        <location filename="../ui/homepage.ui" line="37"/>
        <source>Sociograms</source>
        <translation>Sociogrames</translation>
    </message>
</context>
<context>
    <name>GroupView</name>
    <message>
        <location filename="../src/GroupView.py" line="58"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../src/GroupView.py" line="59"/>
        <source>Organization</source>
        <translation>Organització</translation>
    </message>
    <message>
        <location filename="../src/GroupView.py" line="80"/>
        <source>No values to add</source>
        <translation>Sense valors a afegir</translation>
    </message>
    <message>
        <location filename="../src/GroupView.py" line="81"/>
        <source>The fields Name or Organization are not filled</source>
        <translation>Els camps Nom o Organització no estan omplerts</translation>
    </message>
    <message>
        <location filename="../src/GroupView.py" line="96"/>
        <source>Could not insert the new group</source>
        <translation>No s&apos;ha pogut insertar el nou grup</translation>
    </message>
    <message>
        <location filename="../src/GroupView.py" line="100"/>
        <source>Error Adding the new group</source>
        <translation>Error afegint el nou grup</translation>
    </message>
    <message>
        <location filename="../src/GroupView.py" line="101"/>
        <location filename="../src/GroupView.py" line="131"/>
        <source>Some fields are not correct. The error was: </source>
        <translation>Alguns camps no són correctes. L&apos;error ha sigut: </translation>
    </message>
    <message>
        <location filename="../src/GroupView.py" line="105"/>
        <source>Group inserted</source>
        <translation>Grup insertat</translation>
    </message>
    <message>
        <location filename="../src/GroupView.py" line="106"/>
        <source>The group data was inserted correctly.</source>
        <translation>Les dades del grup s&apos;han insertat correctament</translation>
    </message>
    <message>
        <location filename="../src/GroupView.py" line="110"/>
        <location filename="../src/GroupView.py" line="140"/>
        <source>No group selected</source>
        <translation>Cap grup seleccionat</translation>
    </message>
    <message>
        <location filename="../src/GroupView.py" line="111"/>
        <source>To update a group, you have to select one group.</source>
        <translation>Per modificar un grup has de seleccionar un grup.</translation>
    </message>
    <message>
        <location filename="../src/GroupView.py" line="126"/>
        <source>Could not update the new test</source>
        <translation>No s&apos;ha pogut modifiar el nou test</translation>
    </message>
    <message>
        <location filename="../src/GroupView.py" line="130"/>
        <source>Error Updating the new group</source>
        <translation>Error modificant el nou grup</translation>
    </message>
    <message>
        <location filename="../src/GroupView.py" line="135"/>
        <source>Group Updated</source>
        <translation>Grup modificat</translation>
    </message>
    <message>
        <location filename="../src/GroupView.py" line="136"/>
        <source>The group data was updated correctly.</source>
        <translation>Les dades del grup s&apos;han modificat correctament.</translation>
    </message>
    <message>
        <location filename="../src/GroupView.py" line="141"/>
        <source>To delete a group, you have to select one group.</source>
        <translation>Per borrar un grup, has de seleccionar un grup.</translation>
    </message>
    <message>
        <location filename="../src/GroupView.py" line="145"/>
        <source>Delete the current group?</source>
        <translation>Borra el següent grup?</translation>
    </message>
    <message>
        <location filename="../src/GroupView.py" line="146"/>
        <source>Are you sure?</source>
        <translation>Estàs segur?</translation>
    </message>
</context>
<context>
    <name>Groups</name>
    <message>
        <location filename="../ui/answer.ui" line="14"/>
        <location filename="../ui/answering.ui" line="14"/>
        <location filename="../ui/groups.ui" line="14"/>
        <location filename="../ui/groupAdm.ui" line="14"/>
        <location filename="../ui/members.ui" line="14"/>
        <location filename="../ui/participants.ui" line="14"/>
        <location filename="../ui/results.ui" line="14"/>
        <source>Form</source>
        <translation>Formulari</translation>
    </message>
    <message>
        <location filename="../ui/answer.ui" line="28"/>
        <source>Answer</source>
        <translation>Respondre</translation>
    </message>
    <message>
        <location filename="../ui/answer.ui" line="40"/>
        <source>Group Name: </source>
        <translation>Nom del Grup: </translation>
    </message>
    <message>
        <location filename="../ui/answer.ui" line="74"/>
        <source>Test Name: </source>
        <translation>Nom del Test: </translation>
    </message>
    <message>
        <location filename="../ui/answer.ui" line="123"/>
        <source>The next student to answer is: </source>
        <translation>El següent estudiant a contestar és: </translation>
    </message>
    <message>
        <location filename="../ui/answer.ui" line="150"/>
        <source>S&amp;tart Answering</source>
        <translation>&amp;Començar a respondre</translation>
    </message>
    <message>
        <location filename="../ui/answer.ui" line="170"/>
        <location filename="../ui/participants.ui" line="134"/>
        <source>&amp;Return to Tests</source>
        <translation>&amp;Tornar als Tests</translation>
    </message>
    <message>
        <location filename="../ui/answering.ui" line="28"/>
        <source>Survey</source>
        <translation>Enquesta</translation>
    </message>
    <message>
        <location filename="../ui/answering.ui" line="40"/>
        <source>Student Name: </source>
        <translation>Nom de l&apos;estudiant: </translation>
    </message>
    <message>
        <location filename="../ui/answering.ui" line="105"/>
        <source>Question:</source>
        <translation>Pregunta:</translation>
    </message>
    <message>
        <location filename="../ui/answering.ui" line="125"/>
        <source>/</source>
        <translation>/</translation>
    </message>
    <message>
        <location filename="../ui/answering.ui" line="146"/>
        <source>&lt; &amp;Back</source>
        <translation>&lt; &amp;Anterior</translation>
    </message>
    <message>
        <location filename="../ui/answering.ui" line="153"/>
        <source>&amp;Clear</source>
        <translation>&amp;Netejar</translation>
    </message>
    <message>
        <location filename="../ui/answering.ui" line="160"/>
        <source>&amp;Next &gt;</source>
        <translation>&amp;Següent &gt;</translation>
    </message>
    <message>
        <location filename="../ui/answering.ui" line="197"/>
        <source>&amp;Return to Test Buttons</source>
        <translation>T&amp;ornar als Tests</translation>
    </message>
    <message>
        <location filename="../ui/groups.ui" line="40"/>
        <source>Groups</source>
        <translation>Grups</translation>
    </message>
    <message>
        <location filename="../ui/groups.ui" line="59"/>
        <location filename="../ui/groupAdm.ui" line="98"/>
        <location filename="../ui/groupAdm.ui" line="283"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ui/groups.ui" line="76"/>
        <source>Organization</source>
        <translation>Organització</translation>
    </message>
    <message>
        <location filename="../ui/groups.ui" line="97"/>
        <location filename="../ui/groupAdm.ui" line="192"/>
        <location filename="../ui/groupAdm.ui" line="297"/>
        <source>&amp;Add</source>
        <translation>&amp;Afegir</translation>
    </message>
    <message>
        <location filename="../ui/groups.ui" line="104"/>
        <location filename="../ui/groupAdm.ui" line="304"/>
        <source>&amp;Update</source>
        <translation>&amp;Modificar</translation>
    </message>
    <message>
        <location filename="../ui/groups.ui" line="111"/>
        <location filename="../ui/groupAdm.ui" line="206"/>
        <location filename="../ui/groupAdm.ui" line="311"/>
        <source>&amp;Delete</source>
        <translation>&amp;Borrar</translation>
    </message>
    <message>
        <location filename="../ui/groups.ui" line="188"/>
        <source>&amp;Open Group</source>
        <translation>&amp;Obrir Grup</translation>
    </message>
    <message>
        <location filename="../ui/groupAdm.ui" line="28"/>
        <source>Group Management</source>
        <translation>Gestió del Grup</translation>
    </message>
    <message>
        <location filename="../ui/groupAdm.ui" line="40"/>
        <location filename="../ui/members.ui" line="40"/>
        <location filename="../ui/participants.ui" line="42"/>
        <location filename="../ui/results.ui" line="40"/>
        <source>Group Name:</source>
        <translation>Nom del Grup:</translation>
    </message>
    <message>
        <location filename="../ui/groupAdm.ui" line="83"/>
        <source>&amp;Members</source>
        <translation>&amp;Membres</translation>
    </message>
    <message>
        <location filename="../ui/groupAdm.ui" line="108"/>
        <location filename="../ui/members.ui" line="88"/>
        <source>Surname</source>
        <translation>Cognom</translation>
    </message>
    <message>
        <location filename="../ui/groupAdm.ui" line="118"/>
        <location filename="../ui/members.ui" line="98"/>
        <source>Sex</source>
        <translation>Gènere</translation>
    </message>
    <message>
        <location filename="../ui/groupAdm.ui" line="126"/>
        <source>Female</source>
        <translation>Dona</translation>
    </message>
    <message>
        <location filename="../ui/groupAdm.ui" line="131"/>
        <source>Male</source>
        <translation>Home</translation>
    </message>
    <message>
        <location filename="../ui/groupAdm.ui" line="136"/>
        <source>Other</source>
        <translation>Altre</translation>
    </message>
    <message>
        <location filename="../ui/groupAdm.ui" line="144"/>
        <location filename="../ui/members.ui" line="108"/>
        <source>Age</source>
        <translation>Edat</translation>
    </message>
    <message>
        <location filename="../ui/groupAdm.ui" line="151"/>
        <location filename="../ui/members.ui" line="115"/>
        <source>Active</source>
        <translation>Actiu</translation>
    </message>
    <message>
        <location filename="../ui/groupAdm.ui" line="158"/>
        <location filename="../ui/members.ui" line="122"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="../ui/groupAdm.ui" line="165"/>
        <location filename="../ui/members.ui" line="129"/>
        <source>Abbreviation</source>
        <translation>Abreviació</translation>
    </message>
    <message>
        <location filename="../ui/groupAdm.ui" line="199"/>
        <source>U&amp;pdate</source>
        <translation>M&amp;odificar</translation>
    </message>
    <message>
        <location filename="../ui/groupAdm.ui" line="241"/>
        <location filename="../ui/results.ui" line="282"/>
        <source>&amp;Return to Groups</source>
        <translation>T&amp;ornar als Grups</translation>
    </message>
    <message>
        <location filename="../ui/groupAdm.ui" line="261"/>
        <source>&amp;Upload CSV File</source>
        <translation>&amp;Pujar Fitxer CSV</translation>
    </message>
    <message>
        <location filename="../ui/groupAdm.ui" line="271"/>
        <source>&amp;Tests</source>
        <translation>&amp;Tests</translation>
    </message>
    <message>
        <location filename="../ui/groupAdm.ui" line="342"/>
        <source>A&amp;nswer</source>
        <translation>&amp;Respondre</translation>
    </message>
    <message>
        <location filename="../ui/groupAdm.ui" line="349"/>
        <source>&amp;Participants</source>
        <translation>&amp;Participants</translation>
    </message>
    <message>
        <location filename="../ui/groupAdm.ui" line="356"/>
        <source>&amp;Results</source>
        <translation>&amp;Resultats</translation>
    </message>
    <message>
        <location filename="../ui/members.ui" line="28"/>
        <source>Members</source>
        <translation>Membres</translation>
    </message>
    <message>
        <location filename="../ui/members.ui" line="78"/>
        <source>Nom</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../ui/members.ui" line="156"/>
        <source>Add</source>
        <translation>Afegir</translation>
    </message>
    <message>
        <location filename="../ui/members.ui" line="163"/>
        <source>Update</source>
        <translation>Modificar</translation>
    </message>
    <message>
        <location filename="../ui/members.ui" line="170"/>
        <source>Delete</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location filename="../ui/members.ui" line="205"/>
        <source>Return to Groups</source>
        <translation>Tornar a Grups</translation>
    </message>
    <message>
        <location filename="../ui/members.ui" line="225"/>
        <source>Upload CSV File</source>
        <translation>Pujar Fitxer CSV</translation>
    </message>
    <message>
        <location filename="../ui/participants.ui" line="30"/>
        <source>Participants</source>
        <translation>&amp;Participants</translation>
    </message>
    <message>
        <location filename="../ui/participants.ui" line="69"/>
        <source>Test name:</source>
        <translation>Nom del Test: </translation>
    </message>
    <message>
        <location filename="../ui/participants.ui" line="103"/>
        <source>Who has answered</source>
        <translation>Qui ha respost</translation>
    </message>
    <message>
        <location filename="../ui/participants.ui" line="121"/>
        <source>Who has to answer</source>
        <translation>Qui no ha respost</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="28"/>
        <source>Results</source>
        <translation>&amp;Resultats</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="67"/>
        <source>Test:</source>
        <translation>Test:</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="90"/>
        <source>Group Graph</source>
        <translation>Graf del Grup</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="105"/>
        <source>Socio Type</source>
        <translation>Tipus de Sociograma</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="114"/>
        <source>&amp;Preferences</source>
        <translation>&amp;Preferències</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="121"/>
        <source>P&amp;erceptionPreferences</source>
        <translation>P&amp;ercepció de les Preferències</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="128"/>
        <source>Per&amp;ceptionRejection</source>
        <translation>Per&amp;cepció de rebuig</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="135"/>
        <source>Re&amp;jection</source>
        <translation>Re&amp;buig</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="144"/>
        <source>Color</source>
        <translation>Color</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="153"/>
        <source>&amp;Degree</source>
        <translation>&amp;Grau</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="160"/>
        <source>&amp;Betweeness</source>
        <translation>&amp;Intermediació</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="167"/>
        <source>C&amp;loseness</source>
        <translation>Pr&amp;oximitat</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="174"/>
        <source>&amp;Gender</source>
        <translation>&amp;Gènere</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="183"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="192"/>
        <source>D&amp;ownload Graph</source>
        <translation>&amp;Descarregar Graf</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="199"/>
        <source>Autocenter</source>
        <translation>Autocentrar</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="209"/>
        <source>Force: </source>
        <translation>Força: </translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="231"/>
        <source>+ relax</source>
        <translation>+ relax</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="251"/>
        <source>+ forces</source>
        <translation>+ força</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="308"/>
        <source>Group Indexes</source>
        <translation>Índex del Grup</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="324"/>
        <source>Group Sociometric Indexes</source>
        <translation>Índex Sociomètrics del Grup</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="350"/>
        <source>Values</source>
        <translation>Valors</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="361"/>
        <source>Association Index</source>
        <translation>Índex d&apos;Associació</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="392"/>
        <source>Dissociation Index</source>
        <translation>Índex de Dissociació</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="423"/>
        <source>Social Cohesion</source>
        <translation>Cohesió Social</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="454"/>
        <source>Social Intensity</source>
        <translation>Intensitat Social</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="499"/>
        <source>Individual Graph</source>
        <translation>Graf Individuals</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="512"/>
        <source>Select a member</source>
        <translation>Selecciona un Membre</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="530"/>
        <source>Individual Indexes</source>
        <translation>Índex Individuals</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="540"/>
        <source>Select a Member:</source>
        <translation>Selecciona un Membre:</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="556"/>
        <source>Direct Indexes</source>
        <translation>Índex Directes</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="571"/>
        <location filename="../ui/results.ui" line="1072"/>
        <source>Index</source>
        <translation>Índex</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="597"/>
        <location filename="../ui/results.ui" line="1098"/>
        <source>Value</source>
        <translation>Valor</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="608"/>
        <source>Election Status</source>
        <translation>Estat de les eleccions</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="639"/>
        <source>Perception of election status</source>
        <translation>Percepció de l&apos;estat d&apos;elecció</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="670"/>
        <source>Rejection status</source>
        <translation>Estat dels rebuigs</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="701"/>
        <source>Perception of rejection status</source>
        <translation>Estat de les percepcions de rebuig</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="732"/>
        <source>Reciprocal elections</source>
        <translation>Eleccions Recipròques</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="763"/>
        <source>Reciprocal rejections</source>
        <translation>Rebuigs Recipròcs</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="794"/>
        <source>Feeling opposition</source>
        <translation>Sentiment d&apos;oposició</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="825"/>
        <source>Positive expansion</source>
        <translation>Expansió Positiva</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="856"/>
        <source>Negative expansion</source>
        <translation>Expansió Negativa</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="887"/>
        <source>Guessed right elections perception</source>
        <translation>Percepció acertada d&apos;eleccions</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="918"/>
        <source>Guessed right rejections perception</source>
        <translation>Percepció de rebuigs aceptats</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="949"/>
        <source>Dyadic overestimation</source>
        <translation>Sobreestimació Diàdica</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="980"/>
        <source>Dyadic underestimation</source>
        <translation>Subestimació Diàdica</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="1011"/>
        <source>Dyadic discrepancy</source>
        <translation>Discrepancia Diàdica</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="1057"/>
        <source>Compound Indexes</source>
        <translation>Índex Compostos</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="1109"/>
        <source>Popularity coefficient</source>
        <translation>Coeficient de Popularitat</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="1140"/>
        <source>Antipathy coefficient</source>
        <translation>Coeficient de Antipatia</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="1171"/>
        <source>Affective connection</source>
        <translation>Connexió Afectiva</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="1202"/>
        <source>Sociometric status</source>
        <translation>Estat Sociomètric</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="1233"/>
        <source>Positive expansion coefficient</source>
        <translation>Coeficient d&apos;expansió Positiva</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="1264"/>
        <source>Negative expansion coefficient</source>
        <translation>Coeficient d&apos;expansió Negativa</translation>
    </message>
    <message>
        <location filename="../ui/results.ui" line="1295"/>
        <source>Realistic perception</source>
        <translation>Percepció Realista</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/main.py" line="89"/>
        <source>Ready</source>
        <translation>Preparat</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="119"/>
        <source>File was modified</source>
        <translation>El Fitxer s&apos;ha modificat</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="123"/>
        <location filename="../src/main.py" line="147"/>
        <location filename="../src/main.py" line="160"/>
        <source>No test selected</source>
        <translation>No hi ha cap Test seleccionat</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="124"/>
        <location filename="../src/main.py" line="148"/>
        <location filename="../src/main.py" line="161"/>
        <source>To answer a test, you have to select one test.</source>
        <translation>Per respondre un test, has de seleccionar un test</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="173"/>
        <source>No more participants</source>
        <translation>No hi han més participants</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="174"/>
        <source>All the participans have answered the survey.</source>
        <translation>Tot els participants han respost l&apos;enquesta.</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="181"/>
        <source>No group selected</source>
        <translation>No hi han grups seleccionats</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="182"/>
        <source>To open a group, you have to select one group.</source>
        <translation>Per obrir un grup, has de seleccionar un grup.</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="196"/>
        <location filename="../src/main.py" line="209"/>
        <location filename="../src/main.py" line="239"/>
        <source>Save tha actual file?</source>
        <translation>Guardar el fitxer actual?</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="197"/>
        <location filename="../src/main.py" line="210"/>
        <location filename="../src/main.py" line="240"/>
        <source>The actual file is modified. Do you want to save it?</source>
        <translation>El fitxer actual ha sigut modificat. Vols guargar-lo?</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="223"/>
        <source>Unnamed</source>
        <translation>Sense nom</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="224"/>
        <source>New File</source>
        <translation>Fitxer Nou</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="233"/>
        <source>Select File to Open</source>
        <translation>Selecciona el Fitxer per Obrir-lo</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="233"/>
        <source>Socio Graph(*.sog );;All Files (*.*)</source>
        <translation>Graf Socio(*.sog);; Tots els fitxers (*.*)</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="258"/>
        <source>File Opened</source>
        <translation>Fitxer Obert</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="261"/>
        <source>Incorrect File</source>
        <translation>Fitxer Incorrecte</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="277"/>
        <source>Save file</source>
        <translation>Guardar el Fitxer</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="277"/>
        <source>Grapy Documents (*.sog);;All files (*.*)</source>
        <translation>Documents Grapy (*.sog);;Tots els fitxers (*.*)</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="298"/>
        <source>Saved</source>
        <translation>Guardat</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="314"/>
        <source>Untitled</source>
        <translation>Sense Nom</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="340"/>
        <location filename="../ui/mainwindow.ui" line="54"/>
        <source>&amp;New</source>
        <translation>&amp;Nou</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="341"/>
        <source>Create a new file</source>
        <translation>Crea un nou fitxer</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="344"/>
        <source>&amp;Open...</source>
        <translation>&amp;Obrir</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="345"/>
        <source>Open an existing file</source>
        <translation>Obrir un fitxer existent</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="349"/>
        <source>&amp;Save</source>
        <translation>&amp;Guardar</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="351"/>
        <source>Save the document to disk</source>
        <translation>Guardar el document al disc</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="353"/>
        <source>Save &amp;As...</source>
        <translation>Guardar &amp;Com...</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="355"/>
        <source>Save the document under a new name</source>
        <translation>Guardar el fitxer sota un nou nom</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="358"/>
        <source>E&amp;xit</source>
        <translation>S&amp;ortir</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="359"/>
        <source>Exit the application</source>
        <translation>Sortir de l&apos;aplicació</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="362"/>
        <source>&amp;About</source>
        <translation>%Sobre</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="363"/>
        <source>Show the application&apos;s About box</source>
        <translation>Ensenya la caixa d&apos;amb la informació de l&apos;aplicació</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="366"/>
        <source>About &amp;Qt</source>
        <translation>Sobre &amp;Qt</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="367"/>
        <source>Show the Qt library&apos;s About box</source>
        <translation>Ensenya la caixa amb informació sobre la llibreria Qt</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="369"/>
        <source>&amp;Español</source>
        <translation>&amp;Español</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="370"/>
        <source>Cambia el idioma a español</source>
        <translation>Canvia l&apos;idioma a espanyol</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="372"/>
        <source>&amp;English</source>
        <translation>E&amp;nglish</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="373"/>
        <source>Change the language to English</source>
        <translation>Canvia l&apos;idioma a anglès</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="377"/>
        <location filename="../ui/mainwindow.ui" line="42"/>
        <source>&amp;File</source>
        <translation>&amp;Fitxer</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="387"/>
        <source>&amp;Settings</source>
        <translation>&amp;Configurar</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="388"/>
        <source>&amp;Languages</source>
        <translation>&amp;Idiomes</translation>
    </message>
    <message>
        <location filename="../src/main.py" line="394"/>
        <source>&amp;Help</source>
        <translation>&amp;Ajuda</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>MainWindow</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="27"/>
        <source>PushButton</source>
        <translation>PushButton</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="57"/>
        <source>Create a new File</source>
        <translation>Crea un nou fitxer</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="60"/>
        <source>Ctrl+N</source>
        <translation>Ctrl + N</translation>
    </message>
</context>
<context>
    <name>MemberTestView</name>
    <message>
        <location filename="../src/MemberTestView.py" line="82"/>
        <location filename="../src/MemberTestView.py" line="109"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="83"/>
        <source>Surname</source>
        <translation>Cognom</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="84"/>
        <source>Genre</source>
        <translation>Gènere</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="85"/>
        <source>Age</source>
        <translation>Edat</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="86"/>
        <source>Active</source>
        <translation>Actiu</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="87"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="88"/>
        <source>Abbreviation</source>
        <translation>Abreviació</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="106"/>
        <source>Created_at</source>
        <translation>Creat a</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="141"/>
        <source>Open profile</source>
        <translation>Obrir Perfil</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="170"/>
        <source>One row was not inserted</source>
        <translation>Una fila no ha sigut insertada</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="173"/>
        <source>Error reading CSV File</source>
        <translation>Error llegint el fitxer CSV</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="174"/>
        <source>The CSV File has not the correct format. Try to see the example in this program. The error was: </source>
        <translation>El fitxer CSV no té el format correcte. Intenta veure l&apos;exemple al programa. L&apos;error ha sigut: </translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="176"/>
        <source>CSV File correctly inserted</source>
        <translation>Fitxer CSV insertat correctament</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="177"/>
        <source>The CSV File has correct format.</source>
        <translation>El fitxer CSV té el format correcte.</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="184"/>
        <location filename="../src/MemberTestView.py" line="221"/>
        <source>No values to add</source>
        <translation>No hi han valors a afegir</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="185"/>
        <source>The field Name is not filled</source>
        <translation>El camp Nom no s&apos;ha omplert</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="199"/>
        <location filename="../src/MemberTestView.py" line="247"/>
        <source>Could not insert the new member</source>
        <translation>No s&apos;ha pogut insertr el nou membre</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="202"/>
        <source>Test inserted</source>
        <translation>Test insertat</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="203"/>
        <location filename="../src/MemberTestView.py" line="284"/>
        <source>The test data was inserted correctly.</source>
        <translation>Les dades del test s&apos;han insertat correctament</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="208"/>
        <source>Adding participants</source>
        <translation>Afegint Participants</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="209"/>
        <source>Adding the active members of this group as a participants in this new test. Later, you can not add new participants. To add a new participant, please create a new test. </source>
        <translation>Afegint els membres actius d&apos;aquest grup com a participants en el nou test. Després, no podràs afegir nous participants. Per afegirt un nou participant, per favor crea un nou test. </translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="215"/>
        <location filename="../src/MemberTestView.py" line="278"/>
        <source>Error Adding the new test</source>
        <translation>Error afegint un nou test</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="216"/>
        <location filename="../src/MemberTestView.py" line="251"/>
        <location filename="../src/MemberTestView.py" line="279"/>
        <location filename="../src/MemberTestView.py" line="318"/>
        <source>Some fields are not correct. The error was: </source>
        <translation>Alguns camps no són correctes. L&apos;error ha sigut: </translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="222"/>
        <source>The fields Name or Organization or Email or Abbreviation are not filled or Age is negative</source>
        <translation>Els camps Nom o Organització o Mail o Abreviació  no estan omplerts o l&apos;Edat és negativa</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="250"/>
        <location filename="../src/MemberTestView.py" line="317"/>
        <source>Error Adding the new member</source>
        <translation>Error afegint el nou membre</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="255"/>
        <location filename="../src/MemberTestView.py" line="322"/>
        <source>Member inserted</source>
        <translation>Membre insertat</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="256"/>
        <location filename="../src/MemberTestView.py" line="323"/>
        <source>The member data was inserted correctly.</source>
        <translation>Les dades del membre han estat insertades correctament.</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="260"/>
        <location filename="../src/MemberTestView.py" line="328"/>
        <source>No test selected</source>
        <translation>No hi ha cap test seleccionat</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="261"/>
        <source>To update a test, you have to select one test.</source>
        <translation>Per modificar un test, has de seleccionar un test</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="274"/>
        <source>Could not update the new test</source>
        <translation>No s&apos;ha pogut modificar el nou test</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="283"/>
        <source>Test Updated</source>
        <translation>Test modificat</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="288"/>
        <location filename="../src/MemberTestView.py" line="342"/>
        <source>No member selected</source>
        <translation>Cap membre seleccionat</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="289"/>
        <source>To update a member, you have to select one member.</source>
        <translation>Per modificar un membre has de seleccionar un membre.</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="313"/>
        <source>Could not update the new member</source>
        <translation>No s&apos;ha pogut insertar el nou membre</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="329"/>
        <source>To delete a test, you have to select one test.</source>
        <translation>Per borrar un test, has de seleccionar un test</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="333"/>
        <location filename="../src/MemberTestView.py" line="347"/>
        <source>Delete the current line?</source>
        <translation>Borra la següent línia?</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="334"/>
        <location filename="../src/MemberTestView.py" line="348"/>
        <source>Are you sure?</source>
        <translation>Estàs segur?</translation>
    </message>
    <message>
        <location filename="../src/MemberTestView.py" line="343"/>
        <source>To delete a member, you have to select one member.</source>
        <translation>Per borrar un membre has de seleccionar un membre.</translation>
    </message>
</context>
</TS>
