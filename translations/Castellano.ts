<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>AnswerView</name>
    <message>
        <location filename="src/AnswerView.py" line="51"/>
        <source>Starting Answering</source>
        <translation>Empiece a Responder</translation>
    </message>
    <message>
        <location filename="src/AnswerView.py" line="52"/>
        <source>You will start to answer.</source>
        <translation>Va a empezar a resopnder</translation>
    </message>
</context>
<context>
    <name>AnsweringView</name>
    <message>
        <location filename="src/AnsweringView.py" line="65"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="src/AnsweringView.py" line="66"/>
        <source>You have to choose at least one person.</source>
        <translation>Debe elegir al menos una persona.</translation>
    </message>
    <message>
        <location filename="src/AnsweringView.py" line="87"/>
        <source>Confirm Finish</source>
        <translation>Confirme Salida</translation>
    </message>
    <message>
        <location filename="src/AnsweringView.py" line="87"/>
        <source>Are you sure that you finish answering?</source>
        <translation>Está seguro que ha acabado de responder?</translation>
    </message>
    <message>
        <location filename="src/AnsweringView.py" line="184"/>
        <source>Finish &gt;</source>
        <translation>Acabar &gt;</translation>
    </message>
    <message>
        <location filename="src/AnsweringView.py" line="186"/>
        <source>Next &gt;</source>
        <translation>Siguiente &gt;</translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="ui/homepage.ui" line="14"/>
        <source>Form</source>
        <translation>Formulario</translation>
    </message>
    <message>
        <location filename="ui/homepage.ui" line="22"/>
        <source>Create a new File or Open one using the menu</source>
        <translation>Crea un nuevo fichero o abre uno usando el menú</translation>
    </message>
    <message>
        <location filename="ui/homepage.ui" line="37"/>
        <source>Sociograms</source>
        <translation>Sociogramas</translation>
    </message>
</context>
<context>
    <name>GroupModel</name>
    <message>
        <source>Name</source>
        <oldsource>name</oldsource>
        <translation type="vanished">Nombre</translation>
    </message>
    <message>
        <source>Organization</source>
        <oldsource>organization</oldsource>
        <translation type="vanished">Organización</translation>
    </message>
</context>
<context>
    <name>GroupView</name>
    <message>
        <location filename="src/GroupView.py" line="58"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="src/GroupView.py" line="59"/>
        <source>Organization</source>
        <translation>Organización</translation>
    </message>
    <message>
        <location filename="src/GroupView.py" line="80"/>
        <source>No values to add</source>
        <translation>Sin valores para añadir</translation>
    </message>
    <message>
        <location filename="src/GroupView.py" line="81"/>
        <source>The fields Name or Organization are not filled</source>
        <translation>Los campos Nombre o Organización no estan llenos</translation>
    </message>
    <message>
        <location filename="src/GroupView.py" line="96"/>
        <source>Could not insert the new group</source>
        <translation>No se ha podido insertar el nuevo grupo</translation>
    </message>
    <message>
        <location filename="src/GroupView.py" line="100"/>
        <source>Error Adding the new group</source>
        <translation>Error añadiendo el nuevo grupo</translation>
    </message>
    <message>
        <location filename="src/GroupView.py" line="101"/>
        <location filename="src/GroupView.py" line="131"/>
        <source>Some fields are not correct. The error was: </source>
        <translation>Algunos campos son incorrectos. El error fue: </translation>
    </message>
    <message>
        <location filename="src/GroupView.py" line="105"/>
        <source>Group inserted</source>
        <translation>Grupo insertado</translation>
    </message>
    <message>
        <location filename="src/GroupView.py" line="106"/>
        <source>The group data was inserted correctly.</source>
        <translation>Los datos del grupo fueron insertados correctamente.</translation>
    </message>
    <message>
        <location filename="src/GroupView.py" line="110"/>
        <location filename="src/GroupView.py" line="140"/>
        <source>No group selected</source>
        <translation>Ningún grupo seleccionado</translation>
    </message>
    <message>
        <location filename="src/GroupView.py" line="111"/>
        <source>To update a group, you have to select one group.</source>
        <translation>Para modificar un grupo, debes seleccionar un grupo.</translation>
    </message>
    <message>
        <location filename="src/GroupView.py" line="126"/>
        <source>Could not update the new test</source>
        <translation>No se ha podido modificar el nuevo test</translation>
    </message>
    <message>
        <location filename="src/GroupView.py" line="130"/>
        <source>Error Updating the new group</source>
        <translation>Error modificando el nuevo grup</translation>
    </message>
    <message>
        <location filename="src/GroupView.py" line="135"/>
        <source>Group Updated</source>
        <translation>Grupo modificado</translation>
    </message>
    <message>
        <location filename="src/GroupView.py" line="136"/>
        <source>The group data was updated correctly.</source>
        <translation>Los datos del grupo han sido modificados correctamente</translation>
    </message>
    <message>
        <location filename="src/GroupView.py" line="141"/>
        <source>To delete a group, you have to select one group.</source>
        <translation>Para borrar un grupo, debes seleccionar un grupo.</translation>
    </message>
    <message>
        <location filename="src/GroupView.py" line="145"/>
        <source>Delete the current group?</source>
        <translation>Borrar el grupo actual?</translation>
    </message>
    <message>
        <location filename="src/GroupView.py" line="146"/>
        <source>Are you sure?</source>
        <translation>Está seguro?</translation>
    </message>
</context>
<context>
    <name>Groups</name>
    <message>
        <location filename="ui/answer.ui" line="14"/>
        <location filename="ui/answering.ui" line="14"/>
        <location filename="ui/groups.ui" line="14"/>
        <location filename="ui/groupAdm.ui" line="14"/>
        <location filename="ui/members.ui" line="14"/>
        <location filename="ui/participants.ui" line="14"/>
        <location filename="ui/results.ui" line="14"/>
        <source>Form</source>
        <translation>Formulario</translation>
    </message>
    <message>
        <location filename="ui/answer.ui" line="28"/>
        <location filename="ui/groupAdm.ui" line="342"/>
        <source>Answer</source>
        <translation>Contestar</translation>
    </message>
    <message>
        <location filename="ui/groupAdm.ui" line="40"/>
        <location filename="ui/members.ui" line="40"/>
        <location filename="ui/participants.ui" line="40"/>
        <location filename="ui/results.ui" line="40"/>
        <source>Group Name:</source>
        <translation>Nombre del Grupo:</translation>
    </message>
    <message>
        <location filename="ui/participants.ui" line="67"/>
        <source>Test Name:</source>
        <translation>Nombre del Test:</translation>
    </message>
    <message>
        <location filename="ui/answer.ui" line="40"/>
        <source>Group Name: </source>
        <translation>Nombre del Grupo:</translation>
    </message>
    <message>
        <location filename="ui/answer.ui" line="74"/>
        <source>Test Name: </source>
        <translation>Nombre del Test:</translation>
    </message>
    <message>
        <location filename="ui/answer.ui" line="121"/>
        <source>The next student to answer is: </source>
        <translation>El próximo estudiante a responder es:</translation>
    </message>
    <message>
        <location filename="ui/answer.ui" line="144"/>
        <source>Start Answering</source>
        <translation>Empiece a contestar</translation>
    </message>
    <message>
        <location filename="ui/answer.ui" line="151"/>
        <location filename="ui/participants.ui" line="125"/>
        <source>Return to Tests</source>
        <translation>Volver a los Tests</translation>
    </message>
    <message>
        <location filename="ui/answering.ui" line="28"/>
        <source>Survey</source>
        <translation>Encuesta</translation>
    </message>
    <message>
        <location filename="ui/answering.ui" line="40"/>
        <source>Student Name:  </source>
        <oldsource>Student Name: </oldsource>
        <translation>Nombre del Alumno:</translation>
    </message>
    <message>
        <location filename="ui/answering.ui" line="98"/>
        <source>Question:</source>
        <translation>Pregunta:</translation>
    </message>
    <message>
        <location filename="ui/answering.ui" line="118"/>
        <source>/</source>
        <translation>/</translation>
    </message>
    <message>
        <location filename="ui/answering.ui" line="139"/>
        <source>&lt; Back</source>
        <translation>&lt; Volver</translation>
    </message>
    <message>
        <location filename="ui/answering.ui" line="146"/>
        <source>Clear</source>
        <translation>Limpiar</translation>
    </message>
    <message>
        <location filename="ui/answering.ui" line="153"/>
        <source>Next &gt;</source>
        <translation>Siguiente &gt;</translation>
    </message>
    <message>
        <location filename="ui/answering.ui" line="177"/>
        <source>Return to Test Buttons</source>
        <translation>Volver a Tests</translation>
    </message>
    <message>
        <location filename="ui/groups.ui" line="40"/>
        <source>Groups</source>
        <translation>Grupos</translation>
    </message>
    <message>
        <location filename="ui/members.ui" line="78"/>
        <source>Nom</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="ui/groups.ui" line="76"/>
        <source>Organization</source>
        <oldsource>Organitzacio</oldsource>
        <translation>Organización</translation>
    </message>
    <message>
        <location filename="ui/groups.ui" line="97"/>
        <location filename="ui/groupAdm.ui" line="192"/>
        <location filename="ui/groupAdm.ui" line="297"/>
        <location filename="ui/members.ui" line="156"/>
        <source>Add</source>
        <translation>Añadir</translation>
    </message>
    <message>
        <location filename="ui/groups.ui" line="104"/>
        <location filename="ui/groupAdm.ui" line="199"/>
        <location filename="ui/groupAdm.ui" line="304"/>
        <location filename="ui/members.ui" line="163"/>
        <source>Update</source>
        <translation>Modificar</translation>
    </message>
    <message>
        <location filename="ui/groups.ui" line="111"/>
        <location filename="ui/groupAdm.ui" line="206"/>
        <location filename="ui/groupAdm.ui" line="311"/>
        <location filename="ui/members.ui" line="170"/>
        <source>Delete</source>
        <translation>Borrar</translation>
    </message>
    <message>
        <location filename="ui/groups.ui" line="188"/>
        <source>Open Group</source>
        <translation>Abrir Grupo</translation>
    </message>
    <message>
        <location filename="ui/groupAdm.ui" line="28"/>
        <source>Group Management</source>
        <translation>Gestión de Grupo</translation>
    </message>
    <message>
        <location filename="ui/groupAdm.ui" line="83"/>
        <location filename="ui/members.ui" line="28"/>
        <source>Members</source>
        <translation>Miembros</translation>
    </message>
    <message>
        <location filename="ui/groups.ui" line="59"/>
        <location filename="ui/groupAdm.ui" line="98"/>
        <location filename="ui/groupAdm.ui" line="283"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="ui/groupAdm.ui" line="108"/>
        <location filename="ui/members.ui" line="88"/>
        <source>Surname</source>
        <translation>Apellido</translation>
    </message>
    <message>
        <location filename="ui/groupAdm.ui" line="118"/>
        <location filename="ui/members.ui" line="98"/>
        <source>Sex</source>
        <translation>Género</translation>
    </message>
    <message>
        <location filename="ui/groupAdm.ui" line="126"/>
        <source>Female</source>
        <translation>Mujer</translation>
    </message>
    <message>
        <location filename="ui/groupAdm.ui" line="131"/>
        <source>Male</source>
        <translation>Hombre</translation>
    </message>
    <message>
        <location filename="ui/groupAdm.ui" line="136"/>
        <source>Other</source>
        <translation>Otro</translation>
    </message>
    <message>
        <location filename="ui/groupAdm.ui" line="144"/>
        <location filename="ui/members.ui" line="108"/>
        <source>Age</source>
        <translation>Edad</translation>
    </message>
    <message>
        <location filename="ui/groupAdm.ui" line="151"/>
        <location filename="ui/members.ui" line="115"/>
        <source>Active</source>
        <translation>Activo</translation>
    </message>
    <message>
        <location filename="ui/groupAdm.ui" line="158"/>
        <location filename="ui/members.ui" line="122"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="ui/groupAdm.ui" line="165"/>
        <location filename="ui/members.ui" line="129"/>
        <source>Abbreviation</source>
        <translation>Abreviación</translation>
    </message>
    <message>
        <location filename="ui/groupAdm.ui" line="241"/>
        <location filename="ui/members.ui" line="205"/>
        <location filename="ui/results.ui" line="282"/>
        <source>Return to Groups</source>
        <translation>Volver a Grupos</translation>
    </message>
    <message>
        <location filename="ui/groupAdm.ui" line="261"/>
        <location filename="ui/members.ui" line="225"/>
        <source>Upload CSV File</source>
        <translation>Subir un fichero CSV</translation>
    </message>
    <message>
        <location filename="ui/groupAdm.ui" line="271"/>
        <source>Tests</source>
        <translation>Tests</translation>
    </message>
    <message>
        <location filename="ui/groupAdm.ui" line="349"/>
        <location filename="ui/participants.ui" line="28"/>
        <source>Participants</source>
        <translation>Participantes</translation>
    </message>
    <message>
        <location filename="ui/groupAdm.ui" line="356"/>
        <location filename="ui/results.ui" line="28"/>
        <source>Results</source>
        <translation>Resultados</translation>
    </message>
    <message>
        <location filename="ui/participants.ui" line="94"/>
        <source>Who has answered</source>
        <translation>Quien ha respuesto</translation>
    </message>
    <message>
        <location filename="ui/participants.ui" line="112"/>
        <source>Who has to answer</source>
        <translation>Quien debe responder</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="67"/>
        <source>Test:</source>
        <oldsource>Test: </oldsource>
        <translation>Test:</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="90"/>
        <source>Group Graph</source>
        <translation>Grafo del Grupo</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="105"/>
        <source>Socio Type</source>
        <translation>Tipo de Socio</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="114"/>
        <source>Preferences</source>
        <translation>Preferencias</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="121"/>
        <source>PerceptionPreferences</source>
        <translation>Percepción de preferencias</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="128"/>
        <source>PerceptionRejection</source>
        <translation>Percepción de rechazos</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="135"/>
        <source>Rejection</source>
        <translation>Rechazos</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="144"/>
        <source>Color</source>
        <translation>Color</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="153"/>
        <source>Degree</source>
        <translation>Grado</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="160"/>
        <source>Betweeness</source>
        <translation>Intermediación</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="167"/>
        <source>Closeness</source>
        <translation>Cercanía</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="174"/>
        <source>Gender</source>
        <translation>Género</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="183"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="192"/>
        <source>Download Graph</source>
        <translation>Descargar el Grafo</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="199"/>
        <source>Autocenter</source>
        <translation>Autocentrar</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="209"/>
        <source>Force: </source>
        <translation>Fuerza</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="231"/>
        <source>+ relax</source>
        <translation>+ relax</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="251"/>
        <source>+ forces</source>
        <translation>+ fuerza</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="308"/>
        <source>Group Indexes</source>
        <translation>Índices de Grupo</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="324"/>
        <source>Group Sociometric Indexes</source>
        <translation>Índices Sociométricos de Grupo</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="350"/>
        <source>Values</source>
        <translation>Valores</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="361"/>
        <source>Association Index</source>
        <translation>Índice de asociación</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="392"/>
        <source>Dissociation Index</source>
        <translation>Índice de disociación</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="423"/>
        <source>Social Cohesion</source>
        <translation>Cohesión Social</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="454"/>
        <source>Social Intensity</source>
        <translation>Intensidad Social</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="499"/>
        <source>Individual Graph</source>
        <translation>Grafo Individual</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="512"/>
        <source>Select a member</source>
        <translation>Seleccione un miembro</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="530"/>
        <source>Individual Indexes</source>
        <translation>Índices Individuales</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="540"/>
        <source>Select a Member:</source>
        <translation>Seleccione un miembro</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="556"/>
        <source>Direct Indexes</source>
        <translation>índices Directos</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="571"/>
        <location filename="ui/results.ui" line="1072"/>
        <source>Index</source>
        <translation>Índice</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="597"/>
        <location filename="ui/results.ui" line="1098"/>
        <source>Value</source>
        <translation>Valor</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="608"/>
        <source>Election Status</source>
        <translation>Estado de las elecciones</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="639"/>
        <source>Perception of election status</source>
        <translation>Percepción del estado de las elecciones</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="670"/>
        <source>Rejection status</source>
        <translation>Estado de Rechazo</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="701"/>
        <source>Perception of rejection status</source>
        <translation>Percepción del estado e Rechazo</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="732"/>
        <source>Reciprocal elections</source>
        <translation>Elecciones recíprocas</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="763"/>
        <source>Reciprocal rejections</source>
        <translation>Rechazos recíprocos</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="794"/>
        <source>Feeling opposition</source>
        <translation>Sentimiento de Oposición</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="825"/>
        <source>Positive expansion</source>
        <translation>Expansión Positiva</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="856"/>
        <source>Negative expansion</source>
        <translation>Expansión Negativa</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="887"/>
        <source>Guessed right elections perception</source>
        <translation>Percepción acertada de las elecciones</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="918"/>
        <source>Guessed right rejections perception</source>
        <translation>Percepción de rechazos aceptados</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="949"/>
        <source>Dyadic overestimation</source>
        <translation>Sobreestimación diádica</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="980"/>
        <source>Dyadic underestimation</source>
        <translation>Subestimación diádica</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="1011"/>
        <source>Dyadic discrepancy</source>
        <translation>Discrepancia diádica</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="1057"/>
        <source>Compound Indexes</source>
        <translation>Índices Compuestos</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="1109"/>
        <source>Popularity coefficient</source>
        <translation>Coeficiente de Popularidad</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="1140"/>
        <source>Antipathy coefficient</source>
        <translation>Coeficiente de Antipatía</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="1171"/>
        <source>Affective connection</source>
        <translation>Conexión Afectiva</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="1202"/>
        <source>Sociometric status</source>
        <translation>Estado Sociométrico</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="1233"/>
        <source>Positive expansion coefficient</source>
        <translation>Coeficiente de expansión Positivo</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="1264"/>
        <source>Negative expansion coefficient</source>
        <translation>Coeficiente de expansión Negativo</translation>
    </message>
    <message>
        <location filename="ui/results.ui" line="1295"/>
        <source>Realistic perception</source>
        <translation>Percepción Realista</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="ui/mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation>Ventana Principal</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="27"/>
        <source>PushButton</source>
        <translation>Botón</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="57"/>
        <source>Create a new File</source>
        <translation>Crear un fichero nuevo</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="60"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="src/main.py" line="82"/>
        <source>Ready</source>
        <translation>Listo</translation>
    </message>
    <message>
        <location filename="src/main.py" line="102"/>
        <source>File was modified</source>
        <translation>Fichero modificado</translation>
    </message>
    <message>
        <location filename="src/main.py" line="106"/>
        <location filename="src/main.py" line="125"/>
        <location filename="src/main.py" line="137"/>
        <source>No test selected</source>
        <translation>Ningún test seleccionado</translation>
    </message>
    <message>
        <location filename="src/main.py" line="107"/>
        <location filename="src/main.py" line="126"/>
        <location filename="src/main.py" line="138"/>
        <source>To answer a test, you have to select one test.</source>
        <translation>Para contestar a un test, debes seleccionar un test.</translation>
    </message>
    <message>
        <location filename="src/main.py" line="149"/>
        <source>No more participants</source>
        <translation>No hay más participantes</translation>
    </message>
    <message>
        <location filename="src/main.py" line="150"/>
        <source>All the participans have answered the survey.</source>
        <translation>Todos los participantes han contestado la encuesta.</translation>
    </message>
    <message>
        <location filename="src/main.py" line="157"/>
        <source>No group selected</source>
        <translation>Ningún grupo seleccionado</translation>
    </message>
    <message>
        <location filename="src/main.py" line="158"/>
        <source>To open a group, you have to select one group.</source>
        <translation>Para abrir un grupo, debes seleccionar un grupo.</translation>
    </message>
    <message>
        <location filename="src/main.py" line="172"/>
        <location filename="src/main.py" line="185"/>
        <location filename="src/main.py" line="214"/>
        <source>Save tha actual file?</source>
        <translation>Guardar el fichero actual?</translation>
    </message>
    <message>
        <location filename="src/main.py" line="173"/>
        <location filename="src/main.py" line="186"/>
        <location filename="src/main.py" line="215"/>
        <source>The actual file is modified. Do you want to save it?</source>
        <translation>El fichero acutal ha sido modificado. Quieres guardarlo?</translation>
    </message>
    <message>
        <location filename="src/main.py" line="199"/>
        <source>New File</source>
        <translation>Nuevo fichero</translation>
    </message>
    <message>
        <location filename="src/main.py" line="208"/>
        <source>Select File to Open</source>
        <translation>Selecciona fichero para abrir</translation>
    </message>
    <message>
        <location filename="src/main.py" line="208"/>
        <source>Socio Graph(*.sog );;All Files (*.*)</source>
        <translation>Grafo Socio(*.sog );; Todos los ficheros (*.*)</translation>
    </message>
    <message>
        <location filename="src/main.py" line="235"/>
        <source>Incorrect File</source>
        <translation>Fichero Incorrecto</translation>
    </message>
    <message>
        <location filename="src/main.py" line="251"/>
        <source>Save file</source>
        <translation>Guardar Fichero</translation>
    </message>
    <message>
        <location filename="src/main.py" line="251"/>
        <source>Grapy Documents (*.sog);;All files (*.*)</source>
        <translation>Documentos Grapy (*.sog);; Todos los ficheros (*.*)</translation>
    </message>
    <message>
        <location filename="src/main.py" line="286"/>
        <source>Untitled</source>
        <translation>Sin título</translation>
    </message>
    <message>
        <location filename="src/main.py" line="310"/>
        <location filename="ui/mainwindow.ui" line="54"/>
        <source>&amp;New</source>
        <translation>&amp;Nuevo</translation>
    </message>
    <message>
        <location filename="src/main.py" line="311"/>
        <source>Create a new file</source>
        <translation>Crear un nuevo fichero</translation>
    </message>
    <message>
        <location filename="src/main.py" line="314"/>
        <source>&amp;Open...</source>
        <translation>&amp;Abrir</translation>
    </message>
    <message>
        <location filename="src/main.py" line="315"/>
        <source>Open an existing file</source>
        <translation>Abrir un fichero existente</translation>
    </message>
    <message>
        <location filename="src/main.py" line="319"/>
        <source>&amp;Save</source>
        <translation>&amp;Guardar</translation>
    </message>
    <message>
        <location filename="src/main.py" line="321"/>
        <source>Save the document to disk</source>
        <translation>Guardar el docuemento en el disco</translation>
    </message>
    <message>
        <location filename="src/main.py" line="323"/>
        <source>Save &amp;As...</source>
        <translation>Guardar &amp;Como...</translation>
    </message>
    <message>
        <location filename="src/main.py" line="325"/>
        <source>Save the document under a new name</source>
        <translation>Guardar el documenti bajo un nuevo nombre</translation>
    </message>
    <message>
        <location filename="src/main.py" line="328"/>
        <source>E&amp;xit</source>
        <translation>&amp;Salir</translation>
    </message>
    <message>
        <location filename="src/main.py" line="329"/>
        <source>Exit the application</source>
        <translation>Salir de la aplicación</translation>
    </message>
    <message>
        <location filename="src/main.py" line="332"/>
        <source>&amp;About</source>
        <translation>S&amp;obre</translation>
    </message>
    <message>
        <location filename="src/main.py" line="333"/>
        <source>Show the application&apos;s About box</source>
        <translation>Muestra la caja sobre la aplicación</translation>
    </message>
    <message>
        <location filename="src/main.py" line="336"/>
        <source>About &amp;Qt</source>
        <translation>Sobre &amp;Qt</translation>
    </message>
    <message>
        <location filename="src/main.py" line="337"/>
        <source>Show the Qt library&apos;s About box</source>
        <translation>Muestra la caja sobre la librería Qt</translation>
    </message>
    <message>
        <location filename="src/main.py" line="339"/>
        <source>&amp;Español</source>
        <translation>&amp;Español</translation>
    </message>
    <message>
        <location filename="src/main.py" line="340"/>
        <source>Cambia el idioma a español</source>
        <translation>Cambia el idioma a español</translation>
    </message>
    <message>
        <location filename="src/main.py" line="342"/>
        <source>&amp;English</source>
        <translation>&amp;English</translation>
    </message>
    <message>
        <location filename="src/main.py" line="343"/>
        <source>Change the language to English</source>
        <translation>Change the language to English</translation>
    </message>
    <message>
        <location filename="src/main.py" line="347"/>
        <location filename="ui/mainwindow.ui" line="42"/>
        <source>&amp;File</source>
        <translation>&amp;Fichero</translation>
    </message>
    <message>
        <location filename="src/main.py" line="357"/>
        <source>&amp;Settings</source>
        <translation>&amp;Configuración</translation>
    </message>
    <message>
        <location filename="src/main.py" line="358"/>
        <source>&amp;Languages</source>
        <translation>&amp;Idiomas</translation>
    </message>
    <message>
        <location filename="src/main.py" line="364"/>
        <source>&amp;Help</source>
        <translation>&amp;Ayuda</translation>
    </message>
</context>
<context>
    <name>MemberTestView</name>
    <message>
        <location filename="src/MemberTestView.py" line="73"/>
        <location filename="src/MemberTestView.py" line="102"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="74"/>
        <source>Surname</source>
        <translation>Apellido</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="75"/>
        <source>Genre</source>
        <translation>Género</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="76"/>
        <source>Age</source>
        <translation>Edad</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="77"/>
        <source>Active</source>
        <translation>Activo</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="78"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="80"/>
        <source>Abbreviation</source>
        <translation>Abreviación</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="99"/>
        <source>Created_at</source>
        <translation>Creado en</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="119"/>
        <source>Open profile</source>
        <translation>Abrir profile</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="146"/>
        <source>One row was not inserted</source>
        <translation>Una fila no fue insertada</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="149"/>
        <source>Error reading CSV File</source>
        <translation>Error leyendo el fichero CSV</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="150"/>
        <source>The CSV File has not the correct format. Try to see the example in this program. The error was: </source>
        <translation>El fichero CSV no tiene el formato correcto. Intente ver el ejemplo en este programa. El error fue: </translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="154"/>
        <source>CSV File correctly inserted</source>
        <translation>El fichero CSV fue instalado correctamente</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="155"/>
        <source>The CSV File has correct format.</source>
        <translation>El fichero CSV tiene el formato correcto. </translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="160"/>
        <location filename="src/MemberTestView.py" line="197"/>
        <source>No values to add</source>
        <translation>Sin valores para añadir</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="161"/>
        <source>The field Name is not filled</source>
        <translation>El campo nombre no está lleno</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="175"/>
        <location filename="src/MemberTestView.py" line="223"/>
        <source>Could not insert the new member</source>
        <translation>No se ha podido insertar el nuevo miembro</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="178"/>
        <source>Test inserted</source>
        <translation>Test insertado</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="179"/>
        <location filename="src/MemberTestView.py" line="260"/>
        <source>The test data was inserted correctly.</source>
        <translation>Los datos del test fueron insertados correctamente.</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="184"/>
        <source>Adding participants</source>
        <translation>Añadiendo participantes</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="185"/>
        <source>Adding the active members of this group as a participants in this new test. Later, you can not add new participants. To add a new participant, please create a new test. </source>
        <translation>Añadiendo los miembros activos de este grupo como participantes de este nuevo test. Más tarde, no podrá añadir nuevos participantes. Para añadir nuevos participantes, por favor, cree un nuevo test. </translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="191"/>
        <location filename="src/MemberTestView.py" line="254"/>
        <source>Error Adding the new test</source>
        <translation>Error añadiendo el nuevo test</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="192"/>
        <location filename="src/MemberTestView.py" line="227"/>
        <location filename="src/MemberTestView.py" line="255"/>
        <location filename="src/MemberTestView.py" line="294"/>
        <source>Some fields are not correct. The error was: </source>
        <translation>Algunos campos no son correctos. El error fue: </translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="198"/>
        <source>The fields Name or Organization or Email or Abbreviation are not filled or Age is negative</source>
        <translation>Los campos Nombre o Organización o Email o Abreviación no estan llenos o la edad es negativa</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="226"/>
        <location filename="src/MemberTestView.py" line="293"/>
        <source>Error Adding the new member</source>
        <translation>Error añadiendo el nuevo miembro</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="231"/>
        <location filename="src/MemberTestView.py" line="298"/>
        <source>Member inserted</source>
        <translation>Miembro insertado</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="232"/>
        <location filename="src/MemberTestView.py" line="299"/>
        <source>The member data was inserted correctly.</source>
        <translation>Los datos del miembro fueron insertados correctamente.</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="236"/>
        <location filename="src/MemberTestView.py" line="304"/>
        <source>No test selected</source>
        <translation>Ningún test seleccionado</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="237"/>
        <source>To update a test, you have to select one test.</source>
        <translation>Para modificar un test, debes seleccionar un test. </translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="250"/>
        <source>Could not update the new test</source>
        <translation>No se ha podido modificar el nuevo test</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="259"/>
        <source>Test Updated</source>
        <translation>Test modificado</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="264"/>
        <location filename="src/MemberTestView.py" line="318"/>
        <source>No member selected</source>
        <translation>Ningún miembro seleccionado</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="265"/>
        <source>To update a member, you have to select one member.</source>
        <translation>Para modificar un miembro, debes selecciona un miembro</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="289"/>
        <source>Could not update the new member</source>
        <translation>No se ha podido modificar el nuevo miembro</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="305"/>
        <source>To delete a test, you have to select one test.</source>
        <translation>Para borrar un test, debes seleccionar un test. </translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="309"/>
        <location filename="src/MemberTestView.py" line="323"/>
        <source>Delete the current line?</source>
        <translation>Borrar la linia actual?</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="310"/>
        <location filename="src/MemberTestView.py" line="324"/>
        <source>Are you sure?</source>
        <translation>Estás seguro?</translation>
    </message>
    <message>
        <location filename="src/MemberTestView.py" line="319"/>
        <source>To delete a member, you have to select one member.</source>
        <translation>Para borrar un miembro, debes seleccionar un miembro.</translation>
    </message>
</context>
<context>
    <name>ParticipantView</name>
    <message>
        <location filename="src/ParticipantView.py" line="43"/>
        <location filename="src/ParticipantView.py" line="50"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="src/ParticipantView.py" line="44"/>
        <location filename="src/ParticipantView.py" line="51"/>
        <source>Surname</source>
        <translation>Apellido</translation>
    </message>
    <message>
        <location filename="src/ParticipantView.py" line="45"/>
        <location filename="src/ParticipantView.py" line="52"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="src/ParticipantView.py" line="46"/>
        <location filename="src/ParticipantView.py" line="53"/>
        <source>Code</source>
        <translation>Código</translation>
    </message>
    <message>
        <location filename="src/ParticipantView.py" line="47"/>
        <location filename="src/ParticipantView.py" line="54"/>
        <source>Number of answers</source>
        <translation>Número de Respuestas</translation>
    </message>
</context>
</TS>
